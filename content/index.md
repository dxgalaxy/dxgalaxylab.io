---
title: Deus Ex Galaxy
---

This is a site meant to archive Deus Ex resources, as well as provide extensive documentation for modders.

@mods
    heading: Mods
    subheading: These are mods developed by the Deus Ex Galaxy team
    mods:
        - name: "Deus Ex: Machine God"
          image: "https://machine-god.dxgalaxy.org/img/banner.jpg"
          url: "https://machine-god.dxgalaxy.org"

