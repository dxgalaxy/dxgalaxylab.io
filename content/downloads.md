---
title: Downloads
---

@downloads(Editing tools)
    files:
        - name: DeusExSDK1112f.exe
          author: Ion Storm
        - name: UED2_2_For_DeusEx.7z
          author: LoadLineCalibration
        - name: UED2_2_DoNotPlaceInventorySpots.u
          author: LoadLineCalibration
        - name: DEED.zip
          author: Hexy
        - name: HTK-20160618.zip
          author: Hanfling
        - name: Trestkonizer.exe
          author: Trestkon
        - name: DXOgg.zip
          author: ???

@downloads(Textures)
    files:
        - name: Bright.zip
          author: Erik de Neve

@downloads(Documentation)
    files:
        - name: Tack's Guide.chm
          author: Steve Tack
        - name: lodes-tutorials.zip
          author: Lode Vandevenne

@downloads(Modelling)
    files:
        - name: 3ds2unr.exe
          author: ???
        - name: unr2de.zip
          author: Steve Tack
        - name: UE1-VertexMesh-Blender-IO.zip
          author: Skywolf285
        - name: blender_t3d_110.zip
          author: crapola

@downloads(Conversations)
    files:
        - name: ConEditExport.jar
          author: KillerBeer

@downloads(Game enhancements)
    files:
        - name: DeusExe-v8.1.zip
          author: Marijn Kentie
        - name: dxglr20.zip
          author: Chris Dohnal
        - name: RenderExt.zip
          author: Revision Team
