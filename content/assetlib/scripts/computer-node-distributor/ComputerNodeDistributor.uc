//=========================================
// ComputerNodeDistributor
//=========================================
class ComputerNodeDistributor extends KeyPoint;

var() string NodeName;
var() string NodeDesc;
var() string NodeAddress;
var() Texture NodeTexture;

function PostBeginPlay()
{
    local Computers aComputer;

    if(Event == '')
        return;

    foreach AllActors(class'Computers', aComputer, Event)
    {
        aComputer.SetPropertyText("ComputerNode", "CN_UNATCO");
        aComputer.SetPropertyText("NodeInfo", "(nodeName=\"" $ NodeName $ "\",nodeDesc=\"" $ NodeDesc $ "\",nodeAddress=\"" $ NodeAddress $ "\",nodeTexture=Texture'" $ NodeTexture $ "')");
    }
}


