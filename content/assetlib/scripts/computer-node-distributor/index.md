---
title:          Computer Node Distributor
description:    This is a simple actor that enables you to create custom computer nodes without subclassing. Just place this actor in your map, set its values as desired and connect its `Event` to the `Tag` of the computers you want to change.
author:         Artifechs
---

@downloads(Files)
    files:
        - ComputerNodeDistributor.uc
