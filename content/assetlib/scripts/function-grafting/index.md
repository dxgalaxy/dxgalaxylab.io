---
title:          Function grafting
description:    This hack allows you to add functions to an existing class without subclassing it. Useful for fixing broken/incomplete features.
author:         aizome8086
---

!!! Disclaimer
    Do not use this. Really, don't. Unless you have to.

## BarRat.uc

    class BarRat extends Rat;

    var string SecretCode;

    /* HACK: The final modifier prevents a crash caused by looking up
    function names at runtime. DO NOT REMOVE!!! */

    final function SetSecretCode(string NewCode)
    {
        SecretCode = NewCode;
    }


## FooRat.uc

    class FooRat extends Rat;

    var const private string SecretCode;

    function EnterSecretCode(string Code)
    {
        if(Code == SecretCode)
        {
            Log("Secret code entered!");
        }
        else
        {
            Log("Invalid code entered!");
        }
    }

    defaultproperties
    {
        SecretCode="Swiss cheese"
    }


## TestRat.uc

    class TestRat extends Rat;

    struct Empty {};

    struct FooRatRef extends Empty
    {
        var FooRat Value; 
    };

    final function BarRat CastFooRatToBarRat(FooRat FooRat)
    {
        local FooRatRef FooRatRef;
        local Empty Empty;
        local BarRat Result;

        FooRatRef.Value = FooRat;

        /* HACK: This assignment overflows the contents of FooRatRef.Value onto
        the local variable buffer, causing Result to hold a reference to FooRat. */

        Empty = FooRatRef;

        return Result;
    }

    event PostBeginPlay()
    {
        local FooRat FooRat;
        local BarRat BarRat;

        super.PostBeginPlay();

        FooRat = Spawn(class'FooRat');
        BarRat = CastFooRatToBarRat(FooRat);

        FooRat.EnterSecretCode("cheese");
        BarRat.SetSecretCode("cheese");
        FooRat.EnterSecretCode("cheese");
    }
