class FemalePlayerCarcass extends DeusExCarcass;

defaultproperties
{
    Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants_Carcass'
    Mesh2=LodMesh'DeusExCharacters.GFM_TShirtPants_CarcassB'
    Mesh3=LodMesh'DeusExCharacters.GFM_TShirtPants_CarcassC'
    MultiSkins(0)=Texture'DeusExCharacters.Skins.AnnaNavarreTex0'
    MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(2)=Texture'DeusExCharacters.Skins.AnnaNavarreTex0'
    MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(4)=Texture'DeusExItems.Skins.BlackMaskTex'
    MultiSkins(5)=Texture'DeusExCharacters.Skins.AnnaNavarreTex0'
    MultiSkins(6)=Texture'DeusExCharacters.Skins.AnnaNavarreTex2'
    MultiSkins(7)=Texture'DeusExCharacters.Skins.AnnaNavarreTex1'
    CollisionRadius=40.000000
}
