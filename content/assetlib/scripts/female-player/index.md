---
title:          Female player
description:    This script replaces JC Denton with a female player character (Anna Navarre used here as an example).
author:         Artifechs
---

@downloads(Files)
    files:
        - FemalePlayer.uc
        - FemalePlayerCarcass.uc
        - FemalePlayer.uax

Sounds borrowed from the [LayD Denton](https://www.moddb.com/mods/the-lay-d-denton-project) project.
