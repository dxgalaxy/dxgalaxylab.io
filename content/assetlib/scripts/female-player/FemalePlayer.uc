class FemalePlayer extends JCDentonMale;

// ----------------------------
// Fix player sounds for damage
// ----------------------------
function PlayTakeHitSound(int Damage, name damageType, int Mult)
{
    local float rnd;

    if ( Level.TimeSeconds - LastPainSound < FRand() + 0.5)
        return;

    LastPainSound = Level.TimeSeconds;

    if (Region.Zone.bWaterZone)
    {
        if (damageType == 'Drowned')
        {
            if (FRand() < 0.8)
                PlaySound(sound'FemalePlayer.Player.Drown', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        }
        else
            PlaySound(sound'FemalePlayer.Player.PainSmall', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
    }
    else
    {
        // Body hit sound for multiplayer only
        if (((damageType=='Shot') || (damageType=='AutoShot'))  && ( Level.NetMode != NM_Standalone ))
        {
            PlaySound(sound'BodyHit', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        }

        if ((damageType == 'TearGas') || (damageType == 'HalonGas'))
            PlaySound(sound'FemalePlayer.Player.EyePain', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        else if (damageType == 'PoisonGas')
            PlaySound(sound'FemalePlayer.Player.Cough', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        else
        {
            rnd = FRand();
            if (rnd < 0.33)
                PlaySound(sound'FemalePlayer.Player.PainSmall', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
            else if (rnd < 0.66)
                PlaySound(sound'FemalePlayer.Player.PainMedium', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
            else
                PlaySound(sound'FemalePlayer.Player.PainLarge', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        }
        AISendEvent('LoudNoise', EAITYPE_Audio, FMax(Mult * TransientSoundVolume, Mult * 2.0));
    }
}

// ----------------------------
// Fix player sounds for gasping
// ----------------------------
function Gasp()
{
    PlaySound(sound'FemalePlayer.Player.Gasp', SLOT_Pain,,,, RandomPitch());
}

// ----------------------------
// Fix player sounds for dying
// ----------------------------
function PlayDyingSound()
{
    if (Region.Zone.bWaterZone)
        PlaySound(sound'FemalePlayer.Player.WaterDeath', SLOT_Pain,,,, RandomPitch());
    else
        PlaySound(sound'FemalePlayer.Player.Death', SLOT_Pain,,,, RandomPitch());
}

defaultproperties
{
    BindName="JCDenton"
    TruePlayerName="Female Player"
    FamiliarName="Female Player"
    UnfamiliarName="Female Player"
    CarcassType=Class'DeusEx.FemalePlayerCarcass'
    bIsFemale=True
    BaseEyeHeight=38.000000
    Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
    MultiSkins(0)=Texture'DeusExCharacters.Skins.AnnaNavarreTex0'
    MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(2)=Texture'DeusExCharacters.Skins.AnnaNavarreTex0'
    MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(4)=Texture'DeusExItems.Skins.BlackMaskTex'
    MultiSkins(5)=Texture'DeusExCharacters.Skins.AnnaNavarreTex0'
    MultiSkins(6)=Texture'DeusExCharacters.Skins.AnnaNavarreTex2'
    MultiSkins(7)=Texture'DeusExCharacters.Skins.AnnaNavarreTex1'
    CollisionRadius=20.000000
    CollisionHeight=43.000000
    JumpSound=Sound'FemalePlayer.Player.Jump'
    HitSound1=Sound'FemalePlayer.Player.PainSmall'
    HitSound2=Sound'FemalePlayer.Player.PainMedium'
    Land=Sound'FemalePlayer.Player.Land'
    Die=Sound'FemalePlayer.Player.Death'
}
