---
title:          Crash fixes
description:    These stubs should prevent crashes that occur when nonexisting functions accidentally get called by the game
author:         Hanfling
---

## ConPlay.uc
```
function CloseConWindow();
function SetupEvent();
```

## DeusExPlayer.uc

```
function FindGoodView();
function PlayerMove(Float DeltaTime);
function Rise();
function Dodge(eDodgeDir DodgeMove);
```

## DeusExProjectile.uc
```
function DamageRing();
```

## ScriptedPawn.uc

```
function AdjustJump();
function MoveFallingBody();
function Vector FocusDirection();
function FindBackupPoint();
function bool DoorEncroaches();

function PickDestinationPlain();
function PickDestinationInputBool(bool ExampleBool);
function bool PickDestinationBool();
function EDestinationType PickDestinationEnum();

function float DistanceToTarget();
function AlarmUnit FindTarget();
function bool GetNextAlarmPoint(AlarmUnit alarm);
function vector FindAlarmPosition(Actor alarm);
function bool IsAlarmInRange(AlarmUnit alarm);
function TriggerAlarm();
function bool IsAlarmReady(Actor actorAlarm);
function EndCrouch();
function StartCrouch();
function bool ShouldCrouch();
function bool ReadyForWeapon();
function bool IsHandToHand();
function CheckAttack(bool bPlaySound);
function bool FireIfClearShot();
function bool InSeat(out vector newLoc);
function FinishFleeing();
function NavigationPoint GetOvershootDestination(float randomness, optional float focus);
function bool GetNextLocation(out vector nextLoc);
function PatrolPoint PickStartPoint();
function bool GoHome();
function FollowSeatFallbackOrders();
function FindBestSeat();
function int FindBestSlot(Seat seatActor, out float slotDist);
function bool IsIntersectingSeat();
function vector GetDestinationPosition(Seat seatActor, optional float extraDist);
function Vector SitPosition(Seat seatActor, int slot);
```
