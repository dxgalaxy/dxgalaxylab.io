---
title:          Shivs
description:    This is a collection of useful functions present in later versions of UnrealScript
author:         Artifechs, aizome8086
---

```
function string Locs(coerce string Text)
{
    local int TextLength, TextIndex;
    local string Glyph, Result, Uppercase;

    Uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    TextLength = Len(Text);

    for(TextIndex = 0; TextIndex < TextLength; ++TextIndex)
    {
        Glyph = Mid(Text, TextIndex, 1);

        if(InStr(Uppercase, Glyph) > -1)
            Glyph = Chr(Asc(Glyph) + 32);

        Result = Result $ Glyph;
    }

    return Result;
}

function string Split(coerce string Text, coerce string SplitStr, optional bool bOmitSplitStr)
{
    local int i;

    i = InStr(Text, SplitStr);

    if(i < 0)
        return Text;

    if(!bOmitSplitStr)
        return Mid(Text, i);

    if(bOmitSplitStr)
        return Mid(Text, i + Len(SplitStr));
}

function string Repl(coerce string Text, coerce string Replace, coerce string With)
{
    local int i;
    local string Output;

    i = InStr(Text, Replace);
    while (i != -1) {
        Output = Output $ Left(Text, i) $ With;
        Text = Mid(Text, i + Len(Replace));
        i = InStr(Text, Replace);
    }
    Output = Output $ Text;
    return Output;
}

function JoinArray(string StringArray[100], out string Result, optional string Delimiter, optional bool bIgnoreBlanks)
{
    local int i;

    Result = "";

    for(i = 0; i < ArrayCount(StringArray); i++)
    {
        if(StringArray[i] == "" && bIgnoreBlanks)
            continue;

        if(Result != "")
            Result = Result $ Delimiter;

        Result = Result $ StringArray[i];
    }
}

function ParseStringIntoArray(coerce string InStr, out string Parts[100], string Delimiter)
{
    local string Part, Glyph;
    local int i, NumParts;

    for(i = 0; i < Len(InStr) + 1; i++)
    {
        Glyph = Mid(InStr, i, 1);

        if(Glyph == Delimiter || i == Len(InStr))
        {
            Parts[NumParts] = Part;
            NumParts++;
            Part = "";
        }
        else
        {
            Part = Part $ Glyph;
        }
    }
}
```
