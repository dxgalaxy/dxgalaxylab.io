class RandomMale extends RandomHuman;

var sTextureInfo Heads[100];
var sTextureInfo Torsos[100];
var Texture UNATCOTroopHelmet;
var Texture MechanicHelmet;
var Texture SoldierHelmet;
var Texture MJ12TroopHelmet;
var Texture Visor;
var Texture Goggles;

// -----------------------
// Generates the character
// -----------------------
function Generate()
{
    // Military and police are always in jumpsuits
    if(FinalTag == "Military" || FinalTag == "Police")
    {
        GenerateGMJumpsuit();
    }
    // Guerilla can be in shirts or jumpsuits
    else if(FinalTag == "Guerilla")
    {
        switch(Rand(3))
        {
            case 0:
                GenerateGMDressShirt(BODY_Normal);
                break;

            case 1:
                GenerateGMDressShirt(BODY_Fat);
                break;
            
            case 2:
                GenerateGMJumpsuit();
                break;
        }
    }
    // Everyone else
    else
    {
        switch(Rand(5))
        {
            case 0:
                GenerateGMDressShirt(BODY_Normal);
                break;

            case 1:
                GenerateGMDressShirt(BODY_Fat);
                break;
            
            case 2:
                GenerateGMDressShirt(BODY_Skinny);
                break;
            
            case 3:
                GenerateGMTrench(BODY_Normal);
                break;
            
            case 4:
                GenerateGMTrench(BODY_Fat);
                break;
        }
    }
}

// ---------------
// GM_DressShirt
// 0: Head
// 1: Shade back
// 2: Shade front
// 3: Legs
// 4: ???
// 5: Torso
// 6: Frames
// 7: Lenses
// ---------------
function GenerateGMDressShirt(eBodyType BodyType)
{
    local sTextureInfo HeadTextureInfo;

    if(BodyType == BODY_Fat)
        Mesh = LodMesh'DeusExCharacters.GM_DressShirt_F';

    else if(BodyType == BODY_Skinny)
        Mesh = LodMesh'DeusExCharacters.GM_DressShirt_S';

    else
        Mesh = LodMesh'DeusExCharacters.GM_DressShirt';

    // Head
    HeadTextureInfo = GetRandomTextureInfo(Heads);

    MultiSkins[0] = HeadTextureInfo.Tex; 
    MultiSkins[1] = PinkMask;
    MultiSkins[2] = PinkMask;

    // Cap
    if(HeadTextureInfo.HeadGear == HG_CapForward)
        MultiSkins[1] = MultiSkins[0];
    else if(HeadTextureInfo.HeadGear== HG_CapBackward)
        MultiSkins[2] = MultiSkins[0];

    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[6] = GetRandomTexture(Frames);   
        MultiSkins[7] = GetRandomTexture(Lenses);   
    }
    else
    {
        MultiSkins[6] = GrayMask;
        MultiSkins[7] = BlackMask;
    }

    // Torso
    MultiSkins[5] = GetRandomTexture(Torsos, HeadTextureInfo.SkinColor);
   
    // Legs
    MultiSkins[3] = GetRandomTexture(Pants, HeadTextureInfo.SkinColor);
}

// ----------------------
// GM_Trench
//
// 0: Head
// 1: Torso 
// 2: Legs
// 3: ???
// 4: Shirt
// 5: Coattail and collar
// 6: Frames
// 7: Lenses
// ----------------------
function GenerateGMTrench(eBodyType BodyType)
{
    local sTextureInfo HeadTextureInfo;
  
    if(BodyType == BODY_Fat)
        Mesh = LodMesh'DeusExCharacters.GM_Trench_F';
    else
        Mesh = LodMesh'DeusExCharacters.GM_Trench';

    // Head
    HeadTextureInfo = GetRandomTextureInfo(Heads);

    MultiSkins[0] = HeadTextureInfo.Tex; 
    
    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[6] = GetRandomTexture(Frames);   
        MultiSkins[7] = GetRandomTexture(Lenses);   
    }
    else
    {
        MultiSkins[6] = GrayMask;
        MultiSkins[7] = BlackMask;
    }

    // Coat
    MultiSkins[4] = GetRandomTexture(CoatShirts);
    MultiSkins[1] = GetRandomTexture(Coats);
    MultiSkins[5] = MultiSkins[1];

    // Pants
    MultiSkins[2] = GetRandomTexture(Pants);
}

// -----------
// GM_Jumpsuit
// 0: ???
// 1: Legs
// 2: Torso
// 3: Head
// 4: Mask
// 5: Goggles
// 6: Helmet
// 7: ???
// -----------
function GenerateGMJumpsuit()
{
    local sTextureInfo HeadTextureInfo;
        
    Mesh = LodMesh'DeusExCharacters.GM_Jumpsuit';
 
    // Head
    MultiSkins[0] = GetRandomTexture(Heads);
    MultiSkins[3] = MultiSkins[0];

    // Mask
    if(Rand(2) > 0)
        MultiSkins[4] = MultiSkins[0];
    else
        MultiSkins[4] = PinkMask;

    // Goggles
    if(Rand(2) > 0)
        MultiSkins[5] = Goggles;
    else
        MultiSkins[5] = PinkMask;

    // Torso
    MultiSkins[2] = GetRandomTexture(Torsos);

    // Pants
    MultiSkins[1] = GetRandomTexture(Pants);

    // Helmet
    if(Rand(2) > 0 && Finaltag == "Tech")
        MultiSkins[6] = MechanicHelmet;
    else if(Rand(2) > 0 && InStr(MultiSkins[2].Name, "UNATCO") > -1)
        MultiSkins[6] = UNATCOTroopHelmet;
    else if(Rand(2) > 0 && FinalTag == "Military")
        MultiSkins[6] = SoldierHelmet;
    else if(Rand(2) > 0 && InStr(MultiSkins[2].Name, "MJ12") > -1)
        MultiSkins[6] = MJ12TroopHelmet;
    else
        MultiSkins[6] = PinkMask;

    MultiSkins[7] = PinkMask;
    Texture = PinkMask;
}

defaultproperties
{
    Mesh=LodMesh'DeusExCharacters.GM_DressShirt'
    CollisionHeight=47.500000
    HitSound1=Sound'DeusExSounds.Player.MalePainSmall'
    HitSound2=Sound'DeusExSounds.Player.MalePainMedium'
    Die=Sound'DeusExSounds.Player.MaleDeath'
    WalkingSpeed=0.300000
    BaseAssHeight=-23.000000
    WalkingSpeed=0.320000
    WalkAnimMult=1.000000
    GroundSpeed=250.000000
    BindName="RandomMale"
    UnfamiliarName="Man"
    FamiliarName="Man"

    // Helmets
    UNATCOTroopHelmet=Texture'DeusExCharacters.Skins.UNATCOTroopTex3'
    MechanicHelmet=Texture'DeusExCharacters.Skins.MechanicTex3'
    SoldierHelmet=Texture'DeusExCharacters.Skins.SoldierTex3'
    MJ12TroopHelmet=Texture'DeusExCharacters.Skins.MJ12TroopTex3';
    Goggles=Texture'DeusExCharacters.Skins.GogglesTex'
    
    // Misc
    Visor=Texture'DeusExCharacters.Skins.VisorTex'

    // Heads
    Heads(0)=(Tex=Texture'DeusExCharacters.Skins.AlexJacobsonTex0',bCelebrity=True,SkinColor="Light")
    Heads(1)=(Tex=Texture'DeusExCharacters.Skins.BartenderTex0',SkinColor="Dark")
    Heads(2)=(Tex=Texture'DeusExCharacters.Skins.BoatPersonTex0',SkinColor="Medium")
    Heads(3)=(Tex=Texture'DeusExCharacters.Skins.BobPageTex0',bCelebrity=True,SkinColor="Lighter")
    Heads(4)=(Tex=Texture'DeusExCharacters.Skins.BumMaleTex0',SkinColor="Light")
    Heads(5)=(Tex=Texture'DeusExCharacters.Skins.BumMale2Tex0',SkinColor="Light")
    Heads(6)=(Tex=Texture'DeusExCharacters.Skins.BumMale3Tex0',SkinColor="Light")
    Heads(7)=(Tex=Texture'DeusExCharacters.Skins.Businessman2Tex0',SkinColor="Medium")
    Heads(8)=(Tex=Texture'DeusExCharacters.Skins.ButlerTex0',SkinColor="Light")
    Heads(9)=(Tex=Texture'DeusExCharacters.Skins.ChefTex0',SkinColor="Medium")
    Heads(10)=(Tex=Texture'DeusExCharacters.Skins.CopTex0',SkinColor="Light",Tag1="Police",Tag2="CapForward")
    Heads(11)=(Tex=Texture'DeusExCharacters.Skins.DoctorTex0',SkinColor="Light")
    Heads(12)=(Tex=Texture'DeusExCharacters.Skins.FordSchickTex0',SkinColor="Light")
    Heads(13)=(Tex=Texture'DeusExCharacters.Skins.GarySavageTex0',bCelebrity=True,SkinColor="Light")
    Heads(14)=(Tex=Texture'DeusExCharacters.Skins.GilbertRentonTex0',bCelebrity=True,SkinColor="Light")
    Heads(15)=(Tex=Texture'DeusExCharacters.Skins.GordonQuickTex0',bCelebrity=True,SkinColor="Light")
    Heads(16)=(Tex=Texture'DeusExCharacters.Skins.GuntherHermannTex0',bCelebrity=True,SkinColor="Ghoul")
    Heads(17)=(Tex=Texture'DeusExCharacters.Skins.HarleyFilbenTex0',bCelebrity=True,SkinColor="Light")
    Heads(18)=(Tex=Texture'DeusExCharacters.Skins.HowardStrongTex0',SkinColor="Light")
    Heads(19)=(Tex=Texture'DeusExCharacters.Skins.JaimeReyesTex0',bCelebrity=True,SkinColor="Light")
    Heads(20)=(Tex=Texture'DeusExCharacters.Skins.JCDentonTex0',bCelebrity=True,SkinColor="Light",Tag1="Mech")
    Heads(21)=(Tex=Texture'DeusExCharacters.Skins.JCDentonTex4',bCelebrity=True,SkinColor="Darker",Tag1="Mech")
    Heads(22)=(Tex=Texture'DeusExCharacters.Skins.JCDentonTex5',bCelebrity=True,SkinColor="Dark",Tag1="Mech")
    Heads(23)=(Tex=Texture'DeusExCharacters.Skins.JCDentonTex6',bCelebrity=True,SkinColor="Light",Tag1="Mech")
    Heads(24)=(Tex=Texture'DeusExCharacters.Skins.JCDentonTex7',bCelebrity=True,SkinColor="Ghoul",Tag1="Mech")
    Heads(25)=(Tex=Texture'DeusExCharacters.Skins.JoeGreeneTex0',bCelebrity=True,SkinColor="Light")
    Heads(26)=(Tex=Texture'DeusExCharacters.Skins.JoJoFineTex0',bCelebrity=True,SkinColor="Medium")
    Heads(27)=(Tex=Texture'DeusExCharacters.Skins.JosephManderleyTex0',bCelebrity=True,SkinColor="Lighter")
    Heads(28)=(Tex=Texture'DeusExCharacters.Skins.JuanLebedevTex0',bCelebrity=True,SkinColor="Lighter")
    Heads(29)=(Tex=Texture'DeusExCharacters.Skins.JunkieMaleTex0',SkinColor="Ghoul",Tag1="Bum")
    Heads(30)=(Tex=Texture'DeusExCharacters.Skins.LowerClassMale2Tex0',SkinColor="Light")
    Heads(31)=(Tex=Texture'DeusExCharacters.Skins.Male1Tex0',SkinColor="Light")
    Heads(32)=(Tex=Texture'DeusExCharacters.Skins.Male4Tex0',SkinColor="Medium")
    Heads(33)=(Tex=Texture'DeusExCharacters.Skins.MaxChenTex0',bCelebrity=True,SkinColor="Medium")
    Heads(34)=(Tex=Texture'DeusExCharacters.Skins.MIBTex0',bCelebrity=True,SkinColor="Ghoul")
    Heads(35)=(Tex=Texture'DeusExCharacters.Skins.MichaelHamnerTex0',SkinColor="Light")
    Heads(36)=(Tex=Texture'DeusExCharacters.Skins.MiscTex1',SkinColor="Light",Tag1="Military")
    Heads(37)=(Tex=Texture'DeusExCharacters.Skins.MorganEverettTex0',bCelebrity=True,SkinColor="Darker")
    Heads(38)=(Tex=Texture'DeusExCharacters.Skins.NathanMadisonTex0',SkinColor="Lighter")
    Heads(39)=(Tex=Texture'DeusExCharacters.Skins.PaulDentonTex0',bCelebrity=True,SkinColor="Light")
    Heads(40)=(Tex=Texture'DeusExCharacters.Skins.PaulDentonTex4',bCelebrity=True,SkinColor="Darker")
    Heads(41)=(Tex=Texture'DeusExCharacters.Skins.PaulDentonTex5',bCelebrity=True,SkinColor="Dark")
    Heads(42)=(Tex=Texture'DeusExCharacters.Skins.PaulDentonTex6',bCelebrity=True,SkinColor="Light")
    Heads(43)=(Tex=Texture'DeusExCharacters.Skins.PaulDentonTex7',bCelebrity=True,SkinColor="Ghoul")
    Heads(44)=(Tex=Texture'DeusExCharacters.Skins.PhilipMeadTex0',SkinColor="Light")
    Heads(45)=(Tex=Texture'DeusExCharacters.Skins.SailorTex0',SkinColor="Light",Tag1="Military")
    Heads(46)=(Tex=Texture'DeusExCharacters.Skins.SamCarterTex0',bCelebrity=True,SkinColor="Light")
    Heads(47)=(Tex=Texture'DeusExCharacters.Skins.ScientistMaleTex0',SkinColor="Light")
    Heads(48)=(Tex=Texture'DeusExCharacters.Skins.SecretServiceTex0',SkinColor="Light",Tag1="Police",Tag2="Military")
    Heads(49)=(Tex=Texture'DeusExCharacters.Skins.SkinTex1',SkinColor="Light")
    Heads(50)=(Tex=Texture'DeusExCharacters.Skins.SkinTex2',SkinColor="Light")
    Heads(51)=(Tex=Texture'DeusExCharacters.Skins.SkinTex3',SkinColor="Light")
    Heads(52)=(Tex=Texture'DeusExCharacters.Skins.SkinTex4',SkinColor="Light")
    Heads(53)=(Tex=Texture'DeusExCharacters.Skins.SoldierTex0',SkinColor="Medium",HeadGear=HG_Helmet)
    Heads(54)=(Tex=Texture'DeusExCharacters.Skins.StantonDowdTex0',bCelebrity=True,SkinColor="Light")
    Heads(55)=(Tex=Texture'DeusExCharacters.Skins.TerroristTex0',SkinColor="Light",Tag1="Military")
    Heads(58)=(Tex=Texture'DeusExCharacters.Skins.ThugMale3Tex0',SkinColor="Light",Tag1="Mech")
    Heads(59)=(Tex=Texture'DeusExCharacters.Skins.TonyAtanweTex0',SkinColor="Darker")
    Heads(60)=(Tex=Texture'DeusExCharacters.Skins.TracerTongTex0',bCelebrity=True,SkinColor="Light")
    Heads(61)=(Tex=Texture'DeusExCharacters.Skins.TriadLumPathTex0',SkinColor="Light")
    Heads(62)=(Tex=Texture'DeusExCharacters.Skins.TriadLumPath2Tex0',SkinColor="Light")
    Heads(63)=(Tex=Texture'DeusExCharacters.Skins.TriadRedArrowTex0',SkinColor="Light")
    Heads(64)=(Tex=Texture'DeusExCharacters.Skins.WaltonSimonsTex0',bCelebrity=True,SkinColor="Ghoul")
    
    // Torsos
    Torsos(0)=(Tex=Texture'DeusExCharacters.Skins.AlexJacobsonTex1',Tag1="Normal",Tag2="Tech")
    Torsos(1)=(Tex=Texture'DeusExCharacters.Skins.BartenderTex1',Tag1="Servant")
    Torsos(2)=(Tex=Texture'DeusExCharacters.Skins.BoatPersonTex1',SkinColor="Medium",Tag1="Bum")
    Torsos(3)=(Tex=Texture'DeusExCharacters.Skins.BobPageTex1',Tag1="Normal",Tag2="Formal")
    Torsos(4)=(Tex=Texture'DeusExCharacters.Skins.Businessman1Tex1',Tag1="Formal")
    Torsos(5)=(Tex=Texture'DeusExCharacters.Skins.Businessman2Tex1',Tag1="Formal")
    Torsos(6)=(Tex=Texture'DeusExCharacters.Skins.Businessman3Tex1',Tag1="Formal")
    Torsos(7)=(Tex=Texture'DeusExCharacters.Skins.ButlerTex1',Tag1="Servant")
    Torsos(8)=(Tex=Texture'DeusExCharacters.Skins.ChadTex1',Tag1="Normal")
    Torsos(9)=(Tex=Texture'DeusExCharacters.Skins.ChefTex1',Tag1="Servant")
    Torsos(10)=(Tex=Texture'DeusExCharacters.Skins.ChildMaleTex1',Tag1="Normal")
    Torsos(11)=(Tex=Texture'DeusExCharacters.Skins.ChildMale2Tex1',Tag1="Normal")
    Torsos(12)=(Tex=Texture'DeusExCharacters.Skins.CopTex1',Tag1="Police")
    Torsos(13)=(Tex=Texture'DeusExCharacters.Skins.GuntherHermannTex1',Tag1="Mech")
    Torsos(14)=(Tex=Texture'DeusExCharacters.Skins.HKMilitaryTex1',SkinColor="Light",Tag1="Military",Tag2="Police")
    Torsos(15)=(Tex=Texture'DeusExCharacters.Skins.HowardStrongTex1',Tag1="Normal")
    Torsos(16)=(Tex=Texture'DeusExCharacters.Skins.JanitorTex1',Tag1="Normal")
    Torsos(17)=(Tex=Texture'DeusExCharacters.Skins.JoeGreeneTex1',Tag1="Normal",Tag2="Formal")
    Torsos(18)=(Tex=Texture'DeusExCharacters.Skins.JoJoFineTex1',Tag1="Normal",SkinColor="Medium")
    Torsos(19)=(Tex=Texture'DeusExCharacters.Skins.JunkieMaleTex1',Tag1="Bum")
    Torsos(20)=(Tex=Texture'DeusExCharacters.Skins.LowerClassMaleTex1',Tag1="Normal")
    Torsos(21)=(Tex=Texture'DeusExCharacters.Skins.LowerClassMale2Tex1',Tag1="Chinese")
    Torsos(22)=(Tex=Texture'DeusExCharacters.Skins.Male1Tex1',Tag1="Formal")
    Torsos(23)=(Tex=Texture'DeusExCharacters.Skins.Male2Tex1',Tag1="Normal")
    Torsos(24)=(Tex=Texture'DeusExCharacters.Skins.Male3Tex1',Tag1="Normal")
    Torsos(25)=(Tex=Texture'DeusExCharacters.Skins.Male4Tex1',Tag1="Chinese",SkinColor="Medium")
    Torsos(26)=(Tex=Texture'DeusExCharacters.Skins.MechanicTex1',Tag1="Tech")
    Torsos(27)=(Tex=Texture'DeusExCharacters.Skins.MIBTex1',Tag1="Formal")
    Torsos(28)=(Tex=Texture'DeusExCharacters.Skins.MichaelHamnerTex1',Tag1="Formal")
    Torsos(29)=(Tex=Texture'DeusExCharacters.Skins.MorganEverettTex1',Tag1="Normal")
    Torsos(30)=(Tex=Texture'DeusExCharacters.Skins.NathanMadisonTex1',Tag1="Military")
    Torsos(31)=(Tex=Texture'DeusExCharacters.Skins.PhilipMeadTex1',Tag1="Formal")
    Torsos(32)=(Tex=Texture'DeusExCharacters.Skins.RiotCopTex2',Tag1="Police",Tag2="Military")
    Torsos(33)=(Tex=Texture'DeusExCharacters.Skins.SailorTex1',Tag1="Military")
    Torsos(34)=(Tex=Texture'DeusExCharacters.Skins.SamCarterTex1',Tag1="Military",SkinColor="Light")
    Torsos(35)=(Tex=Texture'DeusExCharacters.Skins.SecretServiceTex1',Tag1="Formal")
    Torsos(36)=(Tex=Texture'DeusExCharacters.Skins.SoldierTex1',Tag1="Military")
    Torsos(37)=(Tex=Texture'DeusExCharacters.Skins.TerroristTex1',Tag1="Military")
    Torsos(39)=(Tex=Texture'DeusExCharacters.Skins.ThugMale2Tex1',Tag1="Normal")
    Torsos(40)=(Tex=Texture'DeusExCharacters.Skins.ThugMale3Tex1',Tag1="Mech")
    Torsos(41)=(Tex=Texture'DeusExCharacters.Skins.TracerTongTex1',Tag1="Tech")
    Torsos(42)=(Tex=Texture'DeusExCharacters.Skins.TriadLumPathTex1',Tag1="Chinese")
    Torsos(43)=(Tex=Texture'DeusExCharacters.Skins.UNATCOTroopTex2',Tag1="Military")
    
    // Coat shirts
    CoatShirts(20)=(Tex=Texture'DeusExCharacters.Skins.GordonQuickTex1',Tag1="Party")
}
