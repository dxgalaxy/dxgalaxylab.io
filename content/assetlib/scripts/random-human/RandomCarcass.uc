class RandomCarcass extends DeusExCarcass;

var bool bRestore;

// -------------------------------
// The player frobbed this carcass
// -------------------------------
function Frob(Actor aFrobber, Inventory FrobWith)
{
    local DeusExPlayer aPlayer;
    local RandomCarcassProxy aProxy;

    Super.Frob(aFrobber, FrobWith);
    
    aPlayer = DeusExPlayer(aFrobber);
    
    if(aPlayer == None || aPlayer.InHand != None)
        return;

    aProxy = Spawn(class'MyPackage.RandomCarcassProxy');

    if(aProxy == None)
        return;

    aProxy.InitFor(Self);
    aProxy.GiveTo(aPlayer);
}

// ------------------------------
// Restores the mesh and textures
// ------------------------------
function Restore()
{
    local RandomCarcassProxy aProxy;
    local int i;

    aProxy = RandomCarcassProxy(DeusExPlayer(GetPlayerPawn()).FindInventoryType(class'MyPackage.RandomCarcassProxy'));
        
    if(aProxy != None)
    {
        Mesh = aProxy.Mesh2; // Dropped corpses are always facing down 
        Mesh2 = aProxy.Mesh2; 
        Mesh3 = aProxy.Mesh3;

        for(i = 0; i < ArrayCount(aProxy.MultiSkins); i++)
        {
            MultiSkins[i] = aProxy.MultiSkins[i];
        }
    
        DeusExPlayer(GetPlayerPawn()).RemoveInventoryType(class'MyPackage.RandomCarcassProxy');
    }
}

// ----------------------------------------------
// Adopts values from a pawn to match their looks
// ----------------------------------------------
function InitFor(Actor aOther)
{
    local RandomHuman aRandomHuman;
    local int i;

    bRestore = False;

    aRandomHuman = RandomHuman(aOther);

    if(aRandomHuman != None)
    {
        
        Mesh = LodMesh(DynamicLoadObject("DeusExCharacters." $ aRandomHuman.Mesh.Name $ "_Carcass", class'LodMesh'));
        Mesh2 = LodMesh(DynamicLoadObject("DeusExCharacters." $ aRandomHuman.Mesh.Name $ "_CarcassB", class'LodMesh'));
        Mesh3 = LodMesh(DynamicLoadObject("DeusExCharacters." $ aRandomHuman.Mesh.Name $ "_CarcassC", class'LodMesh'));

        for(i = 0; i < ArrayCount(MultiSkins); i++)
        {
            MultiSkins[i] = aRandomHuman.MultiSkins[i];
        }
    }

    Super.InitFor(aOther);
}

auto state Dead
{
Begin:
    if(bRestore)
        Restore();

    while(Physics == PHYS_Falling)
    {
        Sleep(1.0);
    }
    
    HandleLanding();
}

defaultproperties
{
     bRestore=True
     CollisionRadius=40.000000
}
