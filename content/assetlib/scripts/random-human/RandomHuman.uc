//=================================================================
// RandomHuman
// 
// Generates a random human based on some parameters.
//
// NOTE: The struct properties had to be strings instead of names,
//       because for some reason they would not serialise properly.
//       The performance impact is negligible, since the generation
//       only happens on PostBeginPlay(). 
//=================================================================
class RandomHuman extends ScriptedPawn;

enum eBodyType
{
    BODY_Normal,
    BODY_Fat,
    BODY_Skinny
};

enum eHeadGear
{
    HG_None,
    HG_CapForward,
    HG_CapBackward
};

enum eGenerationStatus
{
    GS_Waiting,
    GS_Success,
    GS_Fail
};

struct sTextureInfo
{
    var Texture Tex;
    var string Tag1;
    var string Tag2;
    var string Tag3;
    var string SkinColor;
    var bool bCelebrity;
    var eHeadGear HeadGear;
};

// Adjustable parameters
var() bool bNoCelebrities;          // Exclude known faces
var() string Tags[10];              // Allowed style tags

// Internal variables
var sTextureInfo Frames[100];
var sTextureInfo Lenses[100];
var sTextureInfo Coats[100];
var sTextureInfo CoatShirts[100];
var sTextureInfo Pants[100];
var Texture PinkMask;
var Texture GrayMask;
var Texture BlackMask;
var eGenerationStatus GenerationStatus;
var string FinalTag; 

// ---------
// Init pawn
// ---------
function InitializePawn()
{
    if(!bInitialized)
    {
        StartGeneration();
    }

    Super.InitializePawn();
}

// ----------------
// Start generation
// ----------------
function StartGeneration()
{
    local int Attempts;

    // Generation already happened, probably via a spawner
    if(GenerationStatus != GS_Waiting)
        return;

    // Keep trying to generate a few times
    while(GenerationStatus != GS_Success && Attempts < 100)
    {
        Attempts++;
        GenerationStatus = GS_Success;
        FinalTag = GetRandomEnabledTag();
        Generate();
    }

    // If it didn't work out, self-destruct
    if(GenerationStatus == GS_Fail)
    {
        Destroy();
        return;
    }
}

// -----------------------
// Generates the character
// -----------------------
function Generate()
{

}

// -----------------------------
// Gets whether a tag is enabled
// -----------------------------
function bool IsTagEnabled(string TagName)
{
    local int i;

    if(TagName == "")
        return False;

    if(FinalTag != "")
        return FinalTag == TagName;

    for(i = 0; i < ArrayCount(Tags); i++)
    {
        if(Tags[i] == TagName)
            return True;
    }

    return False;
}

// ------------------------------------------
// Gets whether a TextureInfo has a given tag
// ------------------------------------------
function bool HasTag(sTextureInfo TextureInfo, string TagName)
{
    if(TextureInfo.Tag1 == TagName)
        return True;

    if(TextureInfo.Tag2 == TagName)
        return True;
    
    if(TextureInfo.Tag3 == TagName)
        return True;

    return False;
}

// -------------------------------------------------
// Gets whether a TextureInfo has any tags specified
// -------------------------------------------------
function bool HasTags(sTextureInfo TextureInfo)
{
    if(TextureInfo.Tag1 != "")
        return True;

    if(TextureInfo.Tag2 != "")
        return True;
    
    if(TextureInfo.Tag3 != "")
        return True;

    return False;
}

// -----------------------------------------
// Gets whether any tag in a list is enabled
// -----------------------------------------
function bool AreAnyTagsEnabled(sTextureInfo TextureInfo)
{
    if(IsTagEnabled(TextureInfo.Tag1))
        return True;
    
    if(IsTagEnabled(TextureInfo.Tag2))
        return True;
    
    if(IsTagEnabled(TextureInfo.Tag3))
        return True;

    return False;
}

// -------------------------
// Gets a random enabled tag
// -------------------------
function string GetRandomEnabledTag()
{
    local int i, NumValidTags;
    local string ValidTags[10];

    for(i = 0; i < ArrayCount(Tags); i++)
    {
        if(Tags[i] == "")
            continue;

        ValidTags[NumValidTags] = Tags[i];
        NumValidTags++;
    }

    if(NumValidTags < 1)
        return "";

    return ValidTags[Rand(NumValidTags)];
}

// ------------------------------------
// Gets a random tag from a TextureInfo
// ------------------------------------
function string GetRandomTag(sTextureInfo TextureInfo)
{
    local int NumValidTags;
    local string ValidTags[3];

    if(TextureInfo.Tag1 != "")
    {
        ValidTags[NumValidTags] = TextureInfo.Tag1;
        NumValidTags++;
    }
    
    if(TextureInfo.Tag2 != "")
    {
        ValidTags[NumValidTags] = TextureInfo.Tag2;
        NumValidTags++;
    }
    
    if(TextureInfo.Tag3 != "")
    {
        ValidTags[NumValidTags] = TextureInfo.Tag3;
        NumValidTags++;
    }

    if(NumValidTags < 1)
        return "";

    return ValidTags[Rand(NumValidTags)];
}

// ------------------------------------------------
// Gets a random Texture from a list of TextureInfo
// ------------------------------------------------
function Texture GetRandomTexture(out sTextureInfo TextureInfos[100], optional string SkinColor)
{
    local sTextureInfo TextureInfo;

    TextureInfo = GetRandomTextureInfo(TextureInfos, SkinColor);
    
    return TextureInfo.Tex;
}

// ----------------------------------------------------
// Gets a random TextureInfo from a list of TextureInfo
// ----------------------------------------------------
function sTextureInfo GetRandomTextureInfo(out sTextureInfo TextureInfos[100], optional string SkinColor)
{
    local int i;
    local int NumValidTextureInfos;
    local sTextureInfo ValidTextureInfos[100];
    local sTextureInfo Result;

    // Find valid textures
    for(i = 0; i < ArrayCount(TextureInfos); i++)
    {
        // Skip null textures
        if(TextureInfos[i].Tex == None)
            continue;

        // Skip celebrities if we don't want them
        if(bNoCelebrities && TextureInfos[i].bCelebrity)
            continue;

        // Skip if the skin colour is specified and doesn't match
        if(SkinColor != "" && TextureInfos[i].SkinColor != "" && TextureInfos[i].SkinColor != SkinColor)
            continue;

        // Skip if this texture has tags and they're not enabled
        if(HasTags(TextureInfos[i]) && !AreAnyTagsEnabled(TextureInfos[i]))
            continue;
  
        ValidTextureInfos[NumValidTextureInfos] = TextureInfos[i]; 
        NumValidTextureInfos++; 
    }

    // Oops! No valid textures. Return an empty texture and set fail status.
    if(NumValidTextureInfos < 1)
    {
        GenerationStatus = GS_Fail;
        return ValidTextureInfos[0];
    }

    Result = ValidTextureInfos[Rand(NumValidTextureInfos)];

    return Result;
}

defaultproperties
{
    BindName="Person"
    FamiliarName="Person"
    CarcassType=Class'MyPackage.RandomCarcass'
    UnfamiliarName="Person"
    BaseAccuracy=1.200000
    maxRange=400.000000
    MinHealth=40.000000
    bPlayIdle=True
    bAvoidAim=False
    bReactProjectiles=False
    bFearShot=True
    bFearIndirectInjury=True
    bFearCarcass=True
    bFearDistress=True
    bFearAlarm=True
    EnemyTimeout=1.500000
    bCanTurnHead=True
    bCanStrafe=False
    WaterSpeed=80.000000
    AirSpeed=160.000000
    AccelRate=500.000000
    BaseEyeHeight=40.000000
    UnderWaterTime=20.000000
    AttitudeToPlayer=ATTITUDE_Ignore
    VisibilityThreshold=0.010000
    DrawType=DT_Mesh
    Mass=150.000000
    Buoyancy=155.000000

    bNoCelebrities=True
    Tags(0)="Normal"
    
    // Basic textures
    PinkMask=Texture'DeusExItems.Skins.PinkMaskTex'
    GrayMask=Texture'DeusExItems.Skins.GrayMaskTex'
    BlackMask=Texture'DeusExItems.Skins.BlackMaskTex'

    // Frames
    Frames(0)=(Tex=Texture'DeusExCharacters.Skins.FramesTex1')
    Frames(1)=(Tex=Texture'DeusExCharacters.Skins.FramesTex2')
    Frames(2)=(Tex=Texture'DeusExCharacters.Skins.FramesTex3')
    Frames(3)=(Tex=Texture'DeusExCharacters.Skins.FramesTex4')
    Frames(4)=(Tex=Texture'DeusExCharacters.Skins.FramesTex5')
    
    // Lenses
    Lenses(0)=(Tex=Texture'DeusExCharacters.Skins.LensesTex1')
    Lenses(1)=(Tex=Texture'DeusExCharacters.Skins.LensesTex2')
    Lenses(2)=(Tex=Texture'DeusExCharacters.Skins.LensesTex3')
    Lenses(3)=(Tex=Texture'DeusExCharacters.Skins.LensesTex4')
    Lenses(4)=(Tex=Texture'DeusExCharacters.Skins.LensesTex5')
    Lenses(5)=(Tex=Texture'DeusExCharacters.Skins.LensesTex6')
    
    // Coats
    Coats(0)=(Tex=Texture'DeusExCharacters.Skins.BumMale3Tex2',Tag1="Bum")
    Coats(1)=(Tex=Texture'DeusExCharacters.Skins.BumMaleTex2',Tag1="Bum")
    Coats(2)=(Tex=Texture'DeusExCharacters.Skins.FordSchickTex2',Tag1="Normal")
    Coats(3)=(Tex=Texture'DeusExCharacters.Skins.Female4Tex2',Tag1="Normal")
    Coats(4)=(Tex=Texture'DeusExCharacters.Skins.FordSchickTex2',Tag1="Normal")
    Coats(5)=(Tex=Texture'DeusExCharacters.Skins.GilbertRentonTex2',Tag1="Normal")
    Coats(6)=(Tex=Texture'DeusExCharacters.Skins.GordonQuickTex2',Tag1="Chinese")
    Coats(7)=(Tex=Texture'DeusExCharacters.Skins.HarleyFilbenTex2',Tag1="Bum")
    Coats(8)=(Tex=Texture'DeusExCharacters.Skins.JCDentonTex2',Tag1="Normal")
    Coats(9)=(Tex=Texture'DeusExCharacters.Skins.JockTex2',Tag1="Normal")
    Coats(10)=(Tex=Texture'DeusExCharacters.Skins.JosephManderleyTex2',Tag1="Formal")
    Coats(11)=(Tex=Texture'DeusExCharacters.Skins.JuanLebedevTex2',Tag1="Normal")
    Coats(12)=(Tex=Texture'DeusExCharacters.Skins.LabCoatTex1',Tag1="Medical",Tag2="Science")
    Coats(13)=(Tex=Texture'DeusExCharacters.Skins.MaxChenTex2',Tag1="Chinese")
    Coats(14)=(Tex=Texture'DeusExCharacters.Skins.PaulDentonTex2',Tag1="Normal")
    Coats(15)=(Tex=Texture'DeusExCharacters.Skins.ScientistFemaleTex2',Tag1="Medical",Tag2="Science")
    Coats(16)=(Tex=Texture'DeusExCharacters.Skins.SmugglerTex2',Tag1="Normal")
    Coats(17)=(Tex=Texture'DeusExCharacters.Skins.StantonDowdTex2',Tag1="Normal")
    Coats(18)=(Tex=Texture'DeusExCharacters.Skins.ThugMaleTex2',Tag1="Normal")
    Coats(19)=(Tex=Texture'DeusExCharacters.Skins.TobyAtanweTex2',Tag1="Normal")
    Coats(20)=(Tex=Texture'DeusExCharacters.Skins.TrenchCoatTex2',Tag1="Normal")
    Coats(21)=(Tex=Texture'DeusExCharacters.Skins.TriadRedArrowTex2',Tag1="Normal")
    Coats(22)=(Tex=Texture'DeusExCharacters.Skins.WaltonSimonsTex2',Tag1="Normal")

    // Coat shirts
    CoatShirts(0)=(Tex=Texture'DeusExCharacters.Skins.DoctorTex1',Tag1="Formal",Tag2="Medical",Tag3="Science")
    CoatShirts(1)=(Tex=Texture'DeusExCharacters.Skins.FordSchickTex1',Tag1="Normal")
    CoatShirts(2)=(Tex=Texture'DeusExCharacters.Skins.GarySavageTex1',Tag1="Formal",Tag2="Medical",Tag3="Science")
    CoatShirts(3)=(Tex=Texture'DeusExCharacters.Skins.GilbertRentonTex1',Tag1="Normal")
    CoatShirts(4)=(Tex=Texture'DeusExCharacters.Skins.JaimeReyesTex1',Tag1="Formal",Tag2="Medical",Tag3="Science")
    CoatShirts(5)=(Tex=Texture'DeusExCharacters.Skins.JCDentonTex1',Tag1="Military")
    CoatShirts(6)=(Tex=Texture'DeusExCharacters.Skins.JockTex1',Tag1="Normal",Tag2="Military")
    CoatShirts(7)=(Tex=Texture'DeusExCharacters.Skins.JosephManderleyTex1',Tag1="Formal")
    CoatShirts(8)=(Tex=Texture'DeusExCharacters.Skins.JuanLebedevTex1',Tag1="Military",Tag2="Guerilla")
    CoatShirts(9)=(Tex=Texture'DeusExCharacters.Skins.MaxChenTex1',Tag1="Chinese")
    CoatShirts(10)=(Tex=Texture'DeusExCharacters.Skins.PaulDentonTex1',Tag1="Normal")
    CoatShirts(11)=(Tex=Texture'DeusExCharacters.Skins.SmugglerTex1',Tag1="Normal")
    CoatShirts(12)=(Tex=Texture'DeusExCharacters.Skins.StantonDowdTex1',Tag1="Normal")
    CoatShirts(13)=(Tex=Texture'DeusExCharacters.Skins.ThugMaleTex1',Tag1="Normal")
    CoatShirts(14)=(Tex=Texture'DeusExCharacters.Skins.TobyAtanweTex1',Tag1="Military")
    CoatShirts(15)=(Tex=Texture'DeusExCharacters.Skins.TrenchShirtTex1',Tag1="Bum")
    CoatShirts(16)=(Tex=Texture'DeusExCharacters.Skins.TrenchShirtTex2',Tag1="Bum")
    CoatShirts(17)=(Tex=Texture'DeusExCharacters.Skins.TrenchShirtTex3',Tag1="Normal")
    CoatShirts(18)=(Tex=Texture'DeusExCharacters.Skins.TriadRedArrowTex1',Tag1="Normal")
    CoatShirts(19)=(Tex=Texture'DeusExCharacters.Skins.WaltonSimonsTex1',Tag1="Formal")

    // Pants
    Pants(0)=(Tex=Texture'DeusExCharacters.Skins.AlexJacobsonTex2',Tag1="Tech")
    Pants(1)=(Tex=Texture'DeusExCharacters.Skins.BobPageTex2',Tag1="Normal")
    Pants(2)=(Tex=Texture'DeusExCharacters.Skins.Businessman1Tex2',Tag1="Normal",Tag2="Formal")
    Pants(3)=(Tex=Texture'DeusExCharacters.Skins.Businessman2Tex2',Tag1="Formal")
    Pants(4)=(Tex=Texture'DeusExCharacters.Skins.Businessman3Tex2',Tag1="Formal")
    Pants(5)=(Tex=Texture'DeusExCharacters.Skins.ChadTex2',Tag1="Normal")
    Pants(6)=(Tex=Texture'DeusExCharacters.Skins.CopTex2',Tag1="Police")
    Pants(7)=(Tex=Texture'DeusExCharacters.Skins.GordonQuickTex3',Tag1="Normal")
    Pants(8)=(Tex=Texture'DeusExCharacters.Skins.HowardStrongTex2',Tag1="Normal")
    Pants(9)=(Tex=Texture'DeusExCharacters.Skins.JanitorTex2',Tag1="Normal")
    Pants(10)=(Tex=Texture'DeusExCharacters.Skins.JCDentonTex3',Tag1="Police",Tag2="Military")
    Pants(11)=(Tex=Texture'DeusExCharacters.Skins.JockTex3',Tag1="Military")
    Pants(12)=(Tex=Texture'DeusExCharacters.Skins.JoJoFineTex2',Tag1="Normal")
    Pants(13)=(Tex=Texture'DeusExCharacters.Skins.JuanLebedevTex3',Tag1="Military")
    Pants(14)=(Tex=Texture'DeusExCharacters.Skins.JunkieFemaleTex2',Tag1="Bum")
    Pants(15)=(Tex=Texture'DeusExCharacters.Skins.JunkieMaleTex2',Tag1="Bum")
    Pants(16)=(Tex=Texture'DeusExCharacters.Skins.LowerClassMale2Tex2',Tag1="Normal")
    Pants(17)=(Tex=Texture'DeusExCharacters.Skins.Male2Tex2',Tag1="Normal")
    Pants(18)=(Tex=Texture'DeusExCharacters.Skins.Male4Tex2',Tag1="Chinese")
    Pants(19)=(Tex=Texture'DeusExCharacters.Skins.MechanicTex2',Tag1="Tech")
    Pants(20)=(Tex=Texture'DeusExCharacters.Skins.MichaelHamnerTex2',Tag1="Formal")
    Pants(21)=(Tex=Texture'DeusExCharacters.Skins.MJ12TroopTex1',Tag1="Military")
    Pants(22)=(Tex=Texture'DeusExCharacters.Skins.NathanMadisonTex2',Tag1="Military")
    Pants(23)=(Tex=Texture'DeusExCharacters.Skins.PantsTex1',Tag1="Normal")
    Pants(24)=(Tex=Texture'DeusExCharacters.Skins.PantsTex2',Tag1="Normal")
    Pants(25)=(Tex=Texture'DeusExCharacters.Skins.PantsTex3',Tag1="Normal")
    Pants(26)=(Tex=Texture'DeusExCharacters.Skins.PantsTex4',Tag1="Normal")
    Pants(27)=(Tex=Texture'DeusExCharacters.Skins.PantsTex5',Tag1="Normal")
    Pants(28)=(Tex=Texture'DeusExCharacters.Skins.PantsTex6',Tag1="Normal")
    Pants(29)=(Tex=Texture'DeusExCharacters.Skins.PantsTex7',Tag1="Normal")
    Pants(30)=(Tex=Texture'DeusExCharacters.Skins.PantsTex8',Tag1="Normal")
    Pants(31)=(Tex=Texture'DeusExCharacters.Skins.PantsTex9',Tag1="Mech")
    Pants(32)=(Tex=Texture'DeusExCharacters.Skins.PantsTex10',Tag1="Normal")
    Pants(33)=(Tex=Texture'DeusExCharacters.Skins.RiotCopTex1',Tag1="Police")
    Pants(34)=(Tex=Texture'DeusExCharacters.Skins.SailorTex2',Tag1="Normal")
    Pants(35)=(Tex=Texture'DeusExCharacters.Skins.SamCarterTex2',Tag1="Normal")
    Pants(36)=(Tex=Texture'DeusExCharacters.Skins.SecretServiceTex2',Tag1="Formal")
    Pants(37)=(Tex=Texture'DeusExCharacters.Skins.SoldierTex2',Tag1="Military")
    Pants(38)=(Tex=Texture'DeusExCharacters.Skins.TerroristTex2',Tag1="Military",Tag2="Guerilla")
    Pants(39)=(Tex=Texture'DeusExCharacters.Skins.ThugMale2Tex2',Tag1="Normal",Tag2="Guerilla")
    Pants(40)=(Tex=Texture'DeusExCharacters.Skins.ThugMale3Tex2',Tag1="Military",Tag2="Guerilla")
    Pants(41)=(Tex=Texture'DeusExCharacters.Skins.ThugMaleTex3',Tag1="Normal",Tag2="Guerilla")
    Pants(42)=(Tex=Texture'DeusExCharacters.Skins.TiffanySavageTex2',Tag1="Military")
    Pants(43)=(Tex=Texture'DeusExCharacters.Skins.TracerTongTex3',Tag1="Tech")
    Pants(44)=(Tex=Texture'DeusExCharacters.Skins.TriadRedArrowTex3',Tag1="Normal")
    Pants(45)=(Tex=Texture'DeusExCharacters.Skins.UNATCOTroopTex1',Tag1="Military")
}
