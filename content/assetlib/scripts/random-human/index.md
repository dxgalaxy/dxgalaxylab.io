---
title:          Random Human
description:    This is a type of ScriptedPawn that randomises appearance
author:         Artifechs
image:          screenshot.jpg
---


!!! Important
    Remember to replace `MyPackage` with your package name


@downloads(Files)
    files:
        - RandomCarcass.uc
        - RandomCarcassProxy.uc
        - RandomFemale.uc
        - RandomHuman.uc
        - RandomHumanSpawner.uc
        - RandomMale.uc


## Features

- Specify clothing style with tags
- Toggle celebrities (key character faces) on/off
- Spawn a crowd of randomised characters
- Assign orders to groups of characters


## Instructions

@properties(RandomHumanSpawner)
    RandomHumanSpawner:
        Amount:                     Amount of characters to spawn
        bNoCelebrities:             Exclude known character faces
        Events:
            Event:                  The tag of PathNodes to spawn characters at
        PawnOrders:                 
            Amount:                 Amount of characters to issue this order to
            Orders:                 The orders, like "patrolling" or "sitting"
            OrderTag:               Order tag, like of a patrol point
            OrderTagSuffixMax:      Upper range of a numerical order tag suffix
            OrderTagSuffixMin:      Lower range of a numerical order tag suffix
        Tags:                       Keywords used to specify character clothing¹

@properties(RandomHuman)
    RandomHuman:
        bNoCelebrities:             Exclude known character faces
        Tags:                       Keywords used to specify character clothing¹

¹ Available tags:

- Bum
- Chinese
- Formal
- Mech 
- Medical 
- Military
- Normal
- Party
- Police
- Science 
- Tech 
