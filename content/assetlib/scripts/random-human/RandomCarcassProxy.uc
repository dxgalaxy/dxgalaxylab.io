class RandomCarcassProxy extends DeusExPickup;

var Mesh Mesh2;
var Mesh Mesh3;

// -------------------------------------------
// Stores the information from a RandomCarcass
// -------------------------------------------
function InitFor(RandomCarcass aCarcass)
{
    local int i;

    Mesh = aCarcass.Mesh; 
    Mesh2 = aCarcass.Mesh2; 
    Mesh3 = aCarcass.Mesh3;
        
    for(i = 0; i < ArrayCount(aCarcass.MultiSkins); i++)
    {
        MultiSkins[i] = aCarcass.MultiSkins[i];
    }
}
