class RandomFemale extends RandomHuman;

var sTextureInfo Dresses[100];
var sTextureInfo Heads[100];
var sTextureInfo Legs[100];
var sTextureInfo Leggings[100];
var sTextureInfo Skirts[100];
var sTextureInfo Torsos[100];

// -----------------------
// Generates the character
// -----------------------
function Generate()
{
    switch(Rand(5))
    {
        case 0:
            GenerateGFMDress();
            break;

        case 1:
            GenerateGFMSuitSkirt(BODY_Normal);
            break;
        
        case 2:
            GenerateGFMSuitSkirt(BODY_Fat);
            break;
        
        case 3:
            GenerateGFMTrench();
            break;
        
        case 4:
            GenerateGFMTShirtPants();
            break;
    }
}

// ---------------
// GFM_TShirtPants
// 0: Head
// 1: Hair sides
// 2: Ponytail
// 3: Frames
// 4: Lenses
// 5: ???
// 6: Legs
// 7: Torso
// ---------------
function GenerateGFMTShirtPants()
{
    local sTextureInfo HeadTextureInfo;

    Mesh = LodMesh'DeusExCharacters.GFM_TShirtPants';

    // Head
    HeadTextureInfo = GetRandomTextureInfo(Heads);

    MultiSkins[1] = PinkMask;
    MultiSkins[2] = PinkMask;
    MultiSkins[0] = HeadTextureInfo.Tex; 

    // Ponytail
    if(Rand(2) > 0)
        MultiSkins[2] = MultiSkins[0];

    // Side hair
    else if(Rand(2) > 0)
        MultiSkins[1] = MultiSkins[0];
    
    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[3] = GetRandomTexture(Frames);   
        MultiSkins[4] = GetRandomTexture(Lenses);   
    }
    else
    {
        MultiSkins[3] = GrayMask;
        MultiSkins[4] = BlackMask;
    }

    // Torso
    MultiSkins[7] = GetRandomTexture(Torsos, HeadTextureInfo.SkinColor);
   
    // Legs
    MultiSkins[6] = GetRandomTexture(Pants, HeadTextureInfo.SkinColor);
}

// ----------------------
// GFM_Trench
//
// 0: Head
// 1: Torso 
// 2: Legs
// 3: ???
// 4: Shirt
// 5: Coattail and collar
// 6: Frames
// 7: Lenses
// ----------------------
function GenerateGFMTrench()
{
    local sTextureInfo HeadTextureInfo;
   
    Mesh = LodMesh'DeusExCharacters.GFM_Trench';

    // Head
    HeadTextureInfo = GetRandomTextureInfo(Heads);

    MultiSkins[0] = HeadTextureInfo.Tex; 
    
    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[6] = GetRandomTexture(Frames);   
        MultiSkins[7] = GetRandomTexture(Lenses);   
    }
    else
    {
        MultiSkins[6] = GrayMask;
        MultiSkins[7] = BlackMask;
    }

    // Coat
    MultiSkins[4] = GetRandomTexture(CoatShirts);
    MultiSkins[1] = GetRandomTexture(Coats);
    MultiSkins[5] = MultiSkins[1];

    // Leggings
    MultiSkins[2] = GetRandomTexture(Leggings, HeadTextureInfo.SkinColor);
}

// -------------
// GFM_SuitSkirt
//
// 0: Head
// 1: Bun
// 2: ???
// 3: Legs 
// 4: Torso
// 5: Skirt 
// 6: Frames
// 7: Lenses
// -------------
function GenerateGFMSuitSkirt(eBodyType BodyType)
{
    local sTextureInfo HeadTextureInfo;
    
    if(BodyType == BODY_Fat)
        Mesh = LodMesh'DeusExCharacters.GFM_SuitSkirt_F';
    else
        Mesh = LodMesh'DeusExCharacters.GFM_SuitSkirt';

    // Head
    HeadTextureInfo = GetRandomTextureInfo(Heads);

    MultiSkins[0] = HeadTextureInfo.Tex; 
    
    // Bun
    if(Rand(2) > 0)
        MultiSkins[1] = MultiSkins[0];
    else
        MultiSkins[1] = PinkMask;

    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[6] = GetRandomTexture(Frames);   
        MultiSkins[7] = GetRandomTexture(Lenses);   
    }
    else
    {
        MultiSkins[6] = GrayMask;
        MultiSkins[7] = BlackMask;
    }

    // Dress
    MultiSkins[4] = GetRandomTexture(Dresses, HeadTextureInfo.SkinColor);
    MultiSkins[5] = MultiSkins[4];

    // Legs
    MultiSkins[3] = GetRandomTexture(Legs, HeadTextureInfo.SkinColor);
}

// -------------
// GFM_Dress
//
// 0: ???
// 1: Legs
// 2: Waist
// 3: Torso
// 4: Skirt
// 5: Hair sides
// 6: Ponytail
// 7: Head
// -------------
function GenerateGFMDress()
{
    local sTextureInfo HeadTextureInfo;

    Mesh = LodMesh'DeusExCharacters.GFM_Dress';

    // Head
    HeadTextureInfo = GetRandomTextureInfo(Heads);

    MultiSkins[5] = PinkMask;
    MultiSkins[6] = PinkMask;
    MultiSkins[7] = HeadTextureInfo.Tex; 

    // Ponytail
    if(Rand(2) > 0)
        MultiSkins[6] = MultiSkins[7];

    // Side hair
    else if(Rand(2) > 0)
        MultiSkins[5] = MultiSkins[7];

    // Torso
    MultiSkins[3] = GetRandomTexture(Torsos, HeadTextureInfo.SkinColor);
   
    // Skirt
    MultiSkins[2] = GetRandomTexture(Skirts);
    MultiSkins[4] = MultiSkins[2];

    // Legs
    MultiSkins[1] = GetRandomTexture(Legs, HeadTextureInfo.SkinColor);
}

defaultproperties
{
    Mesh=LodMesh'DeusExCharacters.GFM_Dress'
    CollisionHeight=44.5
    HitSound1=Sound'DeusExSounds.Player.FemalePainSmall'
    HitSound2=Sound'DeusExSounds.Player.FemalePainMedium'
    Die=Sound'DeusExSounds.Player.FemaleDeath'
    BaseEyeHeight=38.000000
    BaseAssHeight=-18.000000
    bIsFemale=True
    GroundSpeed=120.000000
    WalkingSpeed=0.320000
    WalkAnimMult=0.650000
    BindName="RandomFemale"
    UnfamiliarName="Woman"
    FamiliarName="Woman"

    // Heads (NOTE: In vanilla DX, there are no black female faces)
    Heads(0)=(Tex=Texture'DeusExCharacters.Skins.AnnaNavarreTex0',bCelebrity=True,SkinColor="Pale",Tag1="Mech")
    Heads(1)=(Tex=Texture'DeusExCharacters.Skins.BumFemaleTex0',SkinColor="Light",Tag1="Bum")
    Heads(2)=(Tex=Texture'DeusExCharacters.Skins.Businesswoman1Tex0',SkinColor="Medium")
    Heads(3)=(Tex=Texture'DeusExCharacters.Skins.Female1Tex0',SkinColor="Light")
    Heads(4)=(Tex=Texture'DeusExCharacters.Skins.Female2Tex0',SkinColor="Light")
    Heads(5)=(Tex=Texture'DeusExCharacters.Skins.Hooker1Tex0',SkinColor="Medium")
    Heads(6)=(Tex=Texture'DeusExCharacters.Skins.Hooker2Tex0',SkinColor="Light")
    Heads(7)=(Tex=Texture'DeusExCharacters.Skins.JordanSheaTex0',SkinColor="Light",bCelebrity=True,Tag1="Mech")
    Heads(8)=(Tex=Texture'DeusExCharacters.Skins.JunkieFemaleTex0',SkinColor="Ghoul",Tag1="Bum")
    Heads(9)=(Tex=Texture'DeusExCharacters.Skins.LowerClassFemaleTex0',SkinColor="Medium")
    Heads(10)=(Tex=Texture'DeusExCharacters.Skins.MargaretWilliamsTex0',SkinColor="Light")
    Heads(11)=(Tex=Texture'DeusExCharacters.Skins.MaidTex0',SkinColor="Light")
    Heads(12)=(Tex=Texture'DeusExCharacters.Skins.NicoletteDuClareTex0',SkinColor="Light",bCelebrity=True)
    Heads(13)=(Tex=Texture'DeusExCharacters.Skins.NurseTex0',SkinColor="Light")
    Heads(14)=(Tex=Texture'DeusExCharacters.Skins.RachelMeadTex0',SkinColor="Light")
    Heads(15)=(Tex=Texture'DeusExCharacters.Skins.SandraRentonTex0',SkinColor="Light",bCelebrity=True)
    Heads(16)=(Tex=Texture'DeusExCharacters.Skins.SarahMeadTex0',SkinColor="Light")
    Heads(17)=(Tex=Texture'DeusExCharacters.Skins.ScientistFemaleTex0',SkinColor="Light")
    Heads(18)=(Tex=Texture'DeusExCharacters.Skins.SecretaryTex0',SkinColor="Light")
    Heads(19)=(Tex=Texture'DeusExCharacters.Skins.SkinTex5',SkinColor="Light")
    Heads(20)=(Tex=Texture'DeusExCharacters.Skins.TiffanySavageTex0',SkinColor="Light",bCelebrity=True,Tag1="Military")

    // Legs
    Legs(0)=(Tex=Texture'DeusExCharacters.Skins.Businesswoman1Tex2',SkinColor="Light",Tag1="Normal")
    Legs(1)=(Tex=Texture'DeusExCharacters.Skins.Female2Tex1',SkinColor="Light",Tag1="Normal")
    Legs(2)=(Tex=Texture'DeusExCharacters.Skins.Female3Tex2',SkinColor="Light",Tag1="Normal")
    Legs(3)=(Tex=Texture'DeusExCharacters.Skins.Female4Tex3',SkinColor="Light",Tag1="Party")
    Legs(4)=(Tex=Texture'DeusExCharacters.Skins.Hooker1Tex1',SkinColor="Medium",Tag1="Normal")
    Legs(5)=(Tex=Texture'DeusExCharacters.Skins.Hooker2Tex1',SkinColor="Light",Tag1="Normal")
    Legs(6)=(Tex=Texture'DeusExCharacters.Skins.LegsTex1',SkinColor="Light",Tag1="Normal")
    Legs(7)=(Tex=Texture'DeusExCharacters.Skins.LegsTex2',SkinColor="Darker",Tag1="Normal")
    Legs(8)=(Tex=Texture'DeusExCharacters.Skins.RachelMeadTex2',SkinColor="Light",Tag1="Normal")
    Legs(9)=(Tex=Texture'DeusExCharacters.Skins.SarahMeadTex3',SkinColor="Light",Tag1="Normal")
    Legs(10)=(Tex=Texture'DeusExCharacters.Skins.ScientistFemaleTex3',SkinColor="Light",Tag1="Normal")
    
    // Leggings (NOTE: In vanilla DX, only white people wear leggings, so we had to make the scientist leggings universal)
    Leggings(0)=(Tex=Texture'DeusExCharacters.Skins.Female4Tex3',SkinColor="Light",Tag1="Party")
    Leggings(1)=(Tex=Texture'DeusExCharacters.Skins.NicoletteDuClareTex3',SkinColor="Light",Tag1="Party")
    Leggings(2)=(Tex=Texture'DeusExCharacters.Skins.ScientistFemaleTex3',Tag1="Normal",Tag2="Formal",Tag3="Military")
    
    // Torsos
    Torsos(0)=(Tex=Texture'DeusExCharacters.Skins.AnnaNavarreTex1',SkinColor="Light",Tag1="Mech")
    Torsos(1)=(Tex=Texture'DeusExCharacters.Skins.BumFemaleTex1',SkinColor="Light",Tag1="Bum")
    Torsos(2)=(Tex=Texture'DeusExCharacters.Skins.Female1Tex1',SkinColor="Light",Tag1="Normal")
    Torsos(3)=(Tex=Texture'DeusExCharacters.Skins.Hooker1Tex3',SkinColor="Medium",Tag1="Party")
    Torsos(4)=(Tex=Texture'DeusExCharacters.Skins.Hooker2Tex3',SkinColor="Light",Tag1="Party")
    Torsos(5)=(Tex=Texture'DeusExCharacters.Skins.JordanSheaTex1',SkinColor="Light",Tag1="Mech")
    Torsos(6)=(Tex=Texture'DeusExCharacters.Skins.JunkieFemaleTex1',SkinColor="Ghoul",Tag1="Bum")
    Torsos(7)=(Tex=Texture'DeusExCharacters.Skins.LowerClassFemaleTex1',SkinColor="Medium",Tag1="Normal")
    Torsos(8)=(Tex=Texture'DeusExCharacters.Skins.NicoletteDuClareTex1',SkinColor="Light",Tag1="Party")
    Torsos(9)=(Tex=Texture'DeusExCharacters.Skins.SandraRentonTex1',SkinColor="Light",Tag1="Bum")
    Torsos(10)=(Tex=Texture'DeusExCharacters.Skins.SarahMeadTex1',SkinColor="Light",Tag1="Formal")
    Torsos(11)=(Tex=Texture'DeusExCharacters.Skins.TiffanySavageTex1',Tag1="Military")
    
    // Skirts
    Skirts(0)=(Tex=Texture'DeusExCharacters.Skins.Hooker1Tex2',Tag1="Party")
    Skirts(1)=(Tex=Texture'DeusExCharacters.Skins.Hooker2Tex2',Tag1="Party")
    Skirts(2)=(Tex=Texture'DeusExCharacters.Skins.NicoletteDuClareTex2',Tag1="Party")
    Skirts(3)=(Tex=Texture'DeusExCharacters.Skins.SarahMeadTex2',Tag1="Normal")

    // Dresses
    Dresses(0)=(Tex=Texture'DeusExCharacters.Skins.Businesswoman1Tex1',Tag1="Formal")
    Dresses(1)=(Tex=Texture'DeusExCharacters.Skins.Female2Tex2',Tag1="Normal")
    Dresses(2)=(Tex=Texture'DeusExCharacters.Skins.Female3Tex1',Tag1="Chinese")
    Dresses(3)=(Tex=Texture'DeusExCharacters.Skins.MaggieChowTex1',Tag1="Chinese")
    Dresses(4)=(Tex=Texture'DeusExCharacters.Skins.MaidTex1',Tag1="Servant")
    Dresses(5)=(Tex=Texture'DeusExCharacters.Skins.MargaretWilliamsTex1',Tag1="Formal")
    Dresses(6)=(Tex=Texture'DeusExCharacters.Skins.NurseTex1',Tag1="Medical",SkinColor="Light")
    Dresses(7)=(Tex=Texture'DeusExCharacters.Skins.RachelMeadTex1',Tag1="Normal")
    Dresses(8)=(Tex=Texture'DeusExCharacters.Skins.SecretaryTex2',Tag1="Formal")
    Dresses(9)=(Tex=Texture'DeusExCharacters.Skins.WIBTex1',Tag1="Formal")

    // Coat shirts
    CoatShirts(20)=(Tex=Texture'DeusExCharacters.Skins.Female4Tex1',Tag1="Party")
}
