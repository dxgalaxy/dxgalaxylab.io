---
title:          Move Trigger
description:    Moves an actor to a location and sets its rotation
author:         Artifechs
---

## MoveTrigger.uc

    class MoveTrigger extends Trigger;

    var() bool bThisLocation;
    var() Name LocationTag;
    var() Vector NewLocation;
    var() Rotator NewRotation;

    function Trigger(Actor Other, Pawn Instigator)
    {
        local Actor A;
        local Actor Target;

        if (Event != '')
        {
            foreach AllActors(class 'Actor', A, Event)
            {
                if(bThisLocation)
                {
                    MoveActor(A, Location, Rotation);
                }
                else if(LocationTag != '')
                {
                    foreach AllActors(class 'Actor', Target, LocationTag)
                    {
                        MoveActor(A, Target.Location, Target.Rotation);
                    }
                }
                else
                {
                    MoveActor(A, NewLocation, NewRotation);
                }
            }
        }

        Super.Trigger(Other, Instigator);
    }

    function MoveActor(Actor A, Vector Loc, Rotator Rot)
    {
        local Rotator HelpRotator;
        local int HelpDistance;
        local int LoopCount;

        HelpRotator = Rot;
        HelpDistance = 16;

        // Try to place tha actor nearby if it fails
        if(!A.SetLocation(Loc))
        {
            while (LoopCount < 16 && !A.SetLocation(Loc + Normal(Vector(HelpRotator)) * HelpDistance))
            {
                HelpRotator.Yaw += 8192;
                HelpDistance += 16;
                LoopCount++;
            }
        }
        
        A.SetRotation(Rot);
    }
