//=============================================================================
// MissionScriptTrigger - Triggers events in the MissionScript
//=============================================================================
class MissionScriptTrigger extends Trigger;

var Actor aOther;

function Touch(Actor Other)
{
    if(!IsRelevant(Other))
        return;

    Trigger(Other, Pawn(Other));
    
    if(bTriggerOnceOnly)
        SetCollision(False);
}

function Trigger(Actor Other, Pawn Instigator)
{
    local MissionScript aMissionScript;

    aOther = Other;
    
    foreach AllActors(class'MissionScript', aMissionScript)
    {
        if(MissionScriptExtended(aMissionScript) != None)
            MissionScriptExtended(aMissionScript).SetEvent(Event, Other, Instigator);
        else
            aMissionScript.Trigger(Self, Instigator);
    }

    Super.Trigger(Other, Instigator);
}
