class HumanExtended extends Human;

// Outfits
struct PlayerOutfit
{
    var string Name;
    var Mesh Mesh;
    var Texture Tex0;
    var Texture Tex1;
    var Texture Tex2;
    var Texture Tex3;
    var Texture Tex4;
    var Texture Tex5;
    var Texture Tex6;
    var Texture Tex7;
};

var PlayerOutfit Outfits[10];
var bool bMuteMusic;
var bool bIgnoreAllShowMenu;
var travel string CurrentOutfit;

// -------------------
// Shows the main menu
// -------------------
exec function ShowMainMenu()
{
    if(bIgnoreAllShowMenu)
        return;

    Super.ShowMainMenu();
}

// ----------------------------------------------------------------
// Fixed this function, so it actually returns a texture group name
// ----------------------------------------------------------------
function name GetWallMaterial(out vector wallNormal)
{
    local Vector StartTrace, EndTrace, HitLocation, HitNormal;
    local Actor aTarget;
    local int TexFlags;
    local Name TexName, TexGroup;

    StartTrace = Location + Vect(0, 0, 1.0) * BaseEyeHeight;
    EndTrace = StartTrace + (Vector(Rotation) * CollisionRadius * 1.5);

    foreach TraceTexture(class'Actor', aTarget, TexName, TexGroup, TexFlags, HitLocation, HitNormal, EndTrace, StartTrace)
    {
        if(aTarget == Level || aTarget.IsA('Mover'))
            break;
    }

    WallNormal = HitNormal;

    return TexGroup;
}

// ---------------------------------
// Clears out the player's inventory
// ---------------------------------
function ClearInventory()
{
    local Inventory item, nextItem, lastItem;

    SetInHandPending(None);

    if (Self.Inventory != None)
    {
        item = Self.Inventory;
        nextItem = item.Inventory;
        lastItem = item;

        do
        {
            if ((item != None) && item.bDisplayableInv || item.IsA('Ammo'))
            {
                // make sure everything is turned off
                if (item.IsA('DeusExWeapon'))
                {
                    DeusExWeapon(item).ScopeOff();
                    DeusExWeapon(item).LaserOff();
                }
                if (item.IsA('DeusExPickup'))
                {
                    if (DeusExPickup(item).bActive)
                        DeusExPickup(item).Activate();
                }

            if (item.IsA('ChargedPickup'))
                    Self.RemoveChargedDisplay(ChargedPickup(item));

                Self.DeleteInventory(item);
                item.Destroy();
                item = Self.Inventory;
            }
            else
                item = nextItem;

            if (item != None)
                nextItem = item.Inventory;
        }
        until ((item == None) || (item == lastItem));
    }
}

// --------------------------
// Updates the music track
//
// Pattern definitions:
//   0 - Ambient 1
//   1 - Dying
//   2 - Ambient 2 (optional)
//   3 - Combat
//   4 - Conversation
//   5 - Outro
// --------------------------
function UpdateDynamicMusic(float DeltaTime)
{
    if(!bMuteMusic)
        Super.UpdateDynamicMusic(DeltaTime);
}

// ------------------------------------------------------------
// Hijack this method to ensure pawns are present during convos 
// ------------------------------------------------------------
function bool StartConversation(
    Actor InvokeActor, 
    EInvokeMethod InvokeMethod, 
    optional Conversation Con,
    optional bool bAvoidState,
    optional bool bForcePlay
    )
{
    local ConEvent Event;
    local ConEventSpeech EventSpeech;
    local ScriptedPawn aScriptedPawn;
    local Rotator HelpRotator;
    local int Radius;

    if(Con == None)
        Con = GetActiveConversation(InvokeActor, InvokeMethod);

    // Only try this trick if the player frobbed a non-random NPC
    if(InvokeActor != None && InStr(InvokeActor.BindName, "Random") < 0 && InvokeMethod == IM_Frob)
    {
        // Set maximum radius
        Radius = 200;

        // Start the rotation 127 degrees offset from the player's direction 
        HelpRotator = Rotation;
        HelpRotator.Yaw += 24576; 

        // Loop all SpeechEvents in the convo to find their actors
        Event = Con.EventList;

        while(Event != None)
        {
            EventSpeech = ConEventSpeech(Event);

            if(EventSpeech != None)
            {
                foreach AllActors(class'ScriptedPawn', aScriptedPawn)
                {
                    if(aScriptedPawn.BindName != EventSpeech.SpeakerName && aScriptedPawn.BindName != EventSpeech.SpeakingToName)
                        continue;

                    if(aScriptedPawn.BindName == InvokeActor.BindName)
                        continue;

                    // Make sure the next actor is placed somewhere new
                    HelpRotator.Yaw += 4096;

                    // Reset the rotator, if it's going further than 225 degrees offset from the player's direction
                    if(HelpRotator.Yaw > Rotation.Yaw + 40960)
                        HelpRotator.Yaw = Rotation.Yaw + 24576; 

                    if(
                        aScriptedPawn != None &&
                        aScriptedPawn != Self &&
                        VSize(aScriptedPawn.Location - Location) > 300
                    )
                    {
                        // Place the actor within a min/max radius of the player
                        while(Radius > 32 && !aScriptedPawn.SetLocation(Location + Normal(Vector(HelpRotator)) * Radius))
                        {
                            HelpRotator.Yaw += 4096;
                            Radius -= 16;
                        }

                        aScriptedPawn.LookAtActor(Self, true, false, true, 0, 0.5);
                        aScriptedPawn.GoToState('Conversation');
                    }
                }
            }

            Event = Event.NextEvent;
        }
    }

    return Super.StartConversation(InvokeActor, InvokeMethod, Con, bAvoidState, bForcePlay);
}

// --------------
// Changes outfit
// --------------
function ChangeOutfit(string Outfit)
{
    local int i;
    local bool bFound;

    CurrentOutfit = Outfit;

    for(i = 0; i < ArrayCount(Outfits); i++)
    {
        if(Outfits[i].Name != Outfit)
            continue;
        
        if(Outfits[i].Mesh == None)
            Mesh = Default.Mesh;
        else
            Mesh = Outfits[i].Mesh;
      
        if(Outfits[i].Tex0 == None)
            MultiSkins[0] = Default.MultiSkins[0]; 
        else
            MultiSkins[0] = Outfits[i].Tex0;
        
        if(Outfits[i].Tex1 == None)
            MultiSkins[1] = Default.MultiSkins[1]; 
        else
            MultiSkins[1] = Outfits[i].Tex1;
        
        if(Outfits[i].Tex2 == None)
            MultiSkins[2] = Default.MultiSkins[2]; 
        else
            MultiSkins[2] = Outfits[i].Tex2;
        
        if(Outfits[i].Tex3 == None)
            MultiSkins[3] = Default.MultiSkins[3]; 
        else
            MultiSkins[3] = Outfits[i].Tex3;
        
        if(Outfits[i].Tex4 == None)
            MultiSkins[4] = Default.MultiSkins[4]; 
        else
            MultiSkins[4] = Outfits[i].Tex4;
        
        if(Outfits[i].Tex5 == None)
            MultiSkins[5] = Default.MultiSkins[5]; 
        else
            MultiSkins[5] = Outfits[i].Tex5;
        
        if(Outfits[i].Tex6 == None)
            MultiSkins[6] = Default.MultiSkins[6]; 
        else
            MultiSkins[6] = Outfits[i].Tex6;
        
        if(Outfits[i].Tex7 == None)
            MultiSkins[7] = Default.MultiSkins[7]; 
        else
            MultiSkins[7] = Outfits[i].Tex7;

        bFound = True;
        break;
    }

    if(!bFound)
    {
        CurrentOutfit = "";
        Mesh = Default.Mesh;
            
        for(i = 0; i < ArrayCount(MultiSkins); i++)
        {
            MultiSkins[i] = Default.MultiSkins[i];
        }
    }
}

// -------------------------------------------
// Checks whether a location is in front of us
// -------------------------------------------
function bool IsInFront(Vector Loc)
{
    local float DotProduct;
    
    DotProduct = (Location - Loc) dot Vector(ViewRotation);

    return DotProduct < 0.0;
}

// ------------------------------------
// Console: Change a character's outfit
// ------------------------------------
exec function Outfit(name ActorTag, string OutfitName)
{
    local ScriptedPawnExtended aPawn;

    if(ActorTag == 'Player')
    {
        ChangeOutfit(OutfitName);
    }
    else
    {
        foreach AllActors(class'ScriptedPawnExtended', aPawn, ActorTag)
        {
            aPawn.ChangeOutfit(OutfitName);
        }
    }
}


// --------------------
// Console: Give orders
// --------------------
exec function Order(name PawnTag, name Orders, optional name OrderTag)
{
    local ScriptedPawn aPawn;

    foreach AllActors(class'ScriptedPawn', aPawn, PawnTag)
    {
        aPawn.SetOrders(Orders, OrderTag, True);
    }
}

// -----------------
// Console: Teleport
// -----------------
exec function TP(name TagName)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, TagName)
    {
        SetLocation(aActor.Location);
        SetRotation(aActor.Rotation);
        ViewRotation = aActor.Rotation;
    }
}

// ------------------------------
// Console: Give the player a key
// ------------------------------
exec function GimmeKey(name KeyID)
{
    local NanoKey aKey;

    aKey = Spawn(class'NanoKey');
    aKey.Description = string(KeyID);
    aKey.KeyID = KeyID;
    aKey.GiveTo(Self);
}

// -------------------------
// Console: Trigger anything
// -------------------------
exec function DoTrigger(name TagName)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, TagName)
    {
        aActor.Trigger(Self, Self);
    }
}

// -------------------
// Console: Set a flag
// -------------------
exec function SetFlag(name FlagName, bool bValue)
{
    FlagBase.SetBool(FlagName, bValue);
}

// --------------------------
// Stubs that prevent crashes
// --------------------------
function FindGoodView();
function PlayerMove(Float DeltaTime);
function Rise();
function Dodge(eDodgeDir DodgeMove);

defaultproperties
{
    BindName="JCDenton"
    Tag='Player'
    CarcassType=class'CarcassExtended'
}
