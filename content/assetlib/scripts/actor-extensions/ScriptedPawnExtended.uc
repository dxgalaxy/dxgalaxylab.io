class ScriptedPawnExtended extends ScriptedPawn;

// Outfits
struct PawnOutfit
{
    var string Name;
    var Mesh Mesh;
    var Texture Tex0;
    var Texture Tex1;
    var Texture Tex2;
    var Texture Tex3;
    var Texture Tex4;
    var Texture Tex5;
    var Texture Tex6;
    var Texture Tex7;
};

// Adjustable parameters
var(ScriptedPawn) bool bExplodeOnDeath;
var(ScriptedPawn) bool bFollowPersistently;
var(ScriptedPawn) PawnOutfit Outfits[10];

// Internal variables
var travel string CurrentOutfit;
var HumanExtended aPlayer;

// ----
// Init
// ----
function PostBeginPlay()
{
    Super.PostBeginPlay();

    if (bIsFemale)
    {
        HitSound1 = Sound'FemalePainMedium';
        HitSound2 = Sound'FemalePainLarge';
        Die = Sound'FemaleDeath';
    }

    aPlayer = HumanExtended(GetPlayerPawn());
}

// --------------
// Changes outfit
// --------------
function ChangeOutfit(string Outfit)
{
    local int i;
    local bool bFound;

    CurrentOutfit = Outfit;

    for(i = 0; i < ArrayCount(Outfits); i++)
    {
        if(Outfits[i].Name != Outfit)
            continue;

        if(Outfits[i].Mesh == None)
            Mesh = Default.Mesh;
        else
            Mesh = Outfits[i].Mesh;
      
        if(Outfits[i].Tex0 == None)
            MultiSkins[0] = Default.MultiSkins[0]; 
        else
            MultiSkins[0] = Outfits[i].Tex0;
        
        if(Outfits[i].Tex1 == None)
            MultiSkins[1] = Default.MultiSkins[1]; 
        else
            MultiSkins[1] = Outfits[i].Tex1;
        
        if(Outfits[i].Tex2 == None)
            MultiSkins[2] = Default.MultiSkins[2]; 
        else
            MultiSkins[2] = Outfits[i].Tex2;
        
        if(Outfits[i].Tex3 == None)
            MultiSkins[3] = Default.MultiSkins[3]; 
        else
            MultiSkins[3] = Outfits[i].Tex3;
        
        if(Outfits[i].Tex4 == None)
            MultiSkins[4] = Default.MultiSkins[4]; 
        else
            MultiSkins[4] = Outfits[i].Tex4;
        
        if(Outfits[i].Tex5 == None)
            MultiSkins[5] = Default.MultiSkins[5]; 
        else
            MultiSkins[5] = Outfits[i].Tex5;
        
        if(Outfits[i].Tex6 == None)
            MultiSkins[6] = Default.MultiSkins[6]; 
        else
            MultiSkins[6] = Outfits[i].Tex6;
        
        if(Outfits[i].Tex7 == None)
            MultiSkins[7] = Default.MultiSkins[7]; 
        else
            MultiSkins[7] = Outfits[i].Tex7;
        
        bFound = True;
        break;
    }

    if(!bFound)
    {
        CurrentOutfit = "";
        Mesh = Default.Mesh;
            
        for(i = 0; i < ArrayCount(MultiSkins); i++)
        {
            MultiSkins[i] = Default.MultiSkins[i];
        }
    }
}

// ---------------------------------------
// Spawns the carcass upon death/knock out
// ---------------------------------------
function Carcass SpawnCarcass()
{
    local DeusExCarcass aCarcass;

    if(bStunned || !bExplodeOnDeath)
        return Super.SpawnCarcass();

    Explode();

    return None;
}

// -------------------------
// Spawn an explosion effect
// -------------------------
function Explode()
{
    local SphereEffect sphere;
    local ScorchMark aScorchMark;
    local ExplosionLight light;
    local int i;
    local float explosionDamage;
    local float explosionRadius;

    ExplosionDamage = 100;
    ExplosionRadius = 256;

    // Alert NPCs that I'm exploding
    AISendEvent('LoudNoise', EAITYPE_Audio,, ExplosionRadius*16);
    PlaySound(Sound'LargeExplosion1', SLOT_None,,, ExplosionRadius*16);

    // Draw a pretty explosion
    Light = Spawn(class'ExplosionLight',,, Location);
    if(Light != None)
        Light.size = 4;

    Spawn(class'ExplosionSmall',,, Location + 2*VRand()*CollisionRadius);
    Spawn(class'ExplosionMedium',,, Location + 2*VRand()*CollisionRadius);
    Spawn(class'ExplosionMedium',,, Location + 2*VRand()*CollisionRadius);
    Spawn(class'ExplosionLarge',,, Location + 2*VRand()*CollisionRadius);

    Sphere = Spawn(class'SphereEffect',,, Location);
    if(Sphere != None)
        Sphere.size = ExplosionRadius / 32.0;

    // Spawn a mark
    aScorchMark = Spawn(class'ScorchMark', Base,, Location-vect(0,0,1)*CollisionHeight, Rotation+rot(16384,0,0));
    if (aScorchMark != None)
    {
        aScorchMark.DrawScale = FClamp(ExplosionDamage / 30, 0.1, 3.0);
        aScorchMark.ReattachDecal();
    }

    // Spawn some rocks and flesh fragments
    for(i=0; i<explosionDamage/6; i++)
    {
        if (FRand() < 0.3)
            Spawn(class'Rockchip',,,Location);
        else
            Spawn(class'FleshFragment',,,Location);
    }

    HurtRadius(ExplosionDamage, ExplosionRadius, 'Exploded', ExplosionDamage * 100, Location);
}

// -----------------
// State: Patrolling
// -----------------
state Patrolling
{
    function SetFall()
    {
        StartFalling('Patrolling', 'ContinuePatrol');
    }

    function HitWall(vector HitNormal, actor Wall)
    {
        if (Physics == PHYS_Falling)
            return;
        Global.HitWall(HitNormal, Wall);
        CheckOpenDoor(HitNormal, Wall);
    }
    
    function AnimEnd()
    {
        PlayWaiting();
    }

    function PatrolPoint PickStartPoint()
    {
        local NavigationPoint   Nav;
        local PatrolPoint       CurNav;
        local float             CurDist;
        local PatrolPoint       ClosestNav;
        local float             ClosestDist;

        Nav = Level.NavigationPointList;
        
        while(Nav != None)
        {
            Nav.VisitedWeight = 0;
            Nav = Nav.NextNavigationPoint;
        }

        ClosestNav = None;
        ClosestDist = 100000;
        Nav = Level.NavigationPointList;
        
        while(Nav != None)
        {
            CurNav = PatrolPoint(Nav);
            if ((CurNav != None) && (CurNav.Tag == OrderTag))
            {
                while (CurNav != None)
                {
                    if (CurNav.VisitedWeight != 0)  // been here before
                        break;
                    CurDist = VSize(Location - CurNav.Location);
                    if ((ClosestNav == None) || (ClosestDist > CurDist))
                    {
                        ClosestNav  = CurNav;
                        ClosestDist = CurDist;
                    }
                    CurNav.VisitedWeight = 1;
                    CurNav = CurNav.NextPatrolPoint;
                }
            }
            nav = nav.nextNavigationPoint;
        }

        return (ClosestNav);
    }

    function PickDestination()
    {
        if (PatrolPoint(DestPoint) != None)
            DestPoint = PatrolPoint(DestPoint).NextPatrolPoint;
        else
            DestPoint = PickStartPoint();
        if (DestPoint == None)  // can't go anywhere...
            GotoState('Standing');
    }

    function BeginState()
    {
        StandUp();
        SetEnemy(None, EnemyLastSeen, true);
        Disable('AnimEnd');
        SetupWeapon(false);
        SetDistress(false);
        bStasis = false;
        SeekPawn = None;
        EnableCheckDestLoc(false);
    }

    function EndState()
    {
        EnableCheckDestLoc(false);
        Enable('AnimEnd');
        bStasis = true;
    }

Begin:
    DestPoint = None;

Patrol:
    WaitForLanding();
    PickDestination();

Moving:
    // Move from PathNode to PathNode until we get where we're going
    if(DestPoint != None)
    {
        if (!IsPointInCylinder(self, DestPoint.Location, 16-CollisionRadius))
        {
            EnableCheckDestLoc(true);
            MoveTarget = FindPathToward(DestPoint);
            
            if (MoveTarget != None)
            {
                while (MoveTarget != None)
                {
                    if (ShouldPlayWalk(MoveTarget.Location))
                        PlayWalking();
                    MoveToward(MoveTarget, GetWalkingSpeed());
                    CheckDestLoc(MoveTarget.Location, true);
                    if (MoveTarget == DestPoint)
                        break;
                    MoveTarget = FindPathToward(DestPoint);
                }
                if ((MoveTarget != None) && (MoveTarget != DestPoint))
                {
                    SetOrders('Standing', 'None', False);
                    GoToState('Standing');
                }
            }
            else
            {
                if(ShouldPlayWalk(DestPoint.Location))
                    PlayWalking();
                MoveToward(DestPoint, GetWalkingSpeed());
            }
            EnableCheckDestLoc(false);
        }
    }
    else
        Goto('Patrol');

Pausing:
    if(!bAlwaysPatrol)
        bStasis = true;

    Acceleration = Vect(0, 0, 0);

    // Turn in the direction dictated by the WanderPoint, or a random direction
    if (PatrolPoint(DestPoint) != None)
    {
        if ((PatrolPoint(DestPoint).PauseTime > 0) || (PatrolPoint(DestPoint).NextPatrolPoint == None))
        {
            if(ShouldPlayTurn(Location + PatrolPoint(DestPoint).LookDir))
                PlayTurning();
            TurnTo(Location + PatrolPoint(DestPoint).LookDir);
            Enable('AnimEnd');
            TweenToWaiting(0.2);
            PlayScanningSound();
            SleepTime = PatrolPoint(DestPoint).PauseTime * ((-0.9*restlessness) + 1);
            Sleep(SleepTime);
            Disable('AnimEnd');
            FinishAnim();
        }
    }
    Goto('Patrol');

ContinuePatrol:
ContinueFromDoor:
    FinishAnim();
    PlayWalking();
    Goto('Moving');

}

// ----------------
// State: Following
// ----------------
state Following
{
    event Tick(float DeltaTime)
    {
        local PathNode aPathNode;

        if(
            bFollowPersistently &&
            aPlayer != None &&
            VSize(Location - aPlayer.Location) > 1024
        )
        {
            foreach AllActors(class'PathNode', aPathNode)
            {
                if(VSize(aPlayer.Location - aPathNode.Location) > 512 || aPlayer.IsInFront(aPathNode.Location))
                    continue;

                SetLocation(aPathNode.Location);
                break;
            }
        }
        
        Super.Tick(DeltaTime);
    }
}

// --------------------------
// Stubs that prevent crashes
// --------------------------
function AdjustJump();
function MoveFallingBody();
function Vector FocusDirection();
function FindBackupPoint();
function bool DoorEncroaches();
function PickDestinationPlain();
function PickDestinationInputBool(bool ExampleBool);
function bool PickDestinationBool();
function EDestinationType PickDestinationEnum();
function float DistanceToTarget();
function AlarmUnit FindTarget();
function bool GetNextAlarmPoint(AlarmUnit alarm);
function vector FindAlarmPosition(Actor alarm);
function bool IsAlarmInRange(AlarmUnit alarm);
function TriggerAlarm();
function bool IsAlarmReady(Actor actorAlarm);
function EndCrouch();
function StartCrouch();
function bool ShouldCrouch();
function bool ReadyForWeapon();
function bool IsHandToHand();
function CheckAttack(bool bPlaySound);
function bool FireIfClearShot();
function bool InSeat(out vector newLoc);
function FinishFleeing();
function NavigationPoint GetOvershootDestination(float randomness, optional float focus);
function bool GetNextLocation(out vector nextLoc);
function PatrolPoint PickStartPoint();
function bool GoHome();
function FollowSeatFallbackOrders();
function FindBestSeat();
function int FindBestSlot(Seat seatActor, out float slotDist);
function bool IsIntersectingSeat();
function vector GetDestinationPosition(Seat seatActor, optional float extraDist);
function Vector SitPosition(Seat seatActor, int slot);

defaultproperties
{
     BaseAccuracy=0.200000
     maxRange=1000.000000
     MinHealth=20.000000
     bPlayIdle=True
     bCanCrouch=True
     bSprint=True
     CrouchRate=1.000000
     SprintRate=1.000000
     bReactAlarm=True
     EnemyTimeout=5.000000
     bCanTurnHead=True
     WaterSpeed=80.000000
     AirSpeed=160.000000
     AccelRate=500.000000
     BaseEyeHeight=40.000000
     UnderWaterTime=20.000000
     AttitudeToPlayer=ATTITUDE_Ignore
     HitSound1=Sound'DeusExSounds.Player.MalePainSmall'
     HitSound2=Sound'DeusExSounds.Player.MalePainMedium'
     Die=Sound'DeusExSounds.Player.MaleDeath'
     VisibilityThreshold=0.010000
     DrawType=DT_Mesh
     Mass=150.000000
     Buoyancy=155.000000
     BindName="Person"
     CarcassType=class'CarcassExtended'
}
