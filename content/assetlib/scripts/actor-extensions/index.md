---
title:          Actor Extensions
description:    These are extensions to some existing actor types that will make working with them a lot simpler
author:         Artifechs
---

@downloads(Files)
    files:
        - CarcassExtended.uc
        - CarcassProxy.uc
        - HumanExtended.uc
        - MissionScriptExtended.uc
        - MissionScriptTrigger.uc
        - ScriptedPawnExtended.uc

## Features

### Carcass

- One CarcassType to rule them all. Automatically matches the mesh and textures of the Pawn.
- Depends on a CarcassProxy to keep mesh and textures in memory when the player picks it up.

### Human

- Several handy functions implemented as console commands
- Change outfits
- Automatically teleports required pawns into view when a conversation is started
- Crash prevention by declaring functions that sometimes get called outside the states they exist in
- Ability to mute music temporarily
- Function for clearing inventory
- Fixed GetWallMaterial function

### MissionScript

- Several convenience functions for manipulating actors
- In combination with MissionScriptTrigger can be used to script events rather than use LogicTriggers and Dispatchers

### ScriptedPawn

- Explode on death flag
- Change outfits
- Improved patrolling
- Persistent following
- Crash prevention by declaring functions that sometimes get called outside the states they exist in
