//=============================================================================
// MissionScriptExtended - Useful helper functions for mission scripts
//=============================================================================
class MissionScriptExtended extends MissionScript;

var ExtensionObject EO;
var name EventName;
var Actor aEventActor;
var Pawn aEventInstigator;

function PostBeginPlay()
{
    EO = new class'ExtensionObject';
}

function name StringToName(string Input)
{
    return EO.StringToName(Input);
}

function HumanExtended GetPlayer()
{
    return HumanExtended(GetPlayerPawn());
}

function SetEvent(name NextEventName, Actor aNextEventActor, Pawn aNextEventInstigator)
{
    EventName = NextEventName;
    aEventActor = aNextEventActor;
    aEventInstigator = aNextEventInstigator;

    GoToState('DoEvent');
}

function GiveItem(class<Inventory> ItemClass, name ReceiverTag)
{
    local Pawn aPawn; 
    local Inventory aItem;

    if(ReceiverTag == 'Player')
    {
        aItem = Spawn(ItemClass, Player);
        aItem.GiveTo(Player);
        aItem.SetBase(Player);
    }
    else
    {
        foreach AllActors(class'Pawn', aPawn, ReceiverTag)
        {
            aItem = Spawn(ItemClass, aPawn);
            aItem.GiveTo(aPawn);
            aItem.SetBase(aPawn);
        }
    }
}

function TriggerActor(name ActorName)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorName)
    {
        aActor.Trigger(Self, Player);
    }
}

function ChangePawnOutfit(name ActorTag, name OutfitName)
{
    local ScriptedPawnExtended aPawn;

    if(ActorTag == 'Player')
    {
        GetPlayer().ChangeOutfit(string(OutfitName));
    }
    else
    {
        foreach AllActors(class'ScriptedPawnExtended', aPawn, ActorTag)
        {
            aPawn.ChangeOutfit(string(OutfitName));
        }
    }
}

function ScriptedPawnExtended GetPawn(name ActorTag)
{
    return ScriptedPawnExtended(GetActor(ActorTag));
        GetPlayer().ClientMessage("OUTFIT!");
}

function Actor GetActor(name ActorTag)
{
    local Actor aActor;

    if(ActorTag == 'Player')
        return Player;

    foreach AllActors(class'Actor', aActor, ActorTag)
        return aActor;

    return None;
}

function MoveActorSeamlessly(name TravellerTag, name FromTag, name ToTag)
{
    local Actor aTraveller, aFrom, aTo;

    aTraveller = GetActor(TravellerTag);

    if(aTraveller == None)
        return;

    foreach AllActors(class'Actor', aFrom, FromTag)
    {
        foreach AllActors(class'Actor', aTo, ToTag)
        {
            aTraveller.SetLocation((aTraveller.Location - aFrom.Location) + aTo.Location);
            break;
        }
        break;
    }
}

function OpenDoor(name TagName)
{
    local DeusExMover aMover;

    foreach AllActors(class'DeusExMover', aMover, TagName)
    {
        aMover.DoOpen();
    }
}

function CloseDoor(name TagName)
{
    local DeusExMover aMover;

    foreach AllActors(class'DeusExMover', aMover, TagName)
    {
        aMover.DoClose();
    }
}

function bool IsDoorOpen(name TagName)
{
    local DeusExMover aMover;

    foreach AllActors(class'DeusExMover', aMover, TagName)
    {
        return aMover.KeyNum > 0;
    }

    return False;
}

function bool IsDoorLocked(name TagName)
{
    local DeusExMover aMover;

    foreach AllActors(class'DeusExMover', aMover, TagName)
    {
        return aMover.bLocked;
    }

    return False;
}

function SetDoorLocked(name DoorTag, bool bLocked)
{
    local DeusExMover aMover;

    foreach AllActors(class'DeusExMover', aMover, DoorTag)
    {
        aMover.bLocked = bLocked;
    }
}

function SetActorEvent(name ActorTag, name EventName)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        aActor.Event = EventName;
    }
}

function DestroyActor(name ActorTag)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        aActor.Destroy();
    }
}

function SetActorHidden(name ActorTag, bool bSetHidden)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        aActor.bHidden = bSetHidden;
    }
}

function MoveActor(name TravellerTag, name DestinationTag)
{
    local Actor aTraveller, aDestination;
    local Rotator HelpRotator;
    local int HelpDistance;
    local int LoopCount;

    aTraveller = GetActor(TravellerTag);

    if(aTraveller == None)
        return;

    foreach AllActors(class'Actor', aDestination, DestinationTag)
    {
        HelpRotator = aDestination.Rotation;
        HelpDistance = 16;

        if(!aTraveller.SetLocation(aDestination.Location))
        {
            while(LoopCount < 16 && !aTraveller.SetLocation(aDestination.Location + Normal(Vector(HelpRotator)) * HelpDistance))
            {
                HelpRotator.Yaw += 8192;
                HelpDistance += 16;
                LoopCount++;
            }
        }
        
        aTraveller.SetRotation(aDestination.Rotation);
        break;
    }
}

function MakePawnFrob(name PawnTag, name FrobTag)
{
    local ScriptedPawn aPawn;
    local Actor aTarget;
    local Vector Direction, TurnTowards;

    foreach AllActors(class'ScriptedPawn', aPawn, PawnTag)
    {
        foreach AllActors(class'Actor', aTarget, FrobTag)
        {
            aPawn.SetOrders('Standing', '', True);

            TurnTowards = aTarget.Location;
            TurnTowards.Z = aPawn.Location.Z;
            Direction = Normal(TurnTowards - aPawn.Location);

            aPawn.DesiredRotation = Rotator(Direction);
            aPawn.PlayPushing();

            if(aTarget.IsA('DeusExDecoration'))
                DeusExDecoration(aTarget).Frob(aPawn, None); 
            else if(aTarget.IsA('ScriptedPawn'))
                ScriptedPawn(aTarget).Frob(aPawn, None);
            else if(aTarget.IsA('DeusExMover'))
                DeusExMover(aTarget).Frob(aPawn, None);
            break;
        }
        break;
    }
}

function OrderPawn(name PawnTag, name Orders, optional name OrderTag)
{
    local ScriptedPawn aPawn;

    foreach AllActors(class'ScriptedPawn', aPawn, PawnTag)
    {
        aPawn.SetOrders(Orders, OrderTag, True);
    }
}
