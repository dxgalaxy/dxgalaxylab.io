---
title:          Visibility Trigger
description:    Hides or shows an actor
author:         Artifechs
---

## VisibilityTrigger.uc

    class VisibilityTrigger extends Trigger;

    var() bool bVisible;

    defaultproperties
    {
         bVisible=True
    }

    function Trigger(Actor Other, Pawn Instigator)
    {
        local Actor A;

        if (Event != '')
        {
            foreach AllActors(class 'Actor', A, Event)
            {
                A.bHidden = !bVisible;
            }
        }

        Super.Trigger(Other, Instigator);
    }

