---
title:          Follow Trigger
description:    This is a trigger that makes pawns follow the player by running and standing/crouching between hiding spots
author:         Artifechs
---

@downloads(Files)
    files:
        - FollowTrigger.uc
        - FollowStealthTrigger.uc

## Features

- Pawns are flagged as undetectable while running between points
- A flag can be optionally set to indicate which pawn is at which point

## Usage

Add this code to your `ScriptedPawn` to make them hide:

```
state Hiding
{
    function StartCrouch()
    {
        if(!bCrouching)
        {
            bCrouching = True;
            SetBasedPawnSize(CollisionRadius, GetCrouchHeight());
            PlayAnim('Crouch',,0.15);
        }
    }

    function EndCrouch()
    {
        if(bCrouching)
        {
            bCrouching = False;
            ResetBasedPawnSize();
            TweenToWaiting(0.2);
        }
    }

    function EndState()
    {
        EndCrouch();
        bDetectable = True;
        bIgnore = False;
    }

Begin:
    bDetectable = False;
    bIgnore = True;

    if(OrderActor != None)
    {
        MoveTo(OrderActor.Location);

        while(!IsAtOrderActor())
            Sleep(0.0);
        
        DesiredRotation = OrderActor.Rotation;
    }

    Acceleration = Vect(0, 0, 0);
    StartCrouch();
}
```
