//=============================================================================
// FollowStealthTrigger
// ---
// Makes pawns follow the player and hide at this spot
//=============================================================================
class FollowStealthTrigger extends FollowTrigger;

function StartPawn(ScriptedPawn aPawn, name OrderTag)
{
    if(
        aPawn == None || 
        (
            aPawn.IsInState('Hiding') &&
            aPawn.OrderTag == OrderTag
        )
    )
        return;

    aPawn.SetOrders('RunningTo', OrderTag, True);

    aPawn.bIgnore = True;
    aPawn.bDetectable = False;
}

function StopPawn(ScriptedPawn aPawn)
{
    if(aPawn == None)
        return;
        
    aPawn.SetOrders('Hiding', Tag, True);
}
