//=============================================================================
// FollowTrigger
// ---
// Makes ScriptedPawns follow the player and stand at this spot. It works in 2 modes:
// 
// Player touch:
// - Set TriggerType to TT_PlayerProximity for the trigger that makes
//   the ScriptedPawn start running
// - Set the "Event" to the "Tag" of the trigger the ScriptedPawn should
//   run towards
//
// ScriptedPawn touch:
// - Set TriggerType to TT_PawnProximity or TT_ClassProximity for the
//   trigger that the SciptedPawn should run to
// - Set the "Event" to the "Tag" of the ScriptedPawn 
//=============================================================================
class FollowTrigger extends Trigger;

var() bool bSetFlag;            // Set a <PawnTag>_Is_At_<TriggerTag> flag, e.g. PaulDenton_Is_At_FollowSpot1
var() name FlagName;            // Check this flag name before executing
var() bool bFlagValue;          // Check this flag value before executing

function bool CheckFlag()
{
    local DeusExPlayer aPlayer;

    aPlayer = DeusExPlayer(GetPlayerPawn());

    if(aPlayer != None && FlagName != '' && aPlayer.FlagBase.GetBool(FlagName) != bFlagValue)
        return False;

    return True;
}

function SetFlag(Actor aOther, bool bValue)
{
    local DeusExPlayer aPlayer;
    local string FlagString;
    local name FlagName;

    if(!bSetFlag)
        return;

    aPlayer = DeusExPlayer(GetPlayerPawn());

    if(aPlayer == None)
        return;

    if(aOther.IsA('DeusExPlayer'))
        FlagString = "Player";
    else
        FlagString = string(aOther.Tag);
    
    FlagString = FlagString $ "_Is_At_" $ Tag;
    FlagName = aPlayer.RootWindow.StringToName(FlagString);
    
    if(bValue == aPlayer.FlagBase.GetBool(FlagName))
        return;

    aPlayer.FlagBase.SetBool(FlagName, bValue);
}

function UnTouch(Actor aOther)
{
    if(ReTriggerDelay > 0 && Level.TimeSeconds - TriggerTime < ReTriggerDelay)
        return;
    
    if(bSetFlag)
        SetFlag(aOther, False);
}

function Touch(Actor aOther)
{
    local ScriptedPawn aPawn;
    local DeusExPlayer aPlayer;
    local Actor aTarget;

    aPlayer = DeusExPlayer(aOther);
    aPawn = ScriptedPawn(aOther);

    if(ReTriggerDelay > 0)
    {
        if(Level.TimeSeconds - TriggerTime < ReTriggerDelay)
            return;

        TriggerTime = Level.TimeSeconds;
    }
    
    // This is the player trigger, where the pawns starts running
    if(TriggerType == TT_PlayerProximity && aPlayer != None && CheckFlag())
    {
        if(bSetFlag)
            SetFlag(aOther, True);

        foreach AllActors(class'Actor', aTarget, Event)
        {
            if(aTarget.Event == '')
                continue;
            
            foreach AllActors(class'ScriptedPawn', aPawn, aTarget.Event)
            {
                StartPawn(aPawn, aTarget.Tag);
            }
        }
    }
    // This is the ScriptedPawn trigger, where the pawn stops
    else if(
        (
            (TriggerType == TT_PawnProximity && aPawn != None) ||
            (TriggerType == TT_ClassProximity && IsRelevant(aPawn))
        ) &&
        aPawn.Tag == Event &&
        aPawn.IsInState('RunningTo') &&
        aPawn.OrderTag == Tag
    )
    {
        if(bTriggerOnceOnly)
            SetCollision(False);
        
        if(bSetFlag)
            SetFlag(aOther, True);
    
        StopPawn(aPawn);
    }
}

function StartPawn(ScriptedPawn aPawn, name OrderTag)
{
    if(
        aPawn == None || 
        (
            aPawn.IsInState('Standing') &&
            aPawn.OrderTag == OrderTag
        )
    )
        return;

    aPawn.SetOrders('RunningTo', OrderTag, True);
}

function StopPawn(ScriptedPawn aPawn)
{
    if(aPawn == None)
        return;
        
    aPawn.SetOrders('Standing', Tag, True);
}

defaultproperties
{
    TriggerType=TT_PawnProximity
    bDirectional=True
}
