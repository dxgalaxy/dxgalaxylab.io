---
title:          Cinematic Point
description:    This is a versatile tool for creating cinematics. As opposed to InterpolationPoint and CameraPoint, this is based on an event chain, so one point is linked to another by `Event` and `Tag` properties. This also means it can work with other triggerable actors.
author:         Artifechs
---

@downloads(Files)
    files:
        - CinematicPoint.uc


## Features

- Control any actor, including the player/camera  
- Play sounds, ambient and transient   
- Interpolate between values:  
    - Location  
    - Rotation  
    - FOV  
    - Sound volume  
    - Sound pitch  
- Delay next point execution  
- Multiple interpolation smoothing options  
- Restore the actor to its previous state (handy for smoothly transitioning into gameplay)

