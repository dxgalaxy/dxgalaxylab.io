---
title: Showers
---

So you're looking to put in a shower. You may think that connecting a `ShowerFaucet`'s `Event` to a `ShowerHead`'s `Tag` would do the job, but they actually need to have the same `Tag` for them to work together. Go figure.
