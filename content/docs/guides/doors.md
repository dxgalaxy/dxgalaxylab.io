---
title: Doors
---

A door in Deus Ex is an Unreal "mover" brush with a few extra features.

## Prerequisites
- [Movers](../movers)

## Create a mover
1. Pick a texture you want for the door
2. Right click the `Cube` brush to bring up the cube builder menu.  
3. Type in height, width and breadth as 128x64x4.  
4. Click `Build`.  
5. If you're making a swinging door, move the pivot point (right click on a vertex) to where the hinge would be.  
6. Right click the `Add mover` button and pick `DeusExMover`.  

The mover brush will not be rendered in the editor, so all you see is the purple outline. It will show up like normal once in-game, though.

!!! Complex shapes
    To create doors made up of several brushes, just `Add/Substract` them to your map, free floating in the air. Then create a box shape around the structure and click `Intersect`. You can now use this brush like you used the cube in the above sequence.

!!! Editing textures
    To change the textures' alignment after creating the mover, just right click it and select `Mover > Show polys`. Make your changes and rebuild.


## Set up the keyframes  
1. Right click the mover brush and select `Movers > Key 1`.  
2. Rotate/move the door into the desired rotation/position.  
3. Right click the mover brush again and select `Movers > Key 0 (Base)`. 

!!! Note
    You can also do this by left clicking on the brush and pressing `Shift`+`0`/`1`/etc...

## Set up the door properties
1. Open the properties for the mover brush, either by double clicking, pressing F4, or by right clicking and selecting `Properties`.  
2. Expand the `DeusExMover` section and change `bIsDoor` to `True`.  
3. Navigate to the sounds you want the door to make in the `Sound Browser` under the `MoverSFX`. You can double click sounds to preview them.    
4. Expand the `MoverSounds` section under the mover brush properties.  
5. Select the opening sound you want in the browser and click `Use` next to the appropriate field in the mover brush properties.  
6. If your door moves between light and dark areas, you might want to switch on the `Mover > bDynamicLightMover` field.  
7. It might also be a good idea to set `Mover > MoverEncroachType` to `ME_IgnoreWhenEncroach` to let the door pass through the player when opened.   
