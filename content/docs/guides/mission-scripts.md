---
title: Mission scripts
---

The `MissionScript` is a crucial component for adding complex functionality to a map.

!!! Note
    UnrealScript is case insensitive, but it's a good idea to stay consistent. The most common practice is to name variables and functions in PascalCase.  

!!! Note
    Remember to [rebuild](../custom-packages#build) before testing your changes.


## Prerequisites
- [Custom packages](../custom-packages)


## Boilerplate

This is the outline of what you will usually need:

    class Mission16 expands MissionScript;

    function InitStateMachine()
    {
        Super.InitStateMachine();
    
        FirstFrame();
    }

    // This gets called once per map
    function FirstFrame()
    {
        Super.FirstFrame();
        
        // Code usually goes here
    }

    // This gets called repeatedly throughout gameplay
    function Timer()
    {
        Super.Timer();
        
        // Code usually goes here
    }

    // This gets called before leaving a map
    function PreTravel()
    {
        // Code usually goes here

        Super.PreTravel();
    }

## Common use cases

### Player

    // Gets a reference to the player character
    Player = DeusExPlayer(GetPlayerPawn());
    
    // Sends a message to the player
    Player.ClientMessage("Hello, player!");


### NPCs
        
    local ScriptedPawn NPC;

    foreach AllActors(class'ScriptedPawn', NPC, 'PaulDenton') // PaulDenton is the Events > Tag of the NPC
    {
        // Hides an NPC
        NPC.LeaveWorld();
        
        // Shows an NPC
        NPC.EnterWorld();

        // Sets the location of an NPC
        NPC.SetLocation(Vect(1024, 521, 284));
    }


### Flags
                
    if ( Flags.GetBool('MyFlag') )
    {
        Flags.SetBool( 'MyFlag', False,, 17 ); // The last number is at which mission this flag should expire
    }


### Performing an action on a specific map

    // The LocalURL is always uppercase
    if ( LocalURL == "16_MY_MAP" )
    {
        // Do something
    }


### Firing a trigger during a conversation between NPCs

    function Timer()
    {
        local Trigger T;

        Super.Timer();

        if ( LocalURL == "16_MY_MAP" )
        {
            // Check if:
            //    1. The conversation has ended (it has to be configured to add a "_Played" flag upon ending)
            //    2. We have already triggered the alliance change
            if ( flags.GetBool('OverhearNPCs_Played') && !flags.GetBool('NPCsFought') )
            {
                // Find an AllianceTrigger with the Events > Tag set to "NPCsStartFighting" and trigger it
                foreach AllActors(class'Trigger', T, 'NPCsStartFighting')
                    T.Trigger(Self, Player);

                // Set a flag to prevent the trigger from being fired again
                flags.SetBool('NPCsFought', True,, 17);
            }
        }
    }

