---
title: Textures
---

## Prerequisites
- [Custom packages](../custom-packages)

## Importing

!!! Important
    Textures need to be 8-bit (256 colours) power of two (8, 16, 32, 64, 128, 256) .pcx files. Larger textures are only supported through community made renderers.  

!!! Transparency
    To make part of a texture transparent, you need to draw a specific colour onto the part you want to be transparent. By default, textures with the "masked" flag active will mask out black colours (`#000000`). You can set this colour to something else, like magenta (`#FF00FF`), by setting it as the first colour in the indexed palette of your image.


### Via UnrealEd

This method packages your textures in a separate .utx file.  

1. In UnrealEd, open the texture browser and click `File > Import...`.  
2. Select all the files you want in your texture package and click `OK`.  
3. A dialog window will come up for each file.  
    - The `Name` field can be anything, but it's good practice to keep it the same as the filename.  
    - The `Group` field is just for organisation.  
    - The `Package` field will be the name of the .utx file.  
4. The import properties speak for themselves. You can save space by disabling mip mapping for UI textures.  
5. Click `File > Save` and save the texture package in your `MyPackage/Textures` folder.   


### Via script

This method imports your textures into your .u file. This is a very reliable method, and convenient for distribution, but you do have to restart UnrealEd every time you make changes to this file and rebuild.   

Create a script called `TextureImport.uc` in your `MyPackage/Classes` folder and rebuild. Here is an example:

    class TextureImport expands Object abstract; 
    
    #exec TEXTURE IMPORT NAME=My_Texture FILE=Textures\My_Texture.pcx GROUP=Default MIPS=Off
    #exec TEXTURE IMPORT NAME=My_Masked_Texture FILE=Textures\My_Masked_Texture.pcx GROUP=Default FLAGS=2
    #exec TEXTURE IMPORT NAME=My_UI_Texture FILE=Textures\My_UI_Texture.pcx GROUP=UI MIPS=Off


- The `NAME` value is the same as the `Name` field in the UnrealEd method.  
- The `FILE` value is relative to your package folder.  
- The `GROUP` value is the same as the `Group` field in the UnrealEd method.  
- The `MIPS` value (optional) can me set to `Off` to disable mip mapping, only used for UI textures.  
- The `FLAGS` value (optional) represents one of the following texture settings:  

    | Setting            | Flag  |
    | ------------------ | ----- |
    | Masked             | 2     |
    | Transparent        | 4     |
    | Environment mapped | 16    |
    | Modulated          | 64    |
    | Fake Backdrop      | 128   |
    | Two-sided          | 256   |
    | Auto U pan         | 512   |
    | Auto V pan         | 1024  |
    | No smooth          | 2048  |
    | Big wavy           | 4096  |
    | Small wavy         | 8192  |
    | Water wavy         | 16384 |
