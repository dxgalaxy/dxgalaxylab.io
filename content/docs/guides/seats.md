---
title: Seats
---

NPC can be made to sit down only on these actors


## Prerequisites
- [Actors](../actors)


@properties
    Events:
        Tag:        Used by ScriptedPawns to locate the seat
    Seat:
        sitPoint:   A vector defining where in relation to the actor's origin an NPC will sit


## How to

1. Place a `Seat` in your map from the `Decoration > DeusExDecoration > Furniture > Seat` section.  
2. Open its properties and set `Events > Tag` to `MySeat`.  
3. Place a `ScriptedPawn` in your map and open its properties.  
4. Set the `Orders > Orders` to `Sitting`.  
5. Set the `Orders > OrderTag` to `MyChair`.  

!!! Note
    For an NPC to use a chair, their need to be able to "find" it. Make sure there is enoug room around the chair for the NPC to move there, and that there is a clear path to it.


## Making custom seats

Apart from subclassing a seat and assigning a custom mesh, you can also trick NPCs to sit on BSP geometry.

1. Add a special brush to your map, set it to `Non-Solid`. It should be the same height as a seat.  
2. Place a seat inside it and open its properties.  
3. Set the `Advanced > bHidden` to `True`.  
4. Set the `Movement > Physics` to `PHYS_None`.  
