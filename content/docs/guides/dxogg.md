---
title: DXOgg
description: This guide decribes how to play .ogg music files in Deus Ex
---

## Prerequisites
- <a href="/downloads/DXOgg.zip" download>DXOgg.zip</a>

## Setup

1. Unpack the `DXOgg.u` and `DXOgg.dll` into your `DeusEx/System` directory.  
2. Add `DXOgg` to the bottom of the `[EditPackages]` list in `DeusEx/System/DeusEx.ini`.  
3. To the same file, under `[Core.System]` and all your other `Paths=` statements, add the line `OggPath=..\MyPackage\Music\`.  

### With UnrealEd 2.2

1. Unpack the `UED22/DXOgg.u` file into your `DeusEx/UED22` folder.  
2. Add `DXOgg` to the bottom of the `[EditPackages]` list in `DeusEx/UED22/unrealtournament.ini`


## Working in UnrealEd

1. Restart UnrealEd if you haven't already  
2. From the `Actor Class Browser`, place a `DXOggMusicManager` actor.
3. Open its properties and define your music filenames there (they must be the exact filename of your track, including the .ogg extension)


## Troubleshooting

DXOgg is generally pretty good at logging errors, so if something isn't going as you expected, have a look in the game's log file (either in `DeusEx/MyPackage/System/*.log`, `DeusEx/System/DeusEx.log` or `Documents/Deus Ex/System/DeusEx.log`).
