---
title: Conversations
---

Conversations in Deus Ex are created using an external software called ConEdit.  


## Prerequisites
- [Actors](../actors)
- [Custom packages](../custom-packages)


## Create a folder

Create a `Conversations` folder inside your package, like so:

@filetree
    DeusEx:
        MyPackage:
            - Conversations


## Starting the conversation file

1. Open ConEdit
2. Create a new conversation (`File > New` or the `New` button). A `Conversation File Properties` dialog will show up.  
3. Pick `Mission 16` in the `Missions` tab ([explanation here](mission-numbers)) and click `<-- Add`.  
4. On the `Misc` tab, type in `Mission16` in the `Audio Package` field (no spaces, as this will eventually be part of a filename).  


## Creating a conversation  

1. Click `Conversation > Add conversation`.  
2. For the `Conversaion Name` field, type `MeetSomeGuy`.  
3. For the `Conversation Owner` field, type `SomeGuy`, and add the name to the table if prompted.  
4. Click `OK`.  
5. At this point, you can save the conversation file. Save it as `DeusEx/MyPackage/Conversation/Mission16.con`.  


## Create a speech event

1. With `MeetSomeGuy` highlighted, click `Event > Add > Speech`.  
2. In the `Actor to Speak` dropdown, select `SomeGuy`.  
3. In the `Speaking To` dropdown, type in `JCDenton`.  
4. Type something for Some Guy to say in the `Speech` text area.  
5. Click `Update`.  
6. Now click `Add Event` and notice that the actors have swapped places. 
7. Type in a smooth response and click `Update` again.  
8. When you're done, click `Close`.  
9. Finally, click `Event > Add > End` to close the conversation.  


## Create an import file

Create a new file called `ConversationImport.uc` inside your `DeusEx/MyPackage/Classes` folder with this content:

    class ConversationImport expands Object abstract;

    #exec CONVERSATION IMPORT FILE=Conversations\Mission16.con


## Rebuild your package

[Rebuild your package](../custom-packages#build) and observe that you now have 3 files in the `DeusEx/System` folder:  
- MyPackageAudioMission16.u  
- MyPackageText.u  
- MyPackage.u   


## Set up your map for conversations

1. Reopen UnrealEd.  
2. Ensure that your map has a `DeusExLevelInfo` actor in it (found in the `Info` section in the `Actor Class Browser`).  
3. Enter the properties dialog and expand the `DeusExLevelInfo` section.  
4. In the `Conversation Package` field, type `MyPackage`.  
5. In the `missionNumber` field, type `16`.  


## Add a character and try it out

1. Add a character from the `Actor > Pawn > ScriptedPawn > HumanCivilian` section of the `Actor Class Browser`.  
2. Now expand the `Conversation` section.  
3. In the `BindName` field, type `SomeGuy`.  
4. In the `FamiliarName` and `UnfamiliarName` fields, type `Some Guy`.  
5. If you want, you can also expand the `Orders` section and change the `Orders` field from `Wandering` to `Standing`.   
6. Click `Play Map`` to try it out.  


## Adding audio to a speech event

ConEdit and Deus Ex are very particular about the names, contents and locations of your audio files.  

The file structure looks like this:

@filetree
    DeusEx:
        MyPackage:
            Audio:
                Mission16:
                    PaulDenton:
                        MeetPaulDenton:
                            - Choice01A.mp3
                            - Choice01B.mp3
                            - PaulDenton01.mp3
                            - JCDenton01.mp3
                    InfoLink:
                        DL_EnterHallway:
                            - AlexJacobson01.mp3

1. Under `View > Options...`, set the `Audio Path` to `C:\DeusEx\MyPackage\Audio`.  
2. If you want help creating the file structure, click `File > Generate Audio Filenames`.  
3. Record some audio for for the lines and replace the placeholder files.  

    !!! Important
        The audio files must be 48kbps 44kHz mono .mp3 files with no silence on either end and no meta tags. [Here](../reference/linux-scripts#mp3-fix) is a helpful script for Linux users that automates this process.

5. Rebuild your package (remember to delete the existing .u files). 
6. Open UnrealEd (you might have to restart it) and click `Play Map!` to test it.  

    !!! Note
        If ConEdit complains about references to nonexisting labels after you moved events around by copying and pasting them, open the event in question and click `Update`, that should fix it.

    !!! Note
        The `Audio File` field is completely ignored, so just leave it.


## InfoLink conversations

Internally, Deus Ex is a bit confused as to whether they are called "InfoLinks" or "DataLinks", so the two terms are both used in different places.  

!!! Note
    In the base game, the convention is to create a separate conversation file called `Mission16_InfoLink.con`, but it's not necessary.

1. Create a new conversation and make your `JCDenton` the owner.  
2. Name it `DL_Test` (The `DL_` is short for "DataLink", just a convention).  
3. Set the `Actor To Speak` to `PaulDenton`.

    !!! Note
        If the `BindName` of the speaker matches a texture in the `InfoPortraits` texture group, it will show up in-game.  

4. Set the `Speaking To` to `JCDenton`.   
5. Type in something informative in the text area.  
6. Assign an audio file, if you like. It should be located at `MyPackage/Audio/Mission16/InfoLink/DL_Test/PaulDenton01.mp3`.  
7. Click `Update`.  
7. Add an `End` event, click `Update` again, save the conversation and rebuild.   
6. Place a `DataLinkTrigger` actor in your map (`Triggers > Trigger > DataLinkTrigger`).  
7. In the properties dialog, set `DataLinkTrigger > datalinkTag` to `DL_Test`.  
8. Right click in the little grey bar above the viewport and select `Actors > Radii View`.  
9. Back in the properties dialog, expand the `Collision` section and adjust `CollisionHeight` and `CollisionRadius` to your liking.  
10. Click `Play Map!` and walk into the collision area of the `DataLinkTrigger`.  


## Choice events

Choice events are a list of options for the player to choose from.

!!! Important
    Audio files for `Choice` events must be named like `Choice01A.mp3`, `Choice01B.mp3`, etc. The number is the number of `Choice` event in the conversation, and the letter is the option.  

Aside from the above little kink, the properties of each choice event more or less speak for themselves.

## Trigger events

The `Trigger tag` field corresponds to the `Events > Tag` of a `Trigger` actor in the map. It will be fired when this event comes up during a conversation. 

## Barks

Barks can be context sensitive, if you name the conversations after a particular scheme. It goes like this:

```
<BarkBindName, if available, or BindName>_Bark<Context>
```

So, for instance: `PaulDenton_BarkFutz` will be played if the player picks up an object with `Advanced > bOwned` set to `True` near Paul Denton, and if his `Reactions > bReactFutz` is set to `True`.

The available contexts are as follows:

Context                 | Description
----------------------- | -------------------------------------------------------------------------
Idle                    | Just murmuring to themselves
CriticalDamage          | Sustained critical damage
AreaSecure              | Spotted the player, but lost line of sight quickly
TargetAcquired          | Spotted the player running, or maintained line of sight
TargetLost              | lost sight of the player
GoingForAlarm           | Running for the alarm
OutOfAmmo               | Ran out of ammunition
Scanning                | Looking for the player
Futz                    | When the player is causing "futz", i.e. stealing and throwing things
OnFire                  | On fire
TearGas                 | Tear gas
Gore                    | When seeing gore
Surprise                | When the player suddenly shows up 
PreAttackSearching      | Sure the player was sighted, running to find them
PreAttackSighting       | About to engage
PostAttackSearching     | When the player escaped line of sight
SearchGiveUp            | Give up searching for the player
AllianceHostile         | When the alliance changes from friendly to hostile
AllianceFriendly        | When the alliance changes from hostile to friendly

## Execute scripts

There are 2 common approaches to this.


### Using a custom trigger

Add a `Trigger` event to your conversation and set the `Trigger tag` to the `Event > Tag` of your custom trigger.  

    class MyTrigger expands Trigger;

    function Trigger(Actor Other, Pawn Instigator)
    {
        Super.Trigger(Other, Instigator); // This is optional

        // Do something here
    }


### Using flags

Add a 'Set flags' event to your conversation and use it in your [mission script](mission-scripts).  

    function Timer()
    {
        Super.Timer();

        if ( Flags.GetBool('MyFlag') )
        {
            // Do something
        }
    }
