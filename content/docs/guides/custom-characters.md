---
title: Custom characters
---

## Prerequisites
- [Creating a custom package](../custom-packages)


## Create a new class

In order to use another player model than JC Denton, you'll need to create another class that inherits its properties from him.

!!! Important
    The `BindName` of your custom character must always be `JCDenton`. There are hardcoded references in the game, so certain things (like conversation choices) will break without it.  

This is an example class that turns the player into Tiffany Savage. The file has to be placed into `DeusEx/MyPackage/Classes/TiffanySavagePlayer.uc`. As always, make sure to [rebuild](../custom-packages#build) your package whenever youve made changes to any of the compiled assets within it.  

    class TiffanySavagePlayer extends JCDentonMale;

    event TravelPostAccept()
    {
        Super.TravelPostAccept();

        MultiSkins[0] = Texture'DeusExCharacters.Skins.TiffanySavageTex0';
    }

    defaultproperties
    {
         BindName="JCDenton"
         TruePlayerName="TiffanySavage"
         FamiliarName="Tiffany Savage"
         UnfamiliarName="Tiffany Savage"
         CarcassType=Class'DeusEx.TiffanySavageCarcass'
         bIsFemale=True
         BaseEyeHeight=38.000000
         Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
         MultiSkins(0)=Texture'DeusExCharacters.Skins.TiffanySavageTex0'
         MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
         MultiSkins(2)=Texture'DeusExCharacters.Skins.TiffanySavageTex0'
         MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
         MultiSkins(4)=Texture'DeusExItems.Skins.BlackMaskTex'
         MultiSkins(5)=Texture'DeusExCharacters.Skins.TiffanySavageTex0'
         MultiSkins(6)=Texture'DeusExCharacters.Skins.TiffanySavageTex2'
         MultiSkins(7)=Texture'DeusExCharacters.Skins.TiffanySavageTex1'
         CollisionRadius=20.000000
         CollisionHeight=43.000000
         JumpSound=Sound'DeusExSounds.Player.FemaleJump'
         HitSound1=Sound'DeusExSounds.Player.FemalePainSmall'
         HitSound2=Sound'DeusExSounds.Player.FemalePainMedium'
         Land=Sound'DeusExSounds.Player.FemaleLand'
         Die=Sound'DeusExSounds.Player.FemaleDeath'
    }

If you find that your custom character is not being used when playing your map, you can try modifying the `DeusEx/System/User.ini` under the `[DefaultPlayer]` section like this:

    Class=MyPackage.TiffanySavagePlayer


## Allow your new class to spawn in-game

Add this function to your `MyPackageGameInfo.uc` file:

    function bool ApproveClass( class<playerpawn> SpawnClass)
    {
        return true;
    }


## Change the first map

To use your own custom map as the first one in the campaign, you can override the `ShowIntro()` function:

    function ShowIntro(optional bool bStartNewGame)
    {
        if (DeusExRootWindow(rootWindow) != None)
            DeusExRootWindow(rootWindow).ClearWindowStack();

        bStartNewGameAfterIntro = bStartNewGame;

        AugmentationSystem.DeactivateAll();

        Level.Game.SendPlayer(Self, "16_My_Map"); // <- Your map name here
    }
