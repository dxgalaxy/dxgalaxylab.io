---
title: Actors
---

## Placing actors into a map

### Actor Class Browser

From the UnrealEd main menu, select `View > Actor Class Browser...`. Find the actor you want to place and highlight it.  

There are 2 ways to place the selected actor:  
1. Right click in the viewport where you want to place the actor, and select `Add <actor name> Here`.  
2. Hold down the `A` key and left click in the viewport where you want to place the actor.  

### Console

You can use the console, replacing `<class name>` with the desired [class](../reference/actors):

    actor add class=<class name>

The actor will be placed where you last left-clicked, or exactly where the camera is.

