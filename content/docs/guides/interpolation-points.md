---
title: Interpolation points
---

Interpolation points are used to create a curve along which to move an actor. Commonly used with cutscenes/cinematics.  

!!! Note
    For making cutscenes where no player interaction is involved, use `CameraPoint` instead.  

## Creating a path

1. Add a `Keypoint > InterpolationPoint` actor to your map.  
2. In the properties, set the `Events > Tag` to `MyPath`.  
3. Create a copy of the `InterpolationPoint` and place it somewhere else.  
4. In the properties, change the `InterpolationPoint > Position` to 1.  
5. Create another copy and set its position to 2.  
6. Still in the properties dialog, set `bEndOfPath` to `True`.  
7. Rotate each point according to which way you want the actor facing along the curve.  

!!! Note
    The points only influence the curve indirectly. If you want tighter control of the shape of the curve, you can place points closer together.

## Linking an actor to a path

Open the actor's properties and set `Events > Event` to `MyPath`.  


## Starting the interpolation

### With InterpolationTrigger

If you want the interpolation to happen during gameplay.  

1. Place a `Triggers > InterpolateTrigger` in your map.  
2. In the properties, set `Events > Event` to the `Events > Tag` of your actor.  

The order of events will be this:

1. Player touches trigger.  
2. Trigger emits event "MyActor".  
3. Actor tagged as "MyActor" emits event "MyPath".  
4. Interpolation points tagges as "MyPath" take control of the actor.  

Here is a little diagram that shows how things are connected:

    InterpolationTrigger        ┌─── "MyActor"
    └─ Events                   │ ┌─ "MyPath" 
       └─ Event ────────────────┤ │
                                │ │
    MyActor                     │ │
    └─ Events                   │ │
       ├─ Event ────────────────│─┤
       └─ Tag ──────────────────┘ │                      
                                  │
    InterpolationPoint0           │
    ├─ Events                     │
    │  └─ Tag ────────────────────┤
    └─ InterpolationPoint         │
       └─ Position = 0            │
                                  │
    InterpolationPoint1           │
    ⋯                             │
                                  │
    InterpolationPoint2           │
    ⋯                             │
                                  │
    InterpolationPoint3           │ 
    ├─ Events                     │
    │  └─ Tag ────────────────────┘
    └─ InterpolationPoint
       ├─ bEndOfPath = True
       ├─ Position = 3
       └─ RateModifier = 0.0


### With a MapExit

If you want the interpolation to control the camera during a transition at the end of a mission.  

1. Place a `MapExit` actor in your map.  
2. In the properties, note that the `Events > Tag` is `MapExit`. You can change it if you like.  
3. Expand the `MapExit` section in the properties.  
4. Set `bPlayTransition` to `True`.  
5. Set `cameraPathTag` to `MyPath`.  
6. Set `DestMap` to the name of your desired map to transition to.  
7. To trigger the `MapExit`, you can set up a `Trigger` with `Events > Event` set to `MapExit`. You can also trigger it from a [conversation](../conversations#trigger-events)
