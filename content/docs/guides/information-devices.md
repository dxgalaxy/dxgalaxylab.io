---
title: Information devices
---

In order to display text using data cubes, books and newspapers, an external text resource is needed.


## Prerequisites  

- [Custom packages](../custom-packages)
- [Textures](../textures)


@properties
    InformationDevices:
        imageClass:                 The image¹ to be transfered
        TextPackage:                The name of your package
        textTag:                    The name of the imported text

¹ More about [images](#images) 


## Create a folder

Create a `Text` folder inside your package, like so:

@filetree
    DeusEx:
        MyPackage:
            - Text


## Create a text file

Create a new text file called `16_MyText.txt` inside your `Text` folder. The 16 is your [mission number](mission-numbers) 

Text has a few HTML-like formatting tags available (not all of these are actually supported in Deus Ex):

Tag                         | Description
--------------------------- | ------------------------------------------
`<P>`                       | A paragraph
`<B>`                       | Bold
`<U>`                       | Underline
`<I>`                       | Italics
`<G>`                       | Graphic (?)
`<F>`                       | Font (?)
`<L>`                       | Label (?)
`<PLAYERNAME>`              | The player's chosen real name
`<NOTE>`                    | A note to add
`<GOAL>`                    | A goal to add
`<JL>`                      | Left aligned text
`<JC>`                      | Centered text
`<JR>`                      | Right aligned text
`<DC=0-255,0-255,0-255>`    | Default colour
`<C=0-255,0-255,0-255>`     | Text colour
`<COMMENT>`                 | A comment that will not be visible in-game

A common practice is to close all tags except the `<P>` tag, like so:

    <P><JC>This is a centered title</JC>
    <P>This is a longer sentence with a <DC=255,0,0>red</DC> word in it
    <COMMENT>This text will not be visible in-game</COMMENT>


## Create an import file

Create a new file called `TextImport.uc` inside your `DeusEx/MyPackage/Classes` folder with this content:

    class TextImport expands Object abstract;

    #exec DEUSEXTEXT IMPORT FILE=Text\16_MyText.txt


## Rebuild your package

Delete the `DeusEx/System/MyPackage.u` file if it's present and run this command from within the `DeusEx/System` folder:

    UCC.exe make


## Add the information device to your map

1. Open UnrealEd.  
2. Bring up the actor browser from `View > Actor Class Browser`.  
3. Navigate to the data cube, book or newspaper you want in the `Decoration > DeusExDecoration > InformationDevices` section.  
4. Right click where you want to place the actor and select `Add <actor> here`.  
5. Double click the actor (or right click and select `Properties`).  
6. Expand the `InformationDevices` section.  
7. In the `TextPackage` field, type `MyPackage`. 
8. In the `textTag` field, type `16_MyText`. 


## Images

This is an example of a custom image.

    class MyImage expands DataVaultImage;

    defaultproperties
    {
         imageTextures(0)=Texture'MyPackage.DataVaultImages.MyImage_1'
         imageTextures(1)=Texture'MyPackage.DataVaultImages.MyImage_2'
         imageTextures(2)=Texture'MyPackage.DataVaultImages.MyImage_3'
         imageTextures(3)=Texture'MyPackage.DataVaultImages.MyImage_4'
         imageDescription="A description of this image"
         colNoteTextNormal=(R=50,G=50)
         colNoteTextFocus=(R=0,G=0)
         colNoteBackground=(R=32,G=32)
    }

!!! Note
    It needs to be sliced into 4 256x256 image files, and the image itself should be 400x400. The remaining space should be black or magenta.
