---
title: Windows
---

A window in Deus Ex is an Unreal "mover" brush with a few extra features.

## Prerequisites
- [Movers](../movers)

## Create the template
1. Right click the `Cube` brush to bring up the cube builder menu.  
2. Type in height, width and breadth as 64x64x4.  
3. Click `Build`.  
4. Move the cube brush to a place in your map where it doesn't overlap with anything.  
5. Click `Add`.  
6. Apply the textures you want to the brush (the CoreTexGlass package is a good place to start).  

## Create the selection box
1. Create a new selection box with the `Cube` button.  
2. Make sure it encompasses the template window.
3. Click `Intersect`

You can safely delete the template brush now, if you like.  

## Create the mover brush
1. Move the new selection box to where you want the window to be.  
2. Right click the `Mover` brush and select `BreakableGlass`. 
3. Move the selection box out of the way to reveal the newly created `Mover` brush.  

The mover brush will not be rendered in the editor, so all you see is the purple outline. It will show up like normal once in-game, though.

## Finishing up
The window should be ready now. Build BSP and lighting and press `Play Map!` to check it out. 
