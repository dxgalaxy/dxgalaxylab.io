---
title: Custom packages
---

In order to play your mod, you will need to create a "package".


## Original source code

The #1 resource for learning how to script in Deus Ex is by studying the UnrealScript source code. Open UnrealEd 1 and click `Export All` in the `Actor Class Browser`. This will create a bunch of subfolders in your root game folder containing the .uc script files.   


## Folder structure

Create a subfolder in the main `DeusEx` folder with the name of your package, as well as a `Classes` folder inside that, like so:

@filetree
    DeusEx:
        MyPackage:
            - Classes

## GameInfo file

To register your package, you need to create a `MyPackageGameInfo.uc` file inside your `Classes` folder with this content:

    class MyPackageGameInfo extends DeusExGameInfo;


## Build

Whenever you make changes to any `.uc` files in your `Classes` folder, you need to run this command from inside your `DeusEx/System` folder:
    
    UCC.exe make

This will instruct the "UCC" compiler to build your package as `MyPackage*.u` files inside the `DeusEx/System` folder. [Here](../reference/linux-scripts#build) is a helpful Linux script to make things easier.  

!!! Important
    You must delete your compiled `MyPackage*.u` files before rebuilding, as UCC will not (always) overwrite them.

If you are using UnrealEd 2, in addition to building your package for Deus Ex, you must also use the `UCC.exe` bundled with UnrealEd 2 to build a version for the editor. For compilation to work, you have to comment out everything in your .uc files except for the class definition and default properties. If you're on Windows, you can use the <a href="/downloads/Trestkonizer.exe" download>Trestkonizer.exe</a> program. If you're on Linux, the aforementioned script has you covered.   


## Change DeusEx.ini

In order to tell Deus Ex to load your package at startup, open the `DeusEx/System/DeusEx.ini` file, scroll to the `[Engine.Engine]` section and change the `DefaultGame` parameter to the following:

    DefaultGame=MyPackage.MyPackageGameInfo

Also, to let UnrealEd know about your package, add this line to the end of the `[Editor.EditorEngine]` section:

    EditPackages=MyPackage
