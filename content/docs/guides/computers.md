---
title: Computers
---

This guide will cover adding personal computers, security consoles and public terminals to your map. You will find these actors in the `Actor Class Browser` under `Decoration > DeusExDecoration > ElectronicDevices > Computers`.  


## Prerequisites
- [Actors](../actors)
- [Custom packages](../custom-packages)

@properties
    Computers:
        ComputerNode:               The [node](#nodes) this computer belongs to
        lockoutDelay:               Seconds to lock out the player after a failed hack attempt
        nodeName:                   The name displayed at the login prompt
        specialOptions:             
            bTriggerOnceOnly:       Prevent retriggering the event
            Text:                   Button text
            TriggerEvent:           Name of the event to be triggered
            TriggerText:            Text to send with the event as a parameter
            UnTriggerEvent:         Name of the event to untrigger
            userName:               User that triggered the event
        TextPackage:                Your package name
        titleString:                The title of the login prompt (not implemented)
        titleTexture:               The logo of the login prompt (not implemented)
        UserList:                   
            accessLevel:            Hacking skill needed to access this account 
            Password:               Account password
            userName:               Account username

## Personal computers

### Emails

1. Create a file called `16_Email01.txt` (mission number + "Email" + number, it's a convention) in the `DeusEx/MyPackage/Text` folder.  
2. Write in something like this. Unlike with [information devices](information-devices), only the `<P>` and `<COMMENT>` tags work here.   
        
        <P>Hi,
        <P>
        <P>This is a test email
        <P>
        <P>Peace,
        <P>Jordan Shea

3. Create another text file called `16_EmailMenu_myuser.txt` (another convention) like this:

        <EMAIL=16_Email01,Test email,JordanShea,myuser,>
       
    !!! Note
        The structure is email filename, subject, sender, username (several can be added)


4. Create/modify a file called `TextImport.uc` inside your `DeusEx/MyPackage/Classes` folder with this content:

        class TextImport expands Object abstract;

        #exec DEUSEXTEXT IMPORT FILE=Text\16_Email01.txt
        #exec DEUSEXTEXT IMPORT FILE=Text\16_EmailMenu_myuser.tx

5. Rebuild your package.  
6. Add a `DeusExLevelInfo` actor to your map, if your haven't already, and set the mission number to 16.  
7. Add a `Decorations > DeusExDecorations > ElectronicDevices > Computers > ComputerPersonal` to your map.  
8. Open its properties and add a an account to the `Computers > UserList` with the username `myuser`.  
9. Set the `TextPackage` value to the name of your package.  


## Nodes

Computer nodes are unfortunately hardcoded in Deus Ex, so we need to do a bit of monkey wrenching to define our own. Create a new class called `ComputerNodeDistributor.uc` and paste this:

    class ComputerNodeDistributor extends KeyPoint;

    var() string NodeName;
    var() string NodeDesc;
    var() string NodeAddress;
    var() Texture NodeTexture;

    function PostBeginPlay()
    {
        local Computers aComputer;

        if(Event == '')
            return;

        foreach AllActors(class'Computers', aComputer, Event)
        {
            aComputer.SetPropertyText("ComputerNode", "CN_UNATCO");
            aComputer.SetPropertyText("NodeInfo", "(nodeName=\"" $ NodeName $ "\",nodeDesc=\"" $ NodeDesc $ "\",nodeAddress=\"" $ NodeAddress $ "\",nodeTexture=Texture'" $ NodeTexture $ "')");
        }
    }

Then add this actor to your map, set its `Event` to the `Tag` of the computers you want to modify, and fill in the values of `ComputerNodeDistributor`'s properties as you like.
