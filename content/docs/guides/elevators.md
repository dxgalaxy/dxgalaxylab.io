---
title: Elevators
---

## Prerequisites
- [Movers](../movers)

## Create the mover brush
1. Create a brush, or collection of brushes, that forms your elevator.  
2. Create a slection box with the `Cube` button.  
3. Ensure that the selection box encompasses the elevator geometry.  
4. Click `Intersect`.  
5. Right click the `Mover` button and select `ElevatorMover`.  
6. You can now move the selection brush out of the way and delete the original brushes.  

## Set up the keyframes  
1. Right click the mover brush and select `Movers > Key 1`.  
2. Rotate/move the door into the desired rotation/position.  
3. Right click the mover brush again and select `Movers > Key 0 (Base)`. 

## Set up the elevator properties
1. Open the properties for the mover brush, either by double clicking, pressing F4, or by right clicking and selecting `Properties`.  
2. Navigate to the sounds you want the elevator to make in the `Sound Browser` under the `MoverSFX`. You can double click sounds to preview them.    
3. Expand the `MoverSounds` section under the mover brush properties.  
4. Select the opening sound you want in the browser and click `Use` next to the appropriate field in the mover brush properties.  
5. If your elevator moves between light and dark areas, you might want to switch on the `Mover > bDynamicLightMover` field.  
6. Set the `Events > Tag` to `MyElevator`.  

## Set up sequence triggers
1. Add a `Triggers > Trigger > SequenceTrigger` to your map.  
2. Open its properties and set `Events > Event` to `MyElevator`.  
3. Set `Events > Tag` to `MyElevatorDown`.  
4. Set `SequenceTrigger > SeqNum` to `0`. This refers to the `Key 0 (Base)` of the mover brush.    
5. Repeat 1-4, with `MyElevatorUp` and `SeqNum` set to `1`.  

## Set up buttons
1. Add a `Decoration > DeusExDecoration > Button1` and place it somewhere on the mover brush.
2. Open its properties and set `Button1 > ButtonType` to whatever is appropriate in your situation.  
3. Set `DeusExDecoration > moverTag` to `MyElevator`, so it will follow the mover brush.    
4. Set `Events > Event` to `MyElevatorUp`.  
5. Repeat 1-4, but using `MyElevatorDown`.  
