---
title: Installing the Deus Ex SDK
---

## Linux

### Wine

Throughout these guides, whenever running an `.exe` is mentioned, run it through Wine with these environment variables set:

    WINEPREFIX="$HOME/.deus-ex-sdk" WINEARCH=win32 [COMMAND HERE]

This means that your `C:\DeusEx` folder will be located at `~/.deus-ex-sdk/drive_c/DeusEx`

Create the Wine prefix and install the necessary dependencies:

    WINEPREFIX="$HOME/.deus-ex-sdk" WINEARCH=win32 wine wineboot
    WINEPREFIX="$HOME/.deus-ex-sdk" WINEARCH=win32 winetricks mfc42 vb6run


#### Notes

- UnrealEd works well in Wine and X11. Wayland is not recommended, as there are multiple input and rendering issues.   
- Fields in the properties dialog sometimes have `Use` and `Clear` buttons, but they might be invisible.  


### Virtualised environment

When using virtualisation, such as Gnome Boxes or VirtualBox, the recommended setup is a Windows 7 image with mouse integration turned off.  


## Install Deus Ex

Since it's the SDK's preferred directory, install Deus Ex at `C:\DeusEx`.

If you value your sanity, download <a href="/downloads/DeusExe-v8.1.zip" download>Marijn Kentie's launcher</a> and <a href="/downloads/dxglr20.zip" download>Chris Dohnal's OpenGL renderer</a> and unzip them in the `C:\DeusEx\System` folder. Select the OpenGL renderer the first time the game starts.  

If the game is too dark, you can open the console by pressing `T`, removing the `Say` prompt and type `preferences`. Then under `Rendering > OpenGL Support` set `OneXBlending` to `False`. 


## Install the SDK

Install/extract the SDK into `C:\DeusEx` as well. There are 2 major options. 

### Official SDK

<a href="/downloads/DeusExSDK1112f.exe" download>DeusExSDK1112f.exe</a>

Installs to `DeusEx/System/UnrealEd.exe`  

The good:  
- It's the most compatible option. Everything works as intended.

The bad:  
- Frequent crashes.  
- Wine/Linux: All viewports have to be floating windows. If they're docked, mouse movement goes haywire.  
- Default file extension in the file browser is .unr, should be .dx.  
- Muddy shadows.  


### Patched UnrealEd 2.2 from OldUnreal

<a href="/downloads/UED2_2_For_DeusEx.7z" download>UED2_2_For_DeusEx.7z</a>

Installs to `DeusEx/UED22/unrealed.exe`  

!!! Note
    If the master browser (actor/mesh/texture/etc.) doesn't show up it's likely to do with your screen size. Edit the `DeusEx/UED22/UnrealEd.ini` and change the positions of the browsers. They're likely set to `X=1717` or something like that. Just lower that number, and maybe set the file to read-only as well.

The good:  
- More stable than UEd1.  
- Many improvements in general, notably mapping features.   
- Better lighting/shadows.  

The bad:  
- Some actors are invisible.  
- An extra step required when building: Another version must be built where everything except member variables and `defaultproperties` has been removed.   
