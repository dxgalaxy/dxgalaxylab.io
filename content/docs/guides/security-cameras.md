---
title: Security cameras
---

## Prerequisites
- [Actors](../actors)

@properties
    Events: 
        Event:                  Can be set to the tag of AutoTurrets to activate them upon detection
        Tag:                    To set up with a ComputerSecurity, provide this tag as its Views > [0-2] > cameraTag
    HackableDevices: 
        bHackable:              Whether it's hackable
        hackStrength:           How many multitools are needed to hack (0.1 is 1 multitool at untrained level)
        initialHackStrength:    ???
    SecurityCamera:
        bActive:                Whether it's on initially
        bNoAlarm:               Whether it will trigger an alarm
        bSwing:                 Whether it pans from side to side
        cameraFOV:              The field of view. Default is 4096 (22.5°)
        cameraRange:            How far it can see into the distance 
        swingAngle:             For far it will pan
        swingPeriod:            How long it takes to pan back and forth once
