---
title: Music
---

## Adding music to your level:  

1. Open the `Music Browser` and click the `Open Package` button.  
2. Find the `.umx` file you want to use and open it.  
3. Open `View > Level Properties` (or press `F6`) and expand `Audio`.  
4. Next to the `Song` field, press the `Use` button.  

See also [DXOgg](../dxogg) for playing .ogg music files in your map.
