---
title: Mission numbers
---

In Deus Ex, every chapter, or "mission", is enumerated 00 to 15. Whenever creating maps, conversations or text content, a mission number is being referred to. When making mods, you can use numbers 16-97. If the mission number is 0, then the player can't die.
