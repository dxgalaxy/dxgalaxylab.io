---
title: Movers
---

Deus Ex movers are commonly used for windows and doors. 

@properties
    DeusExMover:
        bBreakable:                 If the mover can be broken
        bDrawExplosion:             Add an explosion visual upon breaking
        bFragmentTranslucent:       Broken pieces are transparent 
        bFragmentUnlit:             Broken pieces are unlit
        bFrobbable:                 Can be interacted with 
        bHighlight:                 Shows up as interactable object
        bInitialLocked:             Door locked at level start
        bIsDoor:                    Is used as a door
        bIsLocked:                  State of the door used in-game
        bOneWay:                    Elevator obly moves in one direction
        bPickable:                  Door can be picked
        doorStrength:               Door hit points
        ExplodeSound1:              Small explosion sound
        ExplodeSound2:              Large explosion sound
        FragmentClass:              A class of mesh to use as broken pieces
        FragmentScale:              The size of the broken pieces
        FragmentSpread:             The spread of the broken pieces
        FragmentTexture:            The texture of the broken pieces
        initialLockStrength:        How many lockpicks it takes to pick the door (0.1 is one lockpick at untrained skill level)
        KeyIDNeeded:                The name/id of the door key
        lockStrength:               Same as initialLockStrength, but as a transient value
        minDamageThreshold:         ???
        NumFragments:               How many broken pieces to render  
        TimeSinceReset:             ???
        TimeToReset:                ??? 
    ElevatorMover:
        bFollowKeyFrames:           ??? 
    Mover:
        bDamanageTriggered:         Moves upon receiving damage
        bDynamicLightMover:         Updates shadows when moving
        BrushRayTraceKey:           ???
        bSlave:                     ???
        bTriggerOnceOnly:           Can only move once
        BumpEvent:                  Event to fire when touched
        BumpType:                   Which type of actor to consider when touched
        bUseTriggered:              ???
        DamageThreshold:            How much damage to take before moving
        DelayTime:                  Seconds to wait before moving
        EncroachDamage:             Damage to deal when hitting a pawn while moving
        KeyNum:                     Which keyframe to start on
        MoverEncroachType:          Type of effect when hitting a pawn while moving
        MoverGlideType:             ???
        MoveTime:                   Seconds the animation takes to play
        NumKeys:                    Number of keyframes in animation
        OtherTime:                  ???
        PlayerBumpEvent:            Event to fire when touching player
        ReturnGroup:                ???
        StayOpenTime:               Seconds to maintain the "open" position before returning
        WorldRaytraceKey:           The key at which light will be computed 
    MoverSounds: 
        ClosedSound:                Sound at the end of the closing animation 
        ClosingSound:               Sound at the beginning of the closing animation
        MoveAmbientSound:           Sound during animation
        OpenedSound:                Sound at the end of the opening animation
        OpeningSound:               Sound at the beginning of the opening animation

