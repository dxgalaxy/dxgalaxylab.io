---
title: NPCs
---

## Prerequisites
- [Actors](../actors)

All NPCs can be found in the `Actor Class Browser` under `Pawn > ScriptedPawn`.

!!! Note
    The patrolling algorithm in vanilla Deus Ex is quite broken and can cause crashes. It is highly recommended to subclass `ScriptedPawn` and change the patrolling state to [this](/assetlib/scripts/better-patrolling/)

!!! Note
    Another problem with navigation is `InventorySpot`s. The edtor places them automatically with each `Inventory` actor, and they're hidden. The only way to make sure they're removed is to search for actors with the name `InventorySpot` and delete them before playing the map. Whenever navigation is rebuilt, however, they will be back again. A more permanent solution is to use [UnrealEd 2.2](/downloads/UED2_2_For_DeusEx.7z) and replace the `Engine.u` file with [this one](/downloads/UED2_2_DoNotPlaceInventorySpots.u)

## Alliances

An NPC will have an either positive or negative relationship towards a group or the player. 

@properties
    Alliances:
        Alliance:                   The name of the group this NPC belongs to, e.g. "MJ12" 
        InitialAlliances:
            AllianceLevel:          Accepted values are -1 for hostile, 0 for neutral and 1 for friendly
            AllianceName:           The name of the group. Can also just be "Player".
            bPermanent:             When true, this relationship cannot be changed, no matter what


### Make an NPC an enemy of the player

1. Add an NPC to your map and open its properties.  
2. Set `Inventory > InitialInventory > [0] > Inventory` to `WeaponPistol`.   
3. Expand `Alliances > InitialAlliances > [0]`.  
4. Set `AllianceLevel` to -1.  
5. Set `AllianceName` to `Player`.  
6. Set `bPermanent` to `True`.  


### Make an NPC an enemy of another NPC

1. Add an NPC to your map and open its properties.  
2. Set `Inventory > InitialInventory > [0] > Inventory` to `WeaponPistol`.   
3. Expand `Alliances`.  
4. Set `Alliance` to `NPC1`.  
5. Expand `InitialAlliances > [0]`.  
6. Set `AllianceLevel` to -1.  
7. Set `AllianceName` to `NPC2`.  
8. Set `bPermanent` to `True`.
9. Perform steps 1-8 again, but swap `NPC1` and `NPC2`.


### Change alliances using AllianceTriggers

1. Add an NPC to yor map and open its properties.  
2. Set `Inventory > InitialInventory > [0] > Inventory` to `WeaponPistol`.   
3. Set `Events > Tag` to `MyEnemy`.  
4. Add a `Triggers > Trigger > AllianceTrigger` from the `Actor Class Browser` to your map.  
5. Open its properties and expand `AllianceTrigger > Alliances > [7]`.  
6. Set `AllianceLevel` to -1.  
7. Set `AllianceName` to `Player`.  
8. Set `bPermanent` to `True`. 
9. Set `Events > Event` to `MyEnemy`.   
10. Play the map and walk into the trigger.  

A diagram for clarity:

    AllianceTrigger                 ┌─── "MyEnemy"
    ├─ Events                       │ 
    │  └─ Event ────────────────────┤
    └─ Alliances                    │
       └─ [7]                       │
          ├─ AllianceLevel = -1     │
          ├─ AllianceName = Player  │
          └─ bPermanent = True      │
                                    │
    MyEnemy                         │
    ├─ Events                       │
    │  └─ Tag ──────────────────────┘
    └─ Inventory                  
       └─ InitialInventory
          └─ [0]
             └─ Inventory = WeaponPistol

!!! Note
    The alliances set in the `AllianceTrigger` will override the `InitialAlliances` defined on the NPC whose `Events > Tag` match the trigger's `Events > Event`, so it's a good idea to leave the first few alliances empty on the trigger.  

!!! Note
    If you want to trigger an alliance change during a conversation, it's best to use the [mission script](../mission-scripts#firing-a-trigger-during-a-conversation-between-npcs), as the player can cancel the conversation midway by running off.   

## Behaviour

@properties
    AI:
        bEmitDistress:              Emit distress to other NPCs
        RaiseAlarm:                 When to raise an alarm
    Fears:
        bFearHacking:               Run away from a hacker
        bFearWeapon:                Run away from a person holding a weapon
        bFearShot:                  Run away from a person who fires a shot
        bFearInjury:                Run away from a person who causes injury
        bFearIndirectInjury:        Run away from a person who causes indirect injury
        bFearCarcass:               Run away from a carcass
        bFearDistress:              Run away from a person causing distress
        bFearAlarm:                 Run away from the source of an alarm
        bFearProjectiles:           Run away from a projectile
    Reactions:
        bReactFutz:                 React to the player causing a disturbance
        bReactPresence:             React to the presence of an enemy (attacking)
        bReactLoudNoise:            Seek the source of a loud noise (seeking)
        bReactAlarm:                Seek the source of an alarm (seeking)
        bReactShot:                 React to a gunshot fired by an enemy (attacking)
        bReactCarcass:              React to gore appropriately (seeking)
        bReactDistress:             React to distress appropriately (attacking)
        bReactProjectiles:          React to harmful projectiles appropriately
    Stimuli:
        bHateHacking:               Attack hackers
        bHateWeapon:                Attack player when visibly wielding weapons
        bHateShot:                  Attack a shooter
        bHateInjury:                Attack instigator of direct injury
        bHateIndirectInjury:        Attack instogator of indirect injury
        bHateCarcass:               Attack the killer of carcasses
        bHateDistress:              Attack the distressor emitted by other NPCs



## Combat

@properties
    Combat:
        bAimForHead:                They will always aim for their opponent's head
        BaseAccuracy:               The precision with which they fire weapons¹

¹ Lower `BaseAccuracy` gives `ScriptedPawn`s higher reload speed, more rate of fire (including during full auto), and tighter aim. A value as low as -0.25 might give many weapons literally perfect aim. -0.1 to -0.15 is generally a range one might use for boss type enemies, but it will start by calculating with the base stats of the weapon they have equipped.
    

## Orders

@properties
    Orders:
        AlarmTag:                   Which alarm panel they will seek out when alerted
        bFixedStart:                ???
        HomeExtent:                 The radius in Unreal Units within which they will wander from home
        HomeTag:                    The home position indicated by the Events > Tag of a PatrolPoint
        Orders:                     The order¹ to give
        OrderTag:                   Indicates the Events > Tag of the target of the order
        SharedAlarmtag:             ???

¹ Possible values for the `Orders` field:

Order           | OrderTag          | Description
--------------- | ----------------- | -----------
`Dancing`       |                   | Plays the dancing animation. Use with the `AI > bHokeyPokey` property to make them spin
`Following`     | `Pawn`            | Follows the target `Pawn`. Leave empty to follow the player.
`GoingTo`       | `PatrolPoint`     | Walks to the target `PatrolPoint`
`Idle`          |                   | Does nothing
`Patrolling`    | `PatrolPoint`     | Follows the assigned patrol path
`RunningTo`     | `PatrolPoint`     | Runs to the target `PatrolPoint`
`Shadowing`     | `Pawn`            | Follows the target `Pawn` sneakily. Leave empty to shadow the player.
`Sitting`       | `Seat`            | Sits down on a `Seat` (from `Decorations > DeusExDecorations > Furniture`)
`Standing`      |                   | Plays the standing animation
`WaitingFor`    | `Pawn`            | Waits for a `Pawn`
`Wandering`     |                   | Wanders within the `HomeExtent` of the `HomeTag` position


!!! Seats

    To get a `ScriptedPawn` to sit down when a map loads: 

    - Make sure there is ample room between the seat and any other actors and brushes
    - Place the `ScriptedPawn` directly facing the `Seat`
    - Ensure that `ScriptedPawn` is not initially within view of the player


## General settings
   
@properties
    ScriptedPawn:
        bHighlight:                 Whether they will show the highlight interaction box
        bImportant:                 If true, _Dead and _Unconscious flags will be set for them
        bInvincible:                If true, they cannot be killed
        bInWorld:                   If false, they are initially hidden
        ClotPeriod:                 The amount of seconds they take to go back to their default state after reacting to an event
        HealthArmLeft:              Hit points of the left arm 
        HealthArmRight:             Hit points of the right arm 
        HealthHead:                 Hit points of the head  
        HealthLegLeft:              Hit points of the left leg  
        HealthLegRight:             Hit points of the right leg  
        HealthTorso:                Hit points of the torso  
        SurprisePeriod:             The amount fo seconds they take to pursue an enemy after seeing them 
        WalkSound:                  Overlays an extra walking sound on top of their footsteps


## Miscellaneous use cases

### Detecting when a character is dead or unconscious

1. Place any `Pawn > ScriptedPawn > ...` in your map, enter its  properties and expand the `ScriptedPawn` section.  
2. Set `bImportant` to `True`.   
3. Expand the `Conversation` section and assign its `BindName` to something.  
4. Test it by killing/knocking out the actor, bringing up the `legend` menu and clicking `Edit Flags`.  
5. It should show up as `[BindName]_Dead` and `[BindName]_Unconscious`.  

!!! Note
    As fas as flags are concerned, a character who is unconscious is also dead. So remember to check for the `_Unconscious` flag first, if you're using it in a conversation context.   


