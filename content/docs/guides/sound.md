---
title: Sound
---

Any `Actor` can play a sound, either in a loop or as a one-shot.

## Prerequisites
- [Actors](../actors)
- [Custom packages](../custom-packages)


!!! Note
    Sounds should in general be mono, 22kHz, 16bit PCM .wav files


@properties
    Sound:
        AmbientSound:               Sound to play on a loop
        SoundPitch:                 Pitch at which the sound is played
        SoundRadius:                Radius within which the sound can be heard (0-255, not in Unreal Units)
        SoundVolume:                Volume at which the sound is played
        TransientSoundRadius:       Default radius when using PlaySound() 
        TransientSoundVolume:       Default volume when using PlaySound()


## Play a looping sound

!!! Note
    In order to make looping sounds work properly, the files need to be tagged with LOOPSTART and LOOPEND tags. The values are integers representing the loop's boundaries in samples.

To play a looping sound is simple, just set the `AmbientSound` value to the sound of your choice.


## Play a one-shot sound

To play a sound once, use this code:

    aMyActor.PlaySound(
       Sound                Sound,              // The sound object
       optional ESoundSlot  Slot,               // The slot¹
       optional float       Volume,             // The volume (0.0 - 1.0)
       optional bool        bNoOverride,        // Prevent other sounds from butting in
       optional float       Radius,             // The radius in Unreal Units
       optional float       Pitch,              // The pitch (0.5 - 2.0)
       optional bool        bAttenuate          // Whether to play the sound in 3D
    )

¹ Possible slots:  

- `SLOT_None`  
- `SLOT_Misc`  
- `SLOT_Pain`  
- `SLOT_Interact`  
- `SLOT_Ambient`  
- `SLOT_Talk`  
- `SLOT_Interface`  


## Importing sounds

This can either be done via the sound browser (at least in Unreal Editor 2.x), or via script:

    #exec AUDIO IMPORT FILE="Sounds\Ambient\MyLoop.wav" NAME="MyLoop" GROUP="Ambient"

