---
title: Light switches
---

## Prerequisites

- [Actors](../actors)


## The light

1. Place a light of type `TriggerLight` in your map.  
2. Open the light's properties.  
3. Set `TriggerLight > bInitiallyOn` to `True`.  
4. Set `Object > InitialState` to `TriggerToggle`.  
5. Set `Events > Tag` to `MyTriggerLight`.  


## The switch

1. Place a light switch actor (`Decoration > LightSwitch`) in your map.   
2. Open the light switch's properties.  
3. Set `Events > Event` to `MyTriggerLight`.  


## Finishing up

Rebuild the lighting in your map and click `Play Map!` to test it out.
