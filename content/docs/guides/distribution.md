---
title: Distribution
---

This guide is about distributing mods

## Prerequisites
- [Custom packages](../custom-packages)

@filetree(Structure)
    MyPackage:
        Maps:
            - "*.dx"
            - "DX.dx¹"
        Music:
            - "*.umx"
        Sounds:
            - "*.uax"
        System:
            - "*.u"
            - "DeusEx.ini²"
            - "User.ini³"
        Textures:
            - "*.utx"

¹ If you want to replace the main menu scene.  
² A custom `DeusEx.ini` file with the following changes:

    [Engine.Engine]
    DefaultGame=MyPackage.MyPackageGameInfo

    [Core.System]
    # Place these lines BEFORE the originals
    Paths=..\MyPackage\Maps\*.dx
    Paths=..\MyPackage\Music\*.umx
    Paths=..\MyPackage\Sounds\*.uax
    Paths=..\MyPackage\System\*.u
    Paths=..\MyPackage\Textures\*.utx

³ For custom characters, a `User.ini` file with only this change:

    [DefaultPlayer]
    Class=MyPackage.TiffanySavagePlayer # Or whatever your class is called
