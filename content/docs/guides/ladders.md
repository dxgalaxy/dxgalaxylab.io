---
title: Ladders
---

To make a surface act as a ladder, it simply needs to have a texture applied that belong to a `Ladder` group in a texture package. You can either use the texture directly on a surface, or apply it behind some 3D geometry meant to represent a ladder. The textures mostly used in the game are in the `CoreTexMetal` package in the `Ladder` group.

!!! Note
    It can ce tricky making a ladder from a sheet brush. It will only work from one side, and the brush has to be solid (uncheck `Non-solid` in the surface's flags). Also, sheet brushes that penetrate other geometry can create bSP holes. It's generally safer to use a 1 unit thick box brush instead. 
