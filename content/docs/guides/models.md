---
title: Models
keywords: mesh,model,3d
---

Unreal 3D models consist of a data file (`MyModel_d.3d`) and an animation file (`MyModel_a.3d`). Deus Ex uses a modified format, which enables higher precision, but cuts the max vertex count in half. It expects all animations to be played back one after another and then divided into named sequences. This can be a very temperamental process, as every single factor has to align perfectly for the mesh and animations to work as expected.

!!! Animations
    
    As Unreal Engine may not agree with your 3D software on what counts as a frame of animation, the amount of frames may change after import. Therefore the most reliable way to import animations is to export them from the scene timeline and manually enter the sequences in the import file. You can initially just import the "All" animation containing all frames into UnrealEd, and then click through the frames in the Mesh Browser to determine where your animations should start and end.


!!! Surfaces

    To make a surface shiny or reflective, the polygon must be flagged on the model itself ([instructions for Blender here](#polytypes-polyflags-and-texnum)). The reflection map must be set as the `Texture` property of the actor.


## Prerequisites
- [Custom packages](../custom-packages)


## Relevant tools
- [DEED](/downloads/DEED.zip)  
- [HTK](/downloads/HTK-20160618.zip)
- [3ds2unr](/downloads/3ds2unr.exe)
- [unr2de](/downloads/unr2de.zip)


## Import scripts

All models need to be imported via scripts. Here's an example import script:

```
class MeshImport expands Object abstract;

#exec MESH IMPORT MESH=GM_Jumpsuit ANIVFILE=Models\GM_Jumpsuit_a.3d DATAFILE=Models\GM_Jumpsuit_d.3d
#exec MESH ORIGIN MESH=GM_Jumpsuit X=0 Y=0 Z=12200 YAW=64
#exec MESH LODPARAMS MESH=GM_Jumpsuit STRENGTH=0.5

#exec MESH SEQUENCE MESH=GM_Jumpsuit SEQ=All                STARTFRAME=0   NUMFRAMES=372
#exec MESH SEQUENCE MESH=GM_Jumpsuit SEQ=Still              STARTFRAME=0   NUMFRAMES=1
#exec MESH SEQUENCE MESH=GM_Jumpsuit SEQ=Walk               STARTFRAME=1   NUMFRAMES=10  RATE=10

#exec MESHMAP SCALE MESHMAP=GM_Jumpsuit X=0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GM_Jumpsuit SEQ=Walk                 TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit SEQ=Walk                 TIME=0.6    FUNCTION=PlayFootStep
```

## Jumbled vertices when animating

A couple of ways this could happen:

- If you have overlapping vertices in the model (common with hinged animations).
- The current animation's number of frames differs between your 3D software and the import code.
- You still have unapplied modifiers, like mirror.


## Blender

Use [this addon](/downloads/UE1-VertexMesh-Blender-IO.zip) to import/export models into/from Blender.

### Export checklist

- The animation frame rate is 10 FPS (will still be smooth in-game).
- The model is a single object with all modifiers except armature applied.
- You're in object mode.
- The object is selected.
- The object is a child of an armature.
- All the actions you want to export are present as strips in the NLA stack, in sequence, and they're not muted.
- All strips have extrapolation set to "nothing".
- When playing back the scene timeline, all the animations you want to export are played back in order.
- The model is smaller than 255 Blender units on all axies.
- Vertex count is equal to or less than 8191.
- Export settings:
    - Format: Ion Storm
    - Source: Scene timeline

### Polytypes, Polyflags and Texnum

**Polytypes**, **Polyflags** and **texnum** are handled by material name. The material should start with a three digits containing a number between 0 and 255 (if not it will use Blender's material index instead) which is the **texnum**. After this you can add one of the following **Polytypes** (case sensitive):

- `_NORMAL/_NM` = The default. Used if none of these are given.
- `_2SIDED/_DD` = **2-sided**
- `_TRANSLUCENT/_LU` = **translucent** and **2-sided**
- `_MASKED/_CL` = **masked* and **2-sided**
- `_MODULATED/_MD` = **modulated** and **2-sided**
- `_ALPHABLEND/_AB` = **alphablend* and **2-sided** (Supported by OldUnreal's Unreal 227 patch only).
- `_WEAPONTRI/_PH` = makes this polygon a **weapon triangle** (used to place the weapon in 3rd person view).

You can also add these **Polyflags** to the name. Unlike **Polytypes** can you have more than one of these at the same time (also case sensitive):

- `_UNLIT/_UL` = **Unlit**.
- `_FLAT/_FL` = Not really sure to be honest. Probably related to the mesh curvy thingy.
- `_ENVIRONMENT/_RF` = Render this poly to make it look **shiny/reflective**.
- `_NOSMOOTH/_NS` = **No texture smoothing** (see: Minecraft)

Example: The following name would make the polygons assigned to this material **translucent**, **unlit** and **shiny/reflective** while using texture slot 0:

    000_UL_TRANSLUCENT_ENVIRONMENT_

Note how the order of the flags don't matter. The only thing that has a specific spot in the name is the texture number.

You can have different flags for different polygons while still make them use the same texture slot. So lets say I make another one and name it like this:

    000_MD_2SIDED_

Would it use the same texture slot while making these polygons **modulated** and **2-Sided**.

The texture name (used for the .uc file) is read from the currently active (selected) **Image Texture** node in the material.


## Unreal Editor 2.2

For the mesh to show up correctly in UnrealEd 2.2, a different model with different import statements is required.

### Export a separate model

Export a new model using the "Standard" format and use the import statements generated in the .uc file

### Convert an existing model

1. Use [this program](/downloads/u2de3d.exe) to convert it: `u2de3d -de2u source_a3d dest_a3d`.  
2. Modify the import script `MESHMAP SCALE` using this formula: `X=X*32 Y=Y*32 Z=Z*64`  
3. If there is `MESHMAP ORIGIN`, modify it using this formula: `X=X/32 Y=Y/32 Z=Z/64`  

### Example

This example file uses the [build.py](/docs/reference/python-scripts/#build) script.

```
class VTOL extends Vehicles;

// BEGIN UE1

#exec MESH IMPORT MESH=VTOL ANIVFILE=Models\VTOL_a.3d DATAFILE=Models\VTOL_d.3d X=0 Y=0 Z=0 LODSTYLE=10 LODFRAME=0 
#exec MESH ORIGIN MESH=VTOL X=0 Y=0 Z=0 YAW=-64 PITCH=0 ROLL=0

#exec MESH SEQUENCE MESH=VTOL SEQ=ALL    STARTFRAME=0 NUMFRAMES=122 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=Still    STARTFRAME=0 NUMFRAMES=1 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=Flying    STARTFRAME=2 NUMFRAMES=1 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=MotorsVertical    STARTFRAME=3 NUMFRAMES=39 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=MotorsHorizontal    STARTFRAME=41 NUMFRAMES=39 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=WheelsDown    STARTFRAME=81 NUMFRAMES=19 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=WheelsUp    STARTFRAME=100 NUMFRAMES=19 RATE=10

#exec MESHMAP NEW MESHMAP=VTOL MESH=VTOL
#exec MESHMAP SCALE MESHMAP=VTOL X=0.003125 Y=0.003125 Z=0.003125

#exec TEXTURE IMPORT NAME=VTOL1 FILE=Textures\Decorations\VTOL1.pcx GROUP=Decorations FLAGS=2
#exec MESHMAP SETTEXTURE MESHMAP=VTOL NUM=0 TEXTURE=VTOL1

#exec TEXTURE IMPORT NAME=VTOL2 FILE=Textures\Decorations\VTOL2.pcx GROUP=Decorations FLAGS=2
#exec MESHMAP SETTEXTURE MESHMAP=VTOL NUM=1 TEXTURE=VTOL2

#exec TEXTURE IMPORT NAME=VTOL3 FILE=Textures\Decorations\VTOL3.pcx GROUP=Decorations FLAGS=2
#exec MESHMAP SETTEXTURE MESHMAP=VTOL NUM=2 TEXTURE=VTOL3

// END UE1

// BEGIN UE2

#exec MESH IMPORT MESH=VTOL ANIVFILE=Models\VTOL_UNR_a.3d DATAFILE=Models\VTOL_UNR_d.3d X=0 Y=0 Z=0 LODSTYLE=10 LODFRAME=0 
#exec MESH ORIGIN MESH=VTOL X=0 Y=0 Z=0 YAW=-64 PITCH=0 ROLL=0

#exec MESH SEQUENCE MESH=VTOL SEQ=ALL    STARTFRAME=0 NUMFRAMES=122 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=Still    STARTFRAME=0 NUMFRAMES=1 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=Flying    STARTFRAME=2 NUMFRAMES=1 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=MotorsVertical    STARTFRAME=3 NUMFRAMES=39 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=MotorsHorizontal    STARTFRAME=41 NUMFRAMES=39 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=WheelsDown    STARTFRAME=81 NUMFRAMES=19 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=WheelsUp    STARTFRAME=100 NUMFRAMES=19 RATE=10

#exec MESHMAP NEW MESHMAP=VTOL MESH=VTOL
#exec MESHMAP SCALE MESHMAP=VTOL X=0.1 Y=0.1 Z=0.2

#exec TEXTURE IMPORT NAME=VTOL1 FILE=Textures\Decorations\VTOL1.pcx GROUP=Decorations FLAGS=2
#exec MESHMAP SETTEXTURE MESHMAP=VTOL NUM=0 TEXTURE=VTOL1

#exec TEXTURE IMPORT NAME=VTOL2 FILE=Textures\Decorations\VTOL2.pcx GROUP=Decorations FLAGS=2
#exec MESHMAP SETTEXTURE MESHMAP=VTOL NUM=1 TEXTURE=VTOL2

#exec TEXTURE IMPORT NAME=VTOL3 FILE=Textures\Decorations\VTOL3.pcx GROUP=Decorations FLAGS=2
#exec MESHMAP SETTEXTURE MESHMAP=VTOL NUM=2 TEXTURE=VTOL3

// END UE2

defaultproperties

     ItemName="VTOL"
     Mesh=LodMesh'MyPackage.VTOL'
     SoundRadius=160
     SoundVolume=192
     CollisionRadius=200.0
     CollisionHeight=80.0
     Mass=6000.000000
     Buoyancy=1000.000000
}
```
