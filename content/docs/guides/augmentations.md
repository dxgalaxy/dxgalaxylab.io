---
title: Augmentations
---

This guide describes how to build custom augmentations


## Prerequisites
- [Custom characters](../custom-characters)
- [Textures](../textures)

## Create the augmentation class

Create a new file at `DeusEx/MyPackage/Classes/AugMatrix.uc` and paste this:

    class AugMatrix extends Augmentation;

    var float mpAugValue;
    var float mpEnergyDrain;

    defaultproperties
    {
         mpAugValue=1.000000
         mpEnergyDrain=40.000000
         EnergyRate=300.000000
         Icon=Texture'MyPackage.UI.AugIconMatrix'
         smallIcon=Texture'MyPackage.UI.AugIconMatrix_Small'
         AugmentationName="Matrix"
         Description="There is no spoon.|n|nTECH ONE: Power drain is normal.|n|nTECH TWO: Power drain is reduced slightly.|n|nTECH THREE: Power drain is reduced moderately.|n|nTECH FOUR: Power drain is reduced significantly."
         MPInfo="Description for multiplayer effect"
         LevelValues(0)=1.000000
         LevelValues(1)=0.830000
         LevelValues(2)=0.660000
         LevelValues(3)=0.500000
         AugmentationLocation=LOC_Cranial
         MPConflictSlot=6
    }

    state Active
    {
    Begin:
        Player.PlaySound(Sound'CloakUp', SLOT_Interact, 0.85, ,768,1.0);
        Player.Sprite = Texture(DynamicLoadObject("Extras.Matrix_A00", class'Texture'));
        Player.ConsoleCommand("slomo 0");
    }

    function Deactivate()
    {
        Player.PlaySound(Sound'CloakDown', SLOT_Interact, 0.85, ,768,1.0);
        Player.Sprite = None;
        Player.ConsoleCommand("slomo 1");
        Super.Deactivate();
    }

    simulated function float GetEnergyRate()
    {
        return energyRate * LevelValues[CurrentLevel];
    }


## Create and import textures

Create the icons for your augmentation. They should be .pcx files called `AugMatrix.pcx` (64x64) and `AugMatrix_Small.pcx` (32x32). Note that `AugMatrix.pcx` should be offset a little, look at the textures in the `DeusExUI > UserInterface` texture package for reference. Then import them into a `MyPackage.UI` package. With a script it would be done like so:

    class TextureImport expands Object abstract; 
    
    #exec TEXTURE IMPORT NAME=AugMatrix FILE=Textures\AugMatrix.pcx GROUP=UI MIPS=Off
    #exec TEXTURE IMPORT NAME=AugMatrix_Small FILE=Textures\AugMatrix_Small.pcx GROUP=UI MIPS=Off


## Create a custom augmentation manager

Since the augmentations are hardcoded into the manager, we need to subclass it. Create the file `DeusEx/MyPackage/Classes/MyAugmentationManager.uc` with this content:

    class MyAugmentationManager extends AugmentationManager;

    defaultproperties
    {
        AugLocs(0)=(NumSlots=1,KeyBase=4)
        AugLocs(1)=(NumSlots=1,KeyBase=7)
        AugLocs(2)=(NumSlots=3,KeyBase=8)
        AugLocs(3)=(NumSlots=1,KeyBase=5)
        AugLocs(4)=(NumSlots=1,KeyBase=6)
        AugLocs(5)=(NumSlots=2,KeyBase=2)
        AugLocs(6)=(NumSlots=3,KeyBase=11)
        augClasses(0)=Class'DeusEx.AugSpeed'
        augClasses(1)=Class'DeusEx.AugTarget'
        augClasses(2)=Class'DeusEx.AugCloak'
        augClasses(3)=Class'DeusEx.AugBallistic'
        augClasses(4)=Class'DeusEx.AugRadarTrans'
        augClasses(5)=Class'DeusEx.AugShield'
        augClasses(6)=Class'DeusEx.AugEnviro'
        augClasses(7)=Class'DeusEx.AugEMP'
        augClasses(8)=Class'DeusEx.AugCombat'
        augClasses(9)=Class'DeusEx.AugHealing'
        augClasses(10)=Class'DeusEx.AugStealth'
        augClasses(11)=Class'DeusEx.AugIFF'
        augClasses(12)=Class'DeusEx.AugLight'
        augClasses(13)=Class'DeusEx.AugMuscle'
        augClasses(14)=Class'DeusEx.AugVision'
        augClasses(15)=Class'DeusEx.AugDrone'
        augClasses(16)=Class'DeusEx.AugDefense'
        augClasses(17)=Class'DeusEx.AugAqualung'
        augClasses(18)=Class'DeusEx.AugDatalink'
        augClasses(19)=Class'DeusEx.AugHeartLung'
        augClasses(20)=Class'DeusEx.AugPower'
        augClasses(21)=Class'MyPackage.AugMatrix' // <- By default, there can be 25 augs in this list
        defaultAugs(0)=Class'DeusEx.AugLight'
        defaultAugs(1)=Class'DeusEx.AugIFF'
        defaultAugs(2)=Class'DeusEx.AugDatalink'
        AugLocationFull="You can't add any more augmentations to that location!"
        NoAugInSlot="There is no augmentation in that slot"
        bHidden=True
        bTravel=True
    }


## Replace the augmentation manager for the player

In order to ensure that the player is always using the custom manager, we need to introduce a little bandaid code. Deus Ex likes to automatically replace custom managers with the vanilla one, seemingly at random. Add this to your player character's .uc file:

    function InitializeSubSystems()
    {
        // Spawn the BarkManager
        if (BarkManager == None)
            BarkManager = Spawn(class'BarkManager', Self);

        // Spawn the Color Manager
        CreateColorThemeManager();
        ThemeManager.SetOwner(self);
        
        // Install the augmentation system if not found
        //----------------------------------------------------
        // WCCC: Thanks to sorcery, we have to cast this.
        // Somehow vanilla aug managers keep spawning.
        if (MyAugmentationManager(AugmentationSystem) == None)
        {
            AugmentationSystem = Spawn(class'MyAugmentationManager', Self);
            AugmentationSystem.CreateAugmentations(Self);
            AugmentationSystem.AddDefaultAugmentations();
            AugmentationSystem.SetOwner(Self);       
        }
        else
        {
            AugmentationSystem.SetPlayer(Self);
            AugmentationSystem.SetOwner(Self);
        }
        
        // Install the skill system if not found
        if (SkillSystem == None)
        {
            SkillSystem = Spawn(class'SkillManager', Self);
            SkillSystem.CreateSkills(Self);
        }
        else
        {
            SkillSystem.SetPlayer(Self);
        }
        
        if ((Level.Netmode == NM_Standalone) || (!bBeltIsMPInventory))
        {
            // Give the player a keyring
            CreateKeyRing();
        }
    }


## Build

That's it! You can now build your code. To test it, there are two ways:

1. Add a `Pawn > ScriptedPawn > Robot > MedicalBot` and an `Inventory > Pickup > DeusExPickup > AugmentationCannister` to your map, open the canister's properties and set its `AugmentationCannister > AddAugs > [0]` to `AugMatrix`.  
2. While in-game, press `T`, remove the `Say` prompt, type `augadd augmatrix` and press `Enter`.  

Then press `F5` to bend spoons.


## Further development

Check out the `DeusEx/DeusEx/Classes/Aug*.uc` files in the [source code](../custom-packages#original-source-code) for more ideas on how to implement the effects of your augmentation.  
