---
title: Documentation
description: This a comprehensive documentation on modding Deus Ex.
---

The information on this site is largely thanks to these excellent resources:  

- [Beyond Unreal](https://beyondunrealwiki.github.io)  
- [Tactical Ops Archive](https://tactical-ops.eu/info-unrealed-guide.php)  
- [Lode's UnrealEd tutorials](https://lodev.org/unrealed/tutorials.html)
- [Tack's Deus Ex Lab](http://www.stevetack.com/archive/TacksDeusExLab/)   

And these people who helped build this particular documentation:  

- aizome8086  
- Artifechs  
- Defaultmom001  
- Greasel  
- Rubber Ducky WCCC  
- TheAstroPath

!!! Note
    It is assumed that you are already somewhat familiar with UnrealEd 1/2. If you're not, check out BeyondUnreal's guides.

## Getting started
- [Installing the Deus Ex SDK](guides/installing)
- [Creating a custom package](guides/custom-packages)
- [Creating a custom character](guides/custom-characters)


## Videos
- [Unreal Editor Beginner's Guide](https://youtube.com/watch?v=ok3eDssid7s) by Shivaxi  
