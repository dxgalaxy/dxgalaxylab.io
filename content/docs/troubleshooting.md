---
title: Troubleshooting
---

## My game is too dark

1. Install <a href="../../downloads/DeusExe-v8.1.zip" download>Marijn Kentie's launcher</a>.  
2. Unpack <a href="../../downloads/dxglr20.zip" download>Chris Dohnal's OpenGL renderer</a> into your `DeusEx/System` folder.
3. Select the OpenGL renderer the first time the game starts (you can also change renderer in the game's settings).  
4. If it's still too dark, set `OneXBlending` to `False` in your `DeusEx.ini`.  

## My game has render errors

1. Download and unpack <a href="../../downloads/RenderExt.zip" download>RenderExt</a> into yor `DeusEx/System` folder.
2. In your `DeusEx.ini`, change the line under `[Engine.Engine]` from `Render=Render.Render` to `Render=RenderExt.RenderExt`.

## My textures are always scaled down to 256 pixels

In your `DeusEx.ini`, make sure that `HighDetailActors` and `UseS3TC` are set to `True` under the heading corresponding to your renderer (`[*Drv.*RenderDevice]`).

## My conversation doesn't start when I interact with the NPC

1. Is the NPC's `BindName` the same as the `Owner` of the conversation? 
2. Are there any actors referenced by `BindName` in the conversation that are not present in the map, or more than 800 units away?  
3. Is your custom character's `BindName` something other that `JCDenton`?  


## My conversation is either silent or "hangs" and doesn't progress

Check out the [audio section](../guides/conversations#adding-audio-to-a-speech-event) of the conversations guide.  


## UCC cannot compile my conversation

Are any events that refer to labels missing their target label?


## My mouse skips while moving around UnrealEd

You can try a couple of things:  

- Lower your mouse polling rate, if it's high
- Disable mouse acceleration
- Use OpenGL or Software Rendering in the viewport


## My level hangs as if in an infinite loop and then crashes (or path finding completely doesn't work)

This can happen if you have several `Inventory` actors placed close together, as they create `InventorySpot`s that are used for path finding. The easiest solution is to just space them further apart, but you can also do a search for `InventorySpot` actors and remove them before playing. If you're using UnrealEd 2.2, you can also try replacing the `Engine.u` in the `UED22` folder with the one in the `UED22/DoNotPlaceInventorySpots` folder.


## UCC says "Variable is too large (XXX bytes, 255 max)"

This can happen when you're trying to change the values of an actor from another actor. A way around this could be to create getters/setters instead.


## My ScriptedPawn is not sitting down
    
To get a `ScriptedPawn` to sit down when a map loads: 

- Make sure there is ample room between the seat and any other actors and brushes
- Place the `ScriptedPawn` directly facing the `Seat`
- Ensure that `ScriptedPawn` is not initially within view of the player


## Whenever UCC throws compiler errors, the line number is wrong

UCC skips over the `defaultproperties {}`, so always place them at the end of your script files.
