---
title: Unreal units
---

UnrealEd 1 and 2 used some funky units that can be a bit difficult to comprehend. The below information should help.  

Also, [here](../linux-scripts#converting-to-unreal-units) is a handy conversion script to make things even easier.  

## Distance

- 256 UU = 487.68 cm = 16 feet  
- 1 meter = 52.5 UU  
- 1 foot = 16 UU  
- 1 cm = 0.525 UU  
- 1 UU = 0.75 inches  


## Rotation

| Unreal units | Degrees | Radians * π | Steps per circle |
| --- | --- | --- | --- |
| 1024 | 5.625 | 1/32 | 64 |
| 2730 | 15 | 1/12 | 24 |
| 8192 | 45 | 1/4 | 8 |
| 16384 | 90 | 1/2 | 4 |
| 24576 | 135 | 3/4 | 2.66... |
| 32768 | 180 | 1 | 2 |
| 65536 | 360 | 2 | 1 |
