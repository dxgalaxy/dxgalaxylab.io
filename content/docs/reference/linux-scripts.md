---
title: Linux scripts
---

These are a couple of helpful scripts that make modding Deus Ex a little easier.


## MP3 fix

This script ensures that your audio files will work for conversations. It depends on `sox` and `id3v2`. Make sure to back up your original audio files before running this function, as it lowers the audio quality significantly.

    #!/bin/sh

    if [ -z "$1" ]
    then
        echo "Usage: dx-mp3fix PATH [OPTIONS]"
        echo "Options:"
        echo "    -a --amplify          Multiples of the original volume to amplify with"
        echo "    -d --duration         Duration of time to consider silent (must contain decimals)"
        echo "    -i --infolink         Process the audio to sound like an InfoLink message"
        echo "    -t --threshold        Percentage of sound amplitude to consider silence"
        return
    fi
   
    DIR=$(realpath $1 2> /dev/null)

    if [ ! -d "$DIR" ]
    then
        echo "Directory \"$1\" could not be found"
        return
    fi

    IS_INFOLINK=false
    DURATION="0.1"
    THRESHOLD="0.1"
    AMPLIFY="1.0"

    for i in "$@"
    do
        case $i in
            -d=*|--duration=*)
                DURATION="${i#*=}"
                ;;
            -t=*|--threshold=*)
                THRESHOLD="${i#*=}"
                ;;
            -i|--infolink)
                IS_INFOLINK=true
                ;;
            -a=*|--amplify=*)
                AMPLIFY="${i#*=}"
                ;;
        esac
    done

    for FILE in $(find $DIR -maxdepth 1 -name '*.mp3' -o -name '*.wav')
    do
        BASENAME=$(basename $FILE)
        NAME="${BASENAME%.*}"
        EXT="${BASENAME##*.}"
      
        NEW="$DIR/$NAME.$EXT"
        OLD="$DIR/$NAME.old.$EXT"

        mv $NEW $OLD

        # Remove silence from the beginning and end, ensure the sample rate is 44.1kHz, the bitrate is 48kbps and that it's mono
        SOX_CMD="sox -v $AMPLIFY $OLD -r 44.1k -C 48.99 -c 1 $NEW silence 1 $DURATION $THRESHOLD% reverse silence 1 $DURATION $THRESHOLD% reverse"

        # Remove bass and boost treble for InfoLink audio
        if [ $IS_INFOLINK = true ]
        then
            SOX_CMD="$SOX_CMD bass -40 treble +5"
        fi

        # Normalize the audio
        SOX_CMD="$SOX_CMD norm -0.5"

        $SOX_CMD 2> /dev/null

        # Remove tags
        id3v2 --delete-all $NEW 1> /dev/null 

        # Delete old file
        rm $OLD
    done


## Build

This build script removes existing .u files and comments out code for use with UnrealEd 2.x. This script assumes you have all UnrealEd versions installed, but you can adjust as needed.

1. Place this script in your `DeusEx/MyPackage` folder and name it `build.sh`.  
2. Replace "MyPackage" with your package name in the script.  
3. Add `// BEGIN UE1` and `// END UE1` comments to the files that need commenting.  
4. Make it executable: `chmod +x ./build.sh`.  
5. Run it: `./build.sh`.  

```bash
#!/bin/sh

# 0. Set up temporary variables
PACKAGE_NAME="MyPackage"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
RETURN_DIR=$PWD

# 1. Go to the DeusEx/System folder
cd "$HOME/.deus-ex-sdk/drive_c/DeusEx/System"


# 2. Build for Unreal Engine 1
echo "======================================="
echo "Building for Unreal Engine 1"
echo "======================================="
echo ""

# 2.1. Remove existing .u files from DeusEx/System
for FILE in $(find . -name "$PACKAGE_NAME*.u")
do
    echo "Removing $FILE"
    rm $FILE
done

# 2.2. Run UE1 compiler
WINEPREFIX="$HOME/.deus-ex-sdk" WINEARCH=win32 wine ./UCC.exe make

# 2.3. Copy built .u files to the DeusEx/MyPackage/System folder
for FILE in $(find . -name "$PACKAGE_NAME*.u")
do
    echo "Copying $FILE -> ../$PACKAGE_NAME/System/$(basename $FILE)"
    cp $FILE "../$PACKAGE_NAME/System"
done

# 3. Build for Unreal Engine 2
echo ""
echo "======================================="
echo "Building for Unreal Engine 2"
echo "======================================="
echo ""

# 3.1. Remove existing .u files from DeusEx/UED22
for FILE in $(find ../UED22 -name "$PACKAGE_NAME*.u")
do
    echo "Removing $FILE"
    rm $FILE
done

# 3.2. Comment UE1 exclusive code
for FILE in $(find "$SCRIPT_DIR/Classes" -name '*.uc')
do
    sed -i 's/\/\/ BEGIN UE1/\/\* BEGIN UE1/' "$FILE"
    sed -i 's/\/\/ END UE1/END UE1 \*\//' "$FILE"
done

# 3.3. Run UE2 compiler
WINEPREFIX="$HOME/.deus-ex-sdk" WINEARCH=win32 wine ../UED22/UCC.exe make

# 3.4. Uncomment UE1 exclusive code
for FILE in $(find "$SCRIPT_DIR/Classes" -name '*.uc')
do
    sed -i 's/\/\* BEGIN UE1/\/\/ BEGIN UE1/' "$FILE"
    sed -i 's/END UE1 \*\//\/\/ END UE1/' "$FILE"
done

# 3.5 Copy .u files from DeusEx/UED22 to DeusEx/Ued2 folder
for FILE in $(find ../UED22 -name "$PACKAGE_NAME*.u")
do
    echo "Copying $FILE -> ../Ued2/$(basename $FILE)"
    cp $FILE ../Ued2
done

# 4. Return to the directory we came from
cd $RETURN_DIR
```

## Converting to Unreal Units

These are handy when added to your `~/.bashrc`.  

    function deg2uu { 
        echo "print(round(182.044449 * $1))" | python
    }

    function uu2cm {
        CM=$(echo "print($1*(2/16)*30.48)" | python)
        
        wl-copy $CM
        echo $CM
    }

    function cm2uu {
        UU=$(echo "print(round($1*(1/30.48)/(2/16), 2))" | python)
       
        wl-copy $UU
        echo $UU
    }

Use them like this:

    $ deg2uu 90
    > 16384

    $ cm2uu 100
    > 26.25
