---
title: Keyboard shortcuts
---

## General

Shortcut | Effect
--- | ---
`Shift+N`           | Deselect everything
`F3`                | Show log
`F4`                | Object properties
`F5`                | Surface properties
`F6`                | Level properties
`F7`                | Compile changed scripts
`F8`                | Build options
`Shift+Left click`  | Select brush
`A+Left click`      | Place current actor
`L+Left click`      | Place light
`Shift+A`           | Select all brushes
`Alt+Left click`    | Apply current texture to surface
`Alt+Right click`   | Set texture from selected surface as current
`Left click`        | Select surface
`Shift+B`           | Select other surfaces on the same brush as the selected one
`Shift+C`           | Select adjacent coplanar polygons
`Shift+F`           | Select adjacent floors
`Shift+I`           | Select surfaces with items matching the selected surfaces' item names
`Shift+J`           | Select all adjacent surfaces
`Shift+Q`           | Invert the current surface selection
`Shift+T`           | Select all surfaces with the same texture as the selected one
`Shift+W`           | Select adjacent wall surfaces
`Shift+Y`           | Select adjacent slanted surfaces
`Shift-M`           | Memorize selected surfaces
`Shift-O`           | Select surfaces from intersection of selected surfaces and memory
`Shift-R`           | Recall memorized surface selection
`Shift-U`           | Select union of selected surfaces and memorized surfaces
`Shift-X`           | Select exclusive of selected surfaces and memorized surfaces

