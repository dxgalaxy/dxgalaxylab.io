---
title: Python scripts
---

These are a couple of helpful scripts that make modding Deus Ex a little easier.


## Build

This build script removes existing .u files and comments out code for use with UnrealEd 2.x.

Usage: `build.py [-ue2 | --unreal-engine-2] [-w WINE_PREFIX] package dir`

Positional arguments:  
- `package`: The package to build, e.g. "MyPackage"  
- `dir`: The Deus Ex directory  

Options:  
- `-ue2|--unreal-engine-2`: Optionally build for Unreal Engine 2  
- `-w|--wine-prefix WINE_PREFIX`: The WINE prefix (Linux only)  

```python
import argparse, os, glob, subprocess, shutil

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('package', type=str, help='The package to build')
parser.add_argument('dir', type=str, help='The Deus Ex directory')
parser.add_argument('-ue2', '--unreal-engine-2', type=bool, action=argparse.BooleanOptionalAction, default=False, required=False, help='Optionally build for Unreal Engine 2')

if os.name == 'posix':
    parser.add_argument('-w', '--wine-prefix', type=str, default='~/.deus-ex-sdk', required=False, help='The WINE prefix')

args = parser.parse_args()

if not os.path.isdir(args.dir):
    print('The specified directory could not be found')
    quit()

# Set up directories
root_dir = os.path.abspath(args.dir)
system_dir = os.path.join(root_dir, 'System')
ued22_dir = os.path.join(root_dir, 'UED22')
package_dir = os.path.join(root_dir, args.package)
classes_dir = os.path.join(package_dir, 'Classes')

if not os.path.isfile(os.path.join(system_dir, 'UCC.exe')):
    print('System/UCC.exe not found. Did you specify the right directory?')
    quit()

if args.unreal_engine_2 and not os.path.isfile(os.path.join(ued22_dir, 'UCC.exe')):
    print('UED22/UCC.exe not found. Did you specify the right directory?')
    quit()

# Comments/uncomments UE version specific code
def process_exclusive_code(version, is_enabled):
    global classes_dir

    for uc_file in glob.glob(os.path.join(classes_dir, '*.uc')):
        file = open(uc_file, 'r')
        content = file.read()

        if is_enabled:
            content = content.replace('/* BEGIN UE' + str(version), '// BEGIN UE' + str(version))
            content = content.replace('END UE' + str(version) + ' */', '// END UE' + str(version))
        else:
            content = content.replace('// BEGIN UE' + str(version), '/* BEGIN UE' + str(version))
            content = content.replace('// END UE' + str(version), 'END UE' + str(version) + ' */')

        file.close()

        file = open(uc_file, 'w')
        file.write(content)
        file.close()

# Go to the DeusEx/System dir
os.chdir(system_dir)

# Build for UE1
print('')
print('=======================================')
print('Building for Unreal Engine 1')
print('=======================================')
print('')

# Remove existing .u files from DeusEx/System
for u_file in glob.glob(os.path.join(system_dir, args.package + '*.u')):
    os.remove(u_file)
    
# Hide UE2 exclusive code
process_exclusive_code(2, False)

# Run UE1 compiler
if os.name == 'nt':
    subprocess.run(['UCC.exe', 'make'])

elif os.name == 'posix':
    env = os.environ.copy()
    env['WINEPREFIX'] = os.path.abspath(args.wine_prefix)
    env['WINEARCH'] = 'win32' 

    subprocess.run(['wine', 'UCC.exe', 'make'])

# Show UE2 exclusive code
process_exclusive_code(2, True)

# Copy built .u files to the DeusEx/MyPackage/System folder
for u_file in glob.glob(os.path.join(system_dir, args.package + '*.u')):
    print('Copying ' + os.path.basename(u_file) + ' -> ../' + args.package + '/System/' + os.path.basename(u_file))
    shutil.copy2(u_file, os.path.join(package_dir, 'System'))

# Build for Unreal Engine 2
if args.unreal_engine_2:
    print('')
    print('=======================================')
    print('Building for Unreal Engine 2')
    print('=======================================')
    print('')

    # Remove existing .u files from DeusEx/UED22
    for u_file in glob.glob(os.path.join(ued22_dir, args.package + '*.u')):
        os.remove(u_file)

    # Hide UE1 exclusive code
    process_exclusive_code(1, False)
        
    # Run UE2 compiler
    if os.name == 'nt':
        subprocess.run([os.path.join(ued22_dir, 'UCC.exe'), 'make'])

    elif os.name == 'posix':
        env = os.environ.copy()
        env['WINEPREFIX'] = os.path.abspath(args.wine_prefix)
        env['WINEARCH'] = 'win32' 

        subprocess.run(['wine', os.path.join(ued22_dir, 'UCC.exe'), 'make'])

    # Show UE1 exclusive code
    process_exclusive_code(1, True)
```
