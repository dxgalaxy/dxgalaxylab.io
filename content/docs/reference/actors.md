---
title: Actors
---

This is a comprehensive list of all the actors present in the base game.

| Class name | Display name |
| --- | --- |
| AcousticSensor | Gunfire Acoustic Sensor |
| AdaptiveArmor | Thermoptic Camo |
| AIPrototype | AI Prototype |
| AlarmLight | Alarm Light |
| AlarmUnit | Alarm Sounder Panel |
| AlexJacobson | Alex Jacobson |
| Ammo10mm | 10mm Ammo |
| Ammo20mm | 20mm HE Ammo |
| Ammo3006 | 30.06 Ammo |
| Ammo762mm | 7.62x51mm Ammo |
| AmmoBattery | Prod Charger |
| AmmoDart | Darts |
| AmmoDartFlare | Flare Darts |
| AmmoDartPoison | Tranquilizer Darts |
| AmmoEMPGrenade | EMP Grenade |
| AmmoGasGrenade | Gas Grenade |
| AmmoLAM | LAM |
| AmmoNanoVirusGrenade | Scramble Grenade |
| AmmoNapalm | Napalm Canister |
| AmmoPepper | Pepper Cartridge |
| AmmoPlasma | Plasma Clip |
| AmmoRocket | Rockets |
| AmmoRocketWP | WP Rockets |
| AmmoSabot | 12 Gauge Sabot Shells |
| AmmoShell | 12 Gauge Buckshot Shells |
| AnnaNavarre | Anna Navarre |
| ATM | Public Banking Terminal |
| AttackHelicopter | Attack Helicopter |
| AutoTurret | Turret Base |
| AutoTurretGun | Autonomous Defense Turret |
| BallisticArmor | Ballistic Armor |
| Barrel1 | Barrel |
| BarrelAmbrosia | Ambrosia Storage Container |
| BarrelFire | Burning Barrel |
| BarrelVirus | NanoVirus Storage Container |
| Bartender | Bartender |
| Basket | Wicker Basket |
| Basketball | Basketball |
| Binoculars | Binoculars |
| BioelectricCell | Bioelectric Cell |
| BoatPerson | Boat Person |
| BobPage | Bob Page |
| BobPageAugmented | Augmented Bob Page |
| BoneFemur | Human Femur |
| BoneSkull | Human Skull |
| BookClosed | Book |
| BookOpen | Book |
| BoxLarge | Cardboard Box |
| BoxMedium | Cardboard Box |
| BoxSmall | Cardboard Box |
| BumFemale | Bum |
| BumMale | Bum |
| BumMale2 | Older Bum |
| BumMale3 | Bum |
| Businessman1 | Businessman |
| Businessman2 | Businessman |
| Businessman3 | Businessman |
| Businesswoman1 | Businesswoman |
| Butler | Butler |
| Button1 | Button |
| CageLight | Light Fixture |
| Candybar | Candy Bar |
| Cart | Utility Push-Cart |
| Cat | Cat |
| CeilingFan | Ceiling Fan Blades |
| CeilingFanMotor | Ceiling Fan Motor |
| Chad | Chad |
| Chair1 | Chair |
| ChairLeather | Comfy Chair |
| Chandelier | Chandelier |
| Chef | Chef |
| ChildMale | Child |
| ChildMale2 | Child |
| CigaretteMachine | Cigarette Machine |
| Cigarettes | Cigarettes |
| CleanerBot | Cleaner Bot |
| ClothesRack | Hanging Clothes |
| CoffeeTable | Coffee Table |
| ComputerPersonal | Personal Computer Terminal |
| ComputerPublic | Public Computer Terminal |
| ComputerSecurity | Security Computer Terminal |
| ControlPanel | Electronic Control Panel |
| Cop | Cop |
| CouchLeather | Leather Couch |
| CrateBreakableMedCombat | Combat Supply Crate |
| CrateBreakableMedGeneral | General Supply Crate |
| CrateBreakableMedMedical | Medical Supply Crate |
| CrateExplosiveSmall | TNT Crate |
| CrateUnbreakableLarge | Metal Crate |
| CrateUnbreakableMed | Metal Crate |
| CrateUnbreakableSmall | Metal Crate |
| Credits | Credit Chit |
| Cushion | Floor Cushion |
| Dart | Dart |
| DartFlare | Flare Dart |
| DartPoison | Tranquilizer Dart |
| DataCube | DataCube |
| DentonClone | JC Denton Clone |
| Doberman | Doberman |
| Doctor | Doctor |
| EMPGrenade | Electromagnetic Pulse (EMP) Grenade |
| Fan1 | Fan |
| Fan1Vertical | Fan |
| Fan2 | Fan |
| Faucet | Faucet |
| Female1 | Female |
| Female2 | Female |
| Female3 | Female |
| Female4 | Female |
| FireExtinguisher | Fire Extinguisher |
| FlagPole | Flag Pole |
| Flare | Flare |
| Flask | Lab Flask |
| Fleshfragment | Bits of Flesh |
| Flowers | Flowers |
| Fly | Fly |
| FordSchick | Ford Schick |
| GarySavage | Gary Savage |
| GasGrenade | Gas Grenade |
| GilbertRenton | Gilbert Renton |
| GordonQuick | Gordon Quick |
| Gray | Gray |
| Greasel | Greasel |
| GuntherHermann | Gunther Hermann |
| HangingChicken | Slaughtered Chicken |
| HangingShopLight | Flourescent Light |
| HarleyFilben | Harley Filben |
| HazMatSuit | Hazmat Suit |
| HKBirdcage | Birdcage |
| HKBuddha | Buddha Statue |
| HKChair | Chair |
| HKCouch | Bench |
| HKHangingLantern | Paper Lantern |
| HKHangingLantern2 | Paper Lantern |
| HKHangingPig | Slaughtered Pig |
| HKIncenseBurner | Incense Burner |
| HKMarketLight | Hanging Light |
| HKMarketTable | Table |
| HKMarketTarp | Canvas Tarp |
| HKMilitary | Chinese Military |
| HKTable | Table |
| HKTukTuk | TukTuk |
| Hooker1 | Hooker |
| Hooker2 | Hooker |
| HowardStrong | Howard Strong |
| JaimeReyes | Jaime Reyes |
| Janitor | Janitor |
| JCDouble | JC Denton |
| Jock | Jock |
| JoeGreene | Joe Greene |
| JoJoFine | JoJo Fine |
| JordanShea | Jordan Shea |
| JosephManderley | Joseph Manderley |
| JuanLebedev | Juan Lebedev |
| JunkieFemale | Junkie |
| JunkieMale | Junkie |
| Karkian | Karkian |
| KarkianBaby | Baby Karkian |
| Keypad | Security Keypad |
| LAM | Lightweight Attack Munition (LAM) |
| Lamp1 | Table Lamp |
| Lamp2 | Halogen Lamp |
| Lamp3 | Desk Lamp |
| Lightbulb | Light Bulb |
| LightSwitch | Switch |
| Liquor40oz | Forty |
| LiquorBottle | Liquor |
| Lockpick | Lockpick |
| LowerClassFemale | Lower Class |
| LowerClassMale | Lower Class |
| LowerClassMale2 | Lower Class |
| LuciusDeBeers | Lucius De Beers in a life support tube |
| MaggieChow | Maggie Chow |
| Maid | Maid |
| Mailbox | Mailbox |
| Male1 | Male |
| Male2 | Male |
| Male3 | Male |
| Male4 | Male |
| MargaretWilliams | Margaret Williams |
| MaxChen | Max Chen |
| Mechanic | Mechanic |
| MedicalBot | Medical Bot |
| MedKit | Medkit |
| MIB | Man In Black |
| MichaelHamner | Michael Hamner |
| Microscope | Microscope |
| MilitaryBot | Military Bot |
| MiniSub | Mini-Submarine |
| MJ12Commando | MJ12 Commando |
| MJ12Troop | MJ12 Troop |
| MorganEverett | Morgan Everett |
| Multitool | Multitool |
| Mutt | Dog |
| NanoKey | NanoKey |
| NanoVirusGrenade | Scramble Grenade |
| NathanMadison | Nathan Madison |
| Newspaper | Newspaper |
| NewspaperOpen | Newspaper |
| NicoletteDuClare | Nicolette DuClare |
| Nurse | Nurse |
| NYPoliceBoat | Police Boat |
| OfficeChair | Swivel Chair |
| Pan1 | Frying Pan |
| Pan2 | Pot |
| Pan3 | Frying Pan |
| Pan4 | Pot |
| PaulDenton | Paul Denton |
| PhilipMead | Philip Mead |
| Phone | Telephone |
| Pigeon | Pigeon |
| Pillow | Pillow |
| Pinball | Pinball Machine |
| Plant1 | Houseplant |
| Plant2 | Houseplant |
| Plant3 | Houseplant |
| Poolball | Poolball |
| PoolTableLight | Hanging Light |
| Pot1 | Clay Pot |
| Pot2 | Clay Pot |
| POVCorpse | body |
| RachelMead | Rachel Mead |
| Rat | Rat |
| Rebreather | Rebreather |
| RepairBot | Repair Bot |
| RetinalScanner | Retinal Scanner |
| RiotCop | Riot Cop |
| RoadBlock | Concrete Barricade |
| Sailor | Sailor |
| SamCarter | Sam Carter |
| SandraRenton | Sandra Renton |
| SarahMead | Sarah Mead |
| ScientistFemale | Scientist |
| ScientistMale | Scientist |
| ScubaDiver | Scuba Diver |
| Seagull | Seagull |
| Secretary | Secretary |
| SecretService | Secret Service Agent |
| SecurityBot2 | Security Bot |
| SecurityBot3 | Security Bot |
| SecurityBot4 | Security Bot |
| SecurityCamera | Surveillance Camera |
| ShipsWheel | Ship's Wheel |
| ShopLight | Flourescent Light |
| ShowerFaucet | Shower Faucet |
| ShowerHead | Shower Head |
| Shuriken | Throwing Knife |
| SignFloor | Caution Sign |
| Smuggler | Smuggler |
| Sodacan | Soda |
| Soldier | Soldier |
| SoyFood | Soy Food |
| SpiderBot | SpiderBot |
| SpiderBot2 | Mini-SpiderBot |
| SpyDrone | Remote Spy Drone |
| StantonDowd | Stanton Dowd |
| SubwayControlPanel | Subway Control Panel |
| Switch1 | Switch |
| Switch2 | Switch |
| TAD | Telephone Answering Machine |
| TechGoggles | Tech Goggles |
| Terrorist | Terrorist |
| TerroristCommander | Terrorist Commander |
| ThugMale | Thug |
| ThugMale2 | Thug |
| ThugMale3 | Thug |
| TiffanySavage | Tiffany Savage |
| TobyAtanwe | Toby Atanwe |
| Toilet | Toilet |
| Toilet2 | Urinal |
| TracerTong | Tracer Tong |
| TrafficLight | Traffic Light |
| Trashbag | Trashbag |
| Trashbag2 | Trashbag |
| TrashCan1 | Trashcan |
| Trashcan2 | Trashcan |
| TrashCan3 | Trashcan |
| TrashCan4 | Trashcan |
| TrashPaper | Paper |
| TriadLumPath | Gang Member |
| TriadLumPath2 | Gang Leader |
| TriadRedArrow | Gang Member |
| Trophy | Trophy Cup |
| Tumbleweed | Tumbleweed |
| UNATCOTroop | UNATCO Troop |
| Valve | Valve |
| Van | Black Van |
| Vase1 | Vase |
| Vase2 | Vase |
| VendingMachine | Vending Machine |
| VialAmbrosia | Ambrosia Vial |
| VialCrack | Zyme Vial |
| WaltonSimons | Walton Simons |
| WaterCooler | Water Cooler |
| WaterFountain | Water Fountain |
| WeaponAssaultGun | Assault Rifle |
| WeaponAssaultShotgun | Assault Shotgun |
| WeaponBaton | Baton |
| WeaponCombatKnife | Combat Knife |
| WeaponCrowbar | Crowbar |
| WeaponEMPGrenade | Electromagnetic Pulse (EMP) Grenade |
| WeaponFlamethrower | Flamethrower |
| WeaponGasGrenade | Gas Grenade |
| WeaponGEPGun | Guided Explosive Projectile (GEP) Gun |
| WeaponHideAGun | PS20 |
| WeaponLAM | Lightweight Attack Munitions (LAM) |
| WeaponLAW | Light Anti-Tank Weapon (LAW) |
| WeaponMiniCrossbow | Mini-Crossbow |
| WeaponModAccuracy | Weapon Modification (Accuracy) |
| WeaponModClip | Weapon Modification (Clip) |
| WeaponModLaser | Weapon Modification (Laser) |
| WeaponModRange | Weapon Modification (Range) |
| WeaponModRecoil | Weapon Modification (Recoil) |
| WeaponModReload | Weapon Modification (Reload) |
| WeaponModScope | Weapon Modification (Scope) |
| WeaponModSilencer | Weapon Modification (Silencer) |
| WeaponNanoSword | Dragon's Tooth Sword |
| WeaponNanoVirusGrenade | Scramble Grenade |
| WeaponPepperGun | Pepper Gun |
| WeaponPistol | Pistol |
| WeaponPlasmaRifle | Plasma Rifle |
| WeaponProd | Riot Prod |
| WeaponRifle | Sniper Rifle |
| WeaponSawedOffShotgun | Sawed-off Shotgun |
| WeaponShuriken | Throwing Knives |
| WeaponStealthPistol | Stealth Pistol |
| WeaponSword | Sword |
| WHBenchEast | Bench |
| WHBenchLibrary | Bench |
| WHBookstandLibrary | Bookstand |
| WHCabinet | Cabinet |
| WHChairDining | Chair |
| WHChairOvalOffice | Leather Chair |
| WHChairPink | Chair |
| WHDeskLibrarySmall | Desk |
| WHDeskOvalOffice | Desk |
| WHEndtableLibrary | Table |
| WHFireplaceGrill | Fireplace Grating |
| WHFireplaceLog | Log |
| WHPhone | Telephone |
| WHPiano | Grand Piano |
| WHRedCandleabra | Candelabra |
| WHRedCouch | Couch |
| WHRedEagleTable | Table |
| WHRedLampTable | Table |
| WHRedOvalTable | Table |
| WHRedVase | Vase |
| WHTableBlue | Table |
| WIB | Woman In Black |
| WineBottle | Wine |
| Woodfragment | Shards of Wood |
