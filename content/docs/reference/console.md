---
title: Console
---

These are the known console commands for UnrealEd. They are not case sensitive.

Command | Description
--- | ---
`ACTOR ADD CLASS=(CLASS)` | Add actor of class (e.g.: CLASS=Lamp1).
`ACTOR ALIGN` | Aligns the vertices of the selected brushes to the grid.
`ACTOR APPLYTRANSFORM` | Applies the transform permanently to selected brushes.
`ACTOR CLIP Z` | In wireframe view it culls brushes that are after a certain distance.
`ACTOR DELETE` | Deletes any and all currently selected actors.
`ACTOR DUPLICATE` | Duplicates currently selected actor.
`ACTOR HIDE SELECTED` | Hides the currently selected actors/brushes.
`ACTOR HIDE UNSELECTED` | Hides any actors/brushes that are not currently selected.
`ACTOR KEYFRAME NUM=#` | Set the current actor to key frame number.
`ACTOR MIRROR X` | Mirrors the selected actor about the X axis.
`ACTOR MIRROR Y` | Mirrors the selected actor about the Y axis.
`ACTOR MIRROR Z` | Mirrors the selected actor about the Z axis.
`ACTOR REPLACE BRUSH` | Replace selected brush with the builder brush.
`ACTOR REPLACE CLASS=(CLASS)` | Replace selected actor with actor of class (eg: CLASS=Lamp1).
`ACTOR RESET ALL` | Resets the location, pivot, rotation and scale of the selection.
`ACTOR RESET LOCATION` | Resets the selected brush's location.
`ACTOR RESET PIVOT` | Resets the selected brush's pivot.
`ACTOR RESET ROTATION` | Resets the selected brush's rotation.
`ACTOR RESET SCALE` | Resets the selected brush's scale.
`ACTOR SELECT ALL` | Selects all brushes/actors within the map.
`ACTOR SELECT DELETED` | ???
`ACTOR SELECT INSIDE` | Selects all brushes/actors within the builder brush.
`ACTOR SELECT INVERT` | Inverts the state of selected to unselected or vise versa.
`ACTOR SELECT NONE` | Deselect the selected actors.
`ACTOR SELECT OFCLASS CLASS=(CLASS)` | Selects all actors of a class.
`ACTOR SELECT OFSUBCLASS CLASS=(CLASS)` | Selects all actors of a class or subclasses.
`ACTOR UNHIDE ALL` | Shows all actors/brushes that may have been hidden.
`BRUSH ADDMOVER` | Creates a mover brush from red builder brush.
`BRUSH ADD` | Creates an additive brush.
`BRUSH APPLYTRANSFORM` | Transform the selected brush permanently.
`BRUSH FROM DEINTERSECTION` | Creates new brush from the deintersection operation.
`BRUSH FROM INTERSECTION` | Creates new brush from the intersection operation.
`BRUSH SUBTRACT` | Creates a subtractive brush.
`BRUSHCLIP DELETE` | Deletes the verticies used to perform brush clipping operation.
`BRUSHCLIP FLIP` | Flips selection arrow denoting what side of brush to clip.
`BRUSHCLIP SPLIT` | Splits the selected brush along the clipping plane.
`BRUSHCLIP` | Performs the clipping operation on the selected brush.
`BSP REBUILD (LAME/GOOD/OPTIMAL) (BALANCE=0-100) (LIGHTS) (MAPS) (REJECT)` | Rebuild BSP with default settings or custom settings in brackets.
`CAMERA ALIGN` | Aligns the camera on the currently selected actors/ brushes.
`CAMERA CLOSE FREE/(VIEWPORT)` | Closes all free viewports, or a viewport by a given name.
`CAMERA HIDESTANDARD` | Hides all standard viewports.
`CAMERA OPEN` | Opens a new floating 3D viewport.
`CAMERA UPDATE` | Updates viewport.
`CLASS LOAD FILE=(FILENAME)` | Load class from file
`CLASS NEW` | Create new class. 
`CLASS SPEW ALL/(CLASS)` | Exports all scripts, or the scripts of a named class, to your game folder.
`DELETE` | Deletes selected actors/brushes.
`DUPLICATE` | Duplicates selected actors/brushes.
`EDCALLBACK SURFPROPS` | Bring up the surface properties of selected poly.
`EDIT COPY` | Copies selected actors/brushes.
`EDIT CUT` | Cuts selected actors/brushes.
`EDIT PASTE` | Pastes selected actors/brushes.
`JUMPTO X,Y,Z` | Puts all viewports centered on entered coordinates.
`LEVEL LINKS` | Update teleporter links.
`LEVEL REDRAW` | Redraws the level views updating any changes.
`LEVEL VALIDATE` | Validate the level, find errors.
`LIGHT APPLY` | Applies lights in your level.
`LSTATS` | Displays lighting stats.
`MAP BRUSH GET` | Copy selected brush to red builder brush.
`MAP BRUSH PUT` | Move selected brush to builder brush location and take on shape of builder brush.
`MAP NEW` | Start new map.
`MAP REBUILD` | Rebuild map (geometry/bsp).
`MAP SELECT ADDS` | Selects all additive brushes in the map.
`MAP SELECT FIRST` | Select the first created brush in the level.
`MAP SELECT LAST` | Select the last created brush in the level.
`MAP SELECT NONSOLIDS` | Selects all nonsolid brushes in the map.
`MAP SELECT SEMISOLIDS ` | Elects all semisolid brushes in the map.
`MAP SELECT SUBTRACTS` | Selects all subtractive brushes in the map.
`MAP SENDTO FIRST` | Send selected brush to the beginning of the bulding process.
`MAP SENDTO LAST` | Send selected brush to the end of the building process.
`MAP [LOAD/SAVE/IMPORT/EXPORT] FILE=(FILENAME)` | Executes the load, save and import commands with the given filename.
`MODE BRUSHCLIP` | Puts editor in brush clipping mode.
`MODE BRUSHROTATE` | Puts editor in brush rotate mode.
`MODE BRUSHSCALE` | Puts editor in brush scaling mode.
`MODE BRUSHSHEER` | Puts editor in brush sheer? mode.
`MODE BRUSHSNAP` | Scales a brush while snapping vertices to the grid.
`MODE BRUSHSTRETCH` | Puts editor in brush stretching mode.
`MODE CAMERAMOVE` | Puts editor in camera moving mode.
`MODE CAMERAZOOM` | Puts editor in camera zoom mode.
`MODE FACEDRAG` | Puts editor in face drag mode.
`MODE GRID=[1,0]` | Turns snap to grid on or off.
`MODE ROTGRID=[1,0]` | Turns rotational grid on or off.
`MODE SNAPDIST=#` | Distance of vertex to grid intersection before snapping to it.
`MODE SNAPVERTEX=[1,0]` | Turns vertex snap on or off.
`MODE SPEED=[1,2,3]` | Sets the movement speed.
`MODE TEXTURELOCK=[1,0]` | Turns texture lock on or off. If texture lock is on then textures will not be reset on brushes that are vertex manipulated.
`MODE TEXTUREPAN` | Puts editor in texture pan mode.
`MODE TEXTUREROTATE` | Puts editor in texture rotate mode.
`MODE TEXTURESCALE` | Puts editor in texture scaling mode.
`OBJ EXPORT PACKAGE=(NAME) TYPE=(EXPORTER) NAME=(OBJECTNAME) FILE=(FILE)` | Exports an object, TYPE=OBJECT exports to T3D.
`OBJ LOAD PACKAGE=(NAME) FILE=(FILE)` | Loads a given package from a specified file. See Embedding Code.
`OBJ RENAME OLDPACKAGE= OLDGROUP= OLDNAME= NEWPACKAGE= NEWGROUP= NEWNAME=` | Rename/move object.
`OBJ SAVEPACKAGE PACKAGE=(NAME) FILE=(FILE) WARN=?` | Saves a given package to a specified file.
`PATHS BUILD HIGHOPT` | Auto create pathnode network, opt=2.
`PATHS BUILD LOWOPT` | Auto create pathnode network, opt=0.
`PATHS BUILD` | Auto create pathnode network, opt=1.
`PATHS DEFINE` | Create reachspecs.
`PATHS HIDE` | Hide pathnodes.
`PATHS REMOVE` | Remove all pathnodes.
`PATHS SHOW` | Show pathnodes.
`PATHS UNDEFINE` | Remove all reachspecs.
`PIVOT HERE` | Places a pivot a the previously selected grid point.
`PIVOT SNAPPED` | Snaps pivot to grid.
`POLY DEFAULT TEXTURE=(PACKAGE.GROUP.TEXTURE)` | Sets the default texture in the texture browser.
`POLY SELECT ADJACENT ALL` | Selects all adjacent polys.
`POLY SELECT ADJACENT CEILINGS` | Selects all adjacent ceiling polys.
`POLY SELECT ADJACENT COPLANARS` | Selects all adjacent coplaner polys.
`POLY SELECT ADJACENT FLOORS` | Selects all adjacent floor polys.
`POLY SELECT ADJACENT SLANTS` | Selects all adjacent polys on the same slant.
`POLY SELECT ADJACENT WALLS` | Selects all adjacent wall polys.
`POLY SELECT ALL` | Selects all polys in the map.
`POLY SELECT MATCHING TEXTURE` | Selects all matching surfaces that share the same applied texture.
`POLY SELECT MEMORY` | Selects any polys that have been remembered in memory.
`POLY SELECT NONE` | Deselect any currently selected poly.
`POLY SELECT REVERSE` | Inverts the state of selected to unselected or vice versa.
`POLY SELECT ZONE` | Selects all polys in the current zone.
`POLY SET TEXTURE=(PACKAGE.GROUP.TEXTURE)` | Sets the poly to the given texture.
`POLY SETTEXTURE` | Sets the poly to the current selected texture.
`POLY TEXALIGN FLOOR` | Aligns the textures to the floor plane.
`POLY TEXALIGN ONETILE` | Aligns textures to one tile fitting.
`POLY TEXALIGN WALLCOLUMN` | Aligns texture to wall column.
`POLY TEXALIGN WALLDIR` | Aligns the textures to the wall plane.
`POLY TEXALIGN WALLPAN` | Pans out the textures on the wall plane.
`POLY TEXINFO` | Displays information about the texture like scaling multipliers.
`POLY TEXMULT U=# V=#` | ???
`POLY TEXPAN U=# V=#` | Pans textures along the U and V axis by the supplied number of units.
`POLY TEXSCALE U=# V=# UV=# VU=#` | Scales the texture by numbers entered for U and V and skews the texture for numbers entered for UV and VU.
`SELECT NONE` | Deselect any and all selected actors.
`SETCURRENTCLASS CLASS=(CLASS)` | Set the current class.
`SHOWINV` | Toggle the showing of inventory spots.
`TEXTURE APPLYDETAIL` | Apply the "current detail texture" to the texture selected in the texture browser.
`TEXTURE BATCHAPPLY DETAIL=(DetailTextureName|None) PREFIX=(TextureNameMatchingPrefix) OVERRIDE=(TRUE/FALSE)` | Search through all texture packages optionally searching for matches against the "TextureNameMatchingPrefix" for all textures found, apply DetailTextureName as the new detail texture. If a detail texture already exists and OVERRIDE=FALSE, then skip the texture.
`TEXTURE CLEAR ???` | Removes all dynamic texture sources.
`TEXTURE CLEARDETAIL` | Clear the current detail texture.
`TEXTURE CULL` | Remove texture references on all non-visible surfaces.
`TEXTURE REPLACEDETAIL` | Search through all texture packages for occurrences of detail textures that match the texture currently selected in the texture browser. If a match is found, replace the texture's detail texture with the "current detail texture.
`TEXTURE SCALE DELTA=#` | ???.
`TEXTURE SETDETAIL` | Set the "current detail texture" to the current the texture browser selection.
`TRANSACTION REDO` | Redo previous action.
`TRANSACTION UNDO` | Undo previous action.
