'use strict';

class Nav {
    constructor(element) {
        this.element = element;
        this.input = this.element.querySelector('.site-nav-links-search > input');
        this.tree = this.element.querySelector('.site-nav-links-tree > .tree');
        this.timer;
   
        this.input.oninput = (e) => {
            this.onInput(e);
        };
    }

    search() {
        const isCleared = this.input.value.length < 1;
        
        for(const item of this.tree.querySelectorAll('.tree-item')) {
            const isMatch = item.dataset.search.toLowerCase().indexOf(this.input.value.toLowerCase()) > -1;

            if(item.firstElementChild.tagName === 'DETAILS') {
                item.firstElementChild.open = !isCleared || item.classList.contains('-current-parent');
            }

            if(item.classList.contains('-placeholder')) {
                item.classList.toggle('-faded', !isCleared);
            } else {
                item.classList.toggle('-hidden', !isMatch && !isCleared);
            }
        }
    }

    onInput(e) {
        if(this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }

        this.timer = setTimeout(() => {
            this.search();
        }, 500);
    }
}

class Textures {
    constructor(element) {
        this.element = element; 
        this.input = this.element.querySelector('.textures-search-input');
        this.stats = this.element.querySelector('.textures-search-stats');
        this.timer;
   
        this.input.oninput = (e) => {
            this.onInput(e);
        };
    }

    search() {
        const isCleared = this.input.value.length < 1;
       
        if(isCleared)
        {
            for(const element of this.element.querySelectorAll('.-hidden')) {
                element.classList.remove('-hidden');
            }

            this.stats.innerHTML = '';
        }
        else
        {
            let packageMatches = 0;
            let groupMatches = 0;
            let itemMatches = 0;

            for(const texturePackage of this.element.querySelectorAll('.textures-package')) {
                let foundPackageMatch = texturePackage.dataset.name.toLowerCase().indexOf(this.input.value.toLowerCase()) > -1;
                let packageGroupMatches = 0;
                let packageItemMatches = 0;

                if(foundPackageMatch) {
                    packageMatches++;
                }

                for(const textureGroup of texturePackage.querySelectorAll('.textures-group')) {
                    let foundGroupMatch = textureGroup.dataset.name.toLowerCase().indexOf(this.input.value.toLowerCase()) > -1;
                    let groupItemMatches = 0;

                    if(foundGroupMatch) {
                        groupMatches++;
                        packageGroupMatches++;
                    }
                    
                    for(const textureItem of textureGroup.querySelectorAll('.textures-item')) {
                        const foundItemMatch = textureItem.dataset.name.toLowerCase().indexOf(this.input.value.toLowerCase()) > -1;
                       
                        if(foundItemMatch) {
                            itemMatches++;
                            packageItemMatches++;
                            groupItemMatches++;
                        }
                        
                        textureItem.classList.toggle('-hidden', !foundItemMatch && !foundGroupMatch && !foundPackageMatch);
                    }
                
                    textureGroup.classList.toggle('-hidden', !foundGroupMatch && !foundPackageMatch && groupItemMatches === 0);
                }

                texturePackage.classList.toggle('-hidden', !foundPackageMatch && packageGroupMatches === 0 && packageItemMatches === 0);
            }

            this.stats.innerHTML = `Search matched ${packageMatches} package${packageMatches != 1 ? 's' : ''}, ${groupMatches} group${groupMatches != 1 ? 's' : ''} and ${itemMatches} texture${itemMatches != 1 ? 's' : ''}`;
        }
    }


    onInput(e) {
        if(this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }

        this.timer = setTimeout(() => {
            this.search();
        }, 500);
    }
}

document.addEventListener('DOMContentLoaded', (e) => {
    new Nav(document.querySelector('.site-nav'));
    
    for(const section of document.querySelectorAll('section.textures')) {
        new Textures(section);
    }
});

