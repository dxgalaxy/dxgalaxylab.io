import os, copy, functools, json, shutil, re, textwrap, pathlib, pprint

import markdown, frontmatter, jinja2, sass

# Internal vars
base_dir = os.path.dirname(os.path.realpath(__file__))
site_map = []
all_pages = []
cfg = {}

# Markdown renderer
markdown_renderer = markdown.Markdown(extensions=['toc', 'admonition', 'extra'])

# Jinja renderer
template_renderer = jinja2.Environment(loader=jinja2.FileSystemLoader('src/html'))

# Gets the nav title of an item
def get_nav_title(name):
    if name in cfg['site_nav']:
        return cfg['site_nav'][name]

    return name

# Replace a @section match with HTML
def replace_section(match):
    template_name = match.group(1)
    heading = match.group(2)
    raw = textwrap.dedent(match.group(3))

    def parse(obj):
        if type(obj) is dict:
            for key, value in obj.items():
                obj[key] = parse(value)

        elif type(obj) is list:
            for i in range(len(obj)):
                obj[i] = parse(obj[i])

        elif type(obj) is str:
            obj = markdown_renderer.convert(obj).replace('<p>', '').replace('</p>', '')

        return obj

    if not template_name:
        return match.group(0)

    if not heading:
        heading = ''

    if not raw:
        raw = ''

    if raw.count('---') < 1:
        raw = '---\n' + raw
    
    if raw.count('---') < 2:
        raw += '\n---'

    template = template_renderer.get_template('inc/' + template_name + '.html') 
    section = frontmatter.loads(raw)

    meta = parse(dict(section.metadata))
    content = markdown_renderer.convert(section.content)
    html = template.render(
        heading=heading,
        meta=meta,
        content=content
    )
    
    return html
    
# Parse a raw markdown string for @sections
def parse_sections(raw):
    # This regex captures @ followed by the template name (and arguments) and any indented content thereafter
    return re.sub(r'^@([a-z-]+)(?:\(([a-zA-Z ]+)\))?((?:(?:(?:\n?    | *\t)+.*)|\n)*)$', replace_section, raw, flags=re.MULTILINE)

# Lists all pages
def list_pages(folder, root=False):
    for file in folder.iterdir():
        # Recurse into folders
        if file.is_dir():
            list_pages(file) 

        # Store pages 
        elif file.suffix == '.md':
            is_hidden = root and file.stem != 'index' and not file.stem in cfg['site_nav']
            raw = file.read_text()
            page = frontmatter.loads(raw)
            
            # Build path
            path = str(file.parent).replace(base_dir + '/content', '')
            
            if file.name != 'index.md':
                path += '/' + file.stem

            if not path:
                path = '/'

            node = {}

            for key, value in page.metadata.items():
                node[key] = value
          
            if not 'title' in node:
                if file.stem == 'index':
                    node['title'] = get_nav_title(file.parent.stem)
                else:
                    node['title'] = get_nav_title(file.stem)

                node['title'] = node['title'].capitalize().replace('-', ' ')

            node['url'] = path
            node['file'] = file
            node['is_hidden'] = is_hidden or path == '/'

            # Store the file content for later use,
            # and toss out the heading, since we require that in the front matter anyway
            node['content'] = re.sub(r'^# [^\n]+\n\n', '', page.content, flags=re.MULTILINE)

            all_pages.append(node)

# Give orphans some fake parents
def fix_page_orphans():
    for child in all_pages:
        is_orphan = True
        parent_url = str(pathlib.Path(child['url']).parent)

        for parent in all_pages:
            if parent_url == parent['url']:
                is_orphan = False
                break

        if is_orphan:
            all_pages.append({
                'title': get_nav_title(pathlib.Path(parent_url).stem).capitalize().replace('-', ' '),
                'url': parent_url,
                'is_placeholder': True
            })

# Sorts pages
def sort_pages(a, b):
    if 'url' in a and a['url'] == '/':
        return -1
    
    if 'url' in b and b['url'] == '/':
        return 1

    if a['title'] < b['title']:
        return -1
    
    if a['title'] > b['title']:
        return 1

    return 0

# Builds the site map
def build_site_map():
    for page in all_pages:
        if page['url'] != '/':
            for child in all_pages:
                parent_url = str(pathlib.Path(child['url']).parent)

                if parent_url != page['url']:
                    continue

                if not 'children' in page:
                    page['children'] = []

                page['children'].append(child) 

        if page['url'].count('/') < 2:
            site_map.append(page)

# Gets a copy of the site map with context
def get_contextual_site_map(url):
    output = copy.deepcopy(site_map)

    def set_current(pages):
        for page in pages:
            if not 'url' in page:
                continue

            if url == page['url']:
                page['is_current'] = True

            elif url.startswith(page['url']):
                page['is_current_parent'] = True

            if 'children' in page:
                set_current(page['children'])

            if 'is_placeholder' in page:
                del page['is_placeholder']
                del page['url']

    set_current(output)

    return output

# Run the builder
if __name__ == '__main__':
    public_dir = pathlib.Path(base_dir + '/public')
    static_dir = pathlib.Path(base_dir + '/static')
    content_dir = pathlib.Path(base_dir + '/content')

    # First read the config
    cfg = frontmatter.loads('---\n' + pathlib.Path(base_dir + '/config.yml').read_text() + '\n---').metadata 

    # Then purge the /public dir
    if public_dir.is_dir():
        shutil.rmtree(public_dir)

    public_dir.mkdir()

    # Then copy files from the /static and /content dirs into the /public dir
    shutil.copytree(str(static_dir), str(public_dir), dirs_exist_ok=True, ignore=shutil.ignore_patterns('*.md'))
    shutil.copytree(str(content_dir), str(public_dir), dirs_exist_ok=True, ignore=shutil.ignore_patterns('*.md'))

    # Then read all the pages
    list_pages(pathlib.Path(base_dir + '/content'), True)
    
    fix_page_orphans()
    all_pages.sort(key=functools.cmp_to_key(sort_pages))
   
    # Then build the site map
    build_site_map()

    # Then convert all markdown
    for page in all_pages:
        # Skip placeholders
        if not 'file' in page:
            continue

        # Make directory structure
        folder_path = str(page['file'].parent).replace(base_dir + '/content', base_dir + '/public')
        
        if page['file'].name != 'index.md':
            folder_path += '/' + page['file'].stem 

        pathlib.Path(folder_path).mkdir(parents=True, exist_ok=True)
    
        # Process content
        parsed = parse_sections(page['content'])
        content = markdown_renderer.convert(parsed)

        if not 'template' in page:
            page['template'] = 'default'

        # Get context
        url = folder_path.replace(base_dir + '/public', '')
        this_site_map = get_contextual_site_map(url)

        # Cleanup
        del page['content']
        del page['file']
        
        # Render page
        template = template_renderer.get_template(page['template'] + '.html')
        html = template.render(cfg=cfg, site_map=this_site_map, meta=page, content=content)

        # Write page HTML to file
        output = open(pathlib.Path(folder_path + '/index.html'), 'w')
        output.write(html)
        output.close() 

    # Then create a 404 page
    template = template_renderer.get_template('default.html')
    html = template.render(cfg=cfg, site_map=site_map, meta={ 'title': 'Not found', 'image': '/img/404.png', 'description': 'This page does not exist. Or rather, the page you were looking for. You know what I mean.' }, content='')
    output = open(pathlib.Path(base_dir + '/public/404.html'), 'w')
    output.write(html)
    output.close() 

    # Then compile scss files
    sass.compile(dirname=(base_dir + '/src/scss', base_dir + '/public/css'))
